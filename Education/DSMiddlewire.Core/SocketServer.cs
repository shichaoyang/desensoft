﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace DSMiddlewire.Core
{
    public class SocketServer
    {
        public SocketServer(IPAddress address, int port, int listenClientCount, int poolSize, int bufferSize)
        {
            this.listenClientCount = listenClientCount;
            this.poolSize = poolSize;
            this.bufferSize = bufferSize;

            this.endPoint = new IPEndPoint(IPAddress.Any, port);

            this.readWritePool = new SocketAsyncEventArgsPool(poolSize);
            this.bufferManager = new BufferManager(poolSize * bufferSize, bufferSize);
            this.bufferManager.InitBuffer();

            this.acceptArgs = new SocketAsyncEventArgs();
            this.acceptArgs.Completed += acceptArgs_Completed;
        }

        private int listenClientCount;
        private int poolSize;
        private int bufferSize;

        private EndPoint endPoint;
        private Socket serverSocket;
        private SocketAsyncEventArgsPool readWritePool;
        private BufferManager bufferManager;
        private SocketAsyncEventArgs acceptArgs;

        private SocketAsyncEventArgs receiveArgs;
        private SocketAsyncEventArgs sendArgs;

        public Action<Socket> OnStarted;
        public Action<Socket> OnConnected;
        public Action<SocketAsyncEventArgs> OnReceive;
        public Action<SocketAsyncEventArgs> OnSent;
        public Action<SocketAsyncEventArgs> OnDisconnect;
        public Action OnStopped;

        private AutoResetEvent resetEvent = new AutoResetEvent(false);

        public bool IsRunning()
        {
            return this.serverSocket != null;
        }

        public void Start()
        {
            if (this.serverSocket != null)
                throw new Exception("Server is running, can't init another one.");

            this.serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            //try
            //{
                this.serverSocket.Bind(endPoint);
                this.serverSocket.Listen(listenClientCount);
            //}
            //catch (ArgumentNullException ex)
            //{
            //    throw new Exception(ex.Message);
            //}
            //catch (SocketException ex)
            //{
            //    throw new Exception(ex.Message);
            //}

            if (OnStarted != null)
                OnStarted(serverSocket);

            StartAccept(null);
        }

        public void Stop()
        {

            if (serverSocket == null) return;

            serverSocket.Close();
            serverSocket = null;

            if (OnStopped != null)
                OnStopped();
        }

        private void StartAccept(SocketAsyncEventArgs e)
        {
            if (this.acceptArgs.AcceptSocket != null)
            {
                this.acceptArgs.AcceptSocket = null;
            }
            else
            {
                this.acceptArgs = new SocketAsyncEventArgs();
                acceptArgs.Completed += acceptArgs_Completed;
            }

            //No Matther clients connected the server in Async Mode or Sync mode, 
            //the "ProcessAccept" method will alway be invoked.

            if (serverSocket == null) return;  //If user stopped the server socket, then here it will exit directly.

            if (!this.serverSocket.AcceptAsync(this.acceptArgs))
            {
                ProcessAccept(this.acceptArgs);
            }

        }

        private void acceptArgs_Completed(object sender, SocketAsyncEventArgs e)
        {
            ProcessAccept(e);
        }

        private void ProcessAccept(SocketAsyncEventArgs e)
        {
            if (OnConnected != null)
            {
                OnConnected(e.AcceptSocket);
            }
            receiveArgs = this.readWritePool.Pop();
            this.bufferManager.SetBuffer(receiveArgs);
            receiveArgs.AcceptSocket = e.AcceptSocket;
            receiveArgs.Completed += IO_Completed;

            sendArgs = this.readWritePool.Pop();
            this.bufferManager.SetBuffer(sendArgs);
            sendArgs.AcceptSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            sendArgs.Completed += IO_Completed;

            if (!e.AcceptSocket.ReceiveAsync(receiveArgs))
            {
                ProcessReceive(receiveArgs);
            }

            StartAccept(e);
        }

        private void IO_Completed(object sender, SocketAsyncEventArgs e)
        {
            if (e.BytesTransferred > 0 && e.SocketError == SocketError.Success)
            {
                switch (e.LastOperation)
                {
                    case SocketAsyncOperation.Receive:
                        ProcessReceive(e);
                        break;
                    case SocketAsyncOperation.Send:
                        ProcessSend(e);
                        break;
                    case SocketAsyncOperation.Disconnect:
                        ProcessDisconnect(e);
                        break;
                    default:
                        throw new ArgumentException("Last Operation failed to load.");
                }
            }
            else
            {
                CloseClient(e);
            }
        }

        private void ProcessDisconnect(SocketAsyncEventArgs e)
        {
            if (this.OnDisconnect != null)
                this.OnDisconnect(e);
        }

        private void ProcessReceive(SocketAsyncEventArgs e)
        {
            if (e.BytesTransferred > 0 && e.SocketError == SocketError.Success)
            {
                if (this.OnReceive != null)
                    this.OnReceive(e);

                if (!e.AcceptSocket.ReceiveAsync(e))
                {
                    this.ProcessReceive(e);
                }
            }
            else
            {
                CloseClient(e);
            }
        }

        private void ProcessSend(SocketAsyncEventArgs e)
        {
            if (this.OnSent != null)
                this.OnSent(e);

            if (!e.AcceptSocket.ReceiveAsync(e))
            {
                this.ProcessReceive(e);
            }
        }

        private void CloseClient(SocketAsyncEventArgs e)
        {
            if (serverSocket != null)
            {
                if (serverSocket.Connected)
                {
                    //try
                    //{
                        serverSocket.Shutdown(SocketShutdown.Both);
                    //}
                    //catch (SocketException ex)
                    //{
                    //    throw new SocketException(ex.ErrorCode);
                    //}
                    //catch (ObjectDisposedException ex)
                    //{
                    //    throw new ObjectDisposedException(ex.Message);
                    //}
                    //finally
                    //{
                    //    serverSocket.Close();
                    //    serverSocket = null;
                    //}
                }

                if (OnDisconnect != null)
                    OnDisconnect(e);
            }
        }
    }
}