﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSMiddlewire.Core
{
    public enum PortCollection
    {
        ServerListenPort = 4521,
        ClientListenPort = 4098
    }
}
