﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;

namespace DSMiddlewire.Core
{
    public class SocketAsyncEventArgsPool
    {
        public SocketAsyncEventArgsPool(int capacity)
        {
            readWritePool = new Stack<SocketAsyncEventArgs>(capacity);

            //Init new SocketAsyncEventArgs objects and push them into stack
            for (int i = 0; i < capacity; i++)
            {
                Push(new SocketAsyncEventArgs());
            }
        }

        Stack<SocketAsyncEventArgs> readWritePool;

        public void Push(SocketAsyncEventArgs item)
        {
            if (item == null)
                throw new ArgumentNullException("Items added to SocketAsyncEventArgs pool should not be Null");

            lock (readWritePool)
                readWritePool.Push(item);
        }

        public SocketAsyncEventArgs Pop()
        {
            lock (readWritePool)
                return readWritePool.Pop();
        }

        public int Count { get { return readWritePool.Count; } }
    }
}
