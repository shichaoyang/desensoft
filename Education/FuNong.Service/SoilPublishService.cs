﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FuNong.DataAccess;
using FuNong.Framework.Caching;
using FuNong.Framework.Logger;
using System.Linq.Expressions;
using FuNong.DataContract;
using AutoMapper;

namespace FuNong.Service
{
    public class SoilPublishService : ISoilPublishService
    {
        public SoilPublishService(
              IUnitOfWork unitOfWork
            , ICacheManager cacheManager
            , ILoggerService logger)
        {
            this.unitOfWork = unitOfWork;
            this.cacheManager = cacheManager;
            this.logger = logger;

            this.soilMainRepo = unitOfWork.Repository<funong_publish_main>();
            this.soilDetailRepo = unitOfWork.Repository<funong_publish_detail>();
            this.provinceRepo = unitOfWork.Repository<edu_area_province>();
            this.cityRepo = unitOfWork.Repository<edu_area_city>();
            this.districtRepo = unitOfWork.Repository<edu_area_district>();
            this.soilTypeRepo = unitOfWork.Repository<funong_soil_type>();
            this.changeTypeRepo = unitOfWork.Repository<funong_change_type>();
            this.soilAroundRepo = unitOfWork.Repository<funong_soil_around>();
            this.intentionRepo = unitOfWork.Repository<funong_intention>();
            this.dealRepo = unitOfWork.Repository<funong_deal>();
            this.soilPublishDomain = new Model.SoilPublish(soilMainRepo
                                                         , soilDetailRepo
                                                         , soilAroundRepo
                                                         , provinceRepo
                                                         , cityRepo
                                                         , districtRepo
                                                         , soilTypeRepo
                                                         , changeTypeRepo
                                                         ,intentionRepo
                                                         );
        }

        private readonly IUnitOfWork unitOfWork;
        private readonly ICacheManager cacheManager;
        private readonly ILoggerService logger;

        private readonly IRepository<funong_publish_main> soilMainRepo;
        private readonly IRepository<funong_publish_detail> soilDetailRepo;
        private readonly IRepository<funong_soil_around> soilAroundRepo;
        private readonly IRepository<edu_area_province> provinceRepo;
        private readonly IRepository<edu_area_city> cityRepo;
        private readonly IRepository<edu_area_district> districtRepo;
        private readonly IRepository<funong_soil_type> soilTypeRepo;
        private readonly IRepository<funong_change_type> changeTypeRepo;
        private readonly IRepository<funong_intention> intentionRepo;
        private readonly IRepository<funong_deal> dealRepo;
        
        private readonly Model.SoilPublish soilPublishDomain;

        //获取分页列表
        public List<SoilPublishMainResponse> TriggerList(SoilPublishMainRequest soilPMRequest)
        {
            int totalcount;
            var queryableResults = soilPublishDomain.GetSoilMainList(soilPMRequest.pageCount
                                                                     , soilPMRequest.currentIndex
                                                                     , out totalcount, soilPMRequest.where
                                                                     , soilPMRequest.orderBy, soilPMRequest.propertyName
                                                                     , soilPMRequest.propertyValue);

            var resultList = queryableResults.ToList();

            var returnList = new List<SoilPublishMainResponse>();
            resultList.ForEach(item =>
            {
                var convertedItem = Mapper.DynamicMap<SoilPublishMainResponse>(item);

                convertedItem.totalcount = totalcount;

                var detail = soilDetailRepo.Get(x => x.publishmainid == item.id);
                if(detail==null)
                    detail = new funong_publish_detail();

                var soiltype = soilTypeRepo.Get(x => x.id == convertedItem.soiltypeid);
                if(soiltype==null)
                    soiltype = new funong_soil_type();

                var changetype = changeTypeRepo.Get(x => x.id == convertedItem.changetypeid);
                if (changetype == null)
                    changetype = new funong_change_type();

                var deal = dealRepo.Get(x => x.publishmainid == item.id && x.requestid == item.requestid);
                var ischarge = (deal==null) ? 0 : 1;
                var isjsjg = (deal == null) ? 0 : ((deal.isapprove == 0) ? 0 : 1);

                convertedItem.soiltypename = soiltype.typename;
                convertedItem.changetypename = changetype.changetitle;

                convertedItem.indeximg = detail.soilimages;
                convertedItem.chargefee = detail.changefee.ToString();

                convertedItem.districtname = districtRepo.Get(x => x.id == convertedItem.districtid).name;
                convertedItem.cityname = (from p in districtRepo.GetMany(x => x.id == convertedItem.districtid)
                                          join q in cityRepo.GetMany(x => x.id != 0)
                                          on p.cityid equals q.id
                                          select q).FirstOrDefault().name;

                convertedItem.provincename = (from p in districtRepo.GetMany(x => x.id == convertedItem.districtid)
                                              join q in cityRepo.GetMany(x => x.id != 0)
                                              on p.cityid equals q.id
                                              join x in provinceRepo.GetMany(x => x.id != 0)
                                              on q.provinceid equals x.id
                                              select x).FirstOrDefault().name;


                var soilgroundlist = GetIntList(detail.soilgroundlist);
                var earthtypelist = GetIntList(detail.earthtypelist);
                var waterlist = GetIntList(detail.waterlist);
                var aroundlist = GetIntList(detail.aroundlist);

                convertedItem.soilgroundlist = String.Join(",", soilAroundRepo.GetMany(x => soilgroundlist.Contains(x.id)).Select(x => x.aroundattribute).ToList());
                convertedItem.earthtypelist = String.Join(",", soilAroundRepo.GetMany(x => earthtypelist.Contains(x.id)).Select(x => x.aroundattribute).ToList());
                convertedItem.waterlist = String.Join(",", soilAroundRepo.GetMany(x => waterlist.Contains(x.id)).Select(x => x.aroundattribute).ToList());
                convertedItem.aroundlist = String.Join(",", soilAroundRepo.GetMany(x => aroundlist.Contains(x.id)).Select(x => x.aroundattribute).ToList());

                convertedItem.ischarge = ischarge;
                convertedItem.isjsjg = isjsjg;

                returnList.Add(convertedItem);
            });

            returnList = returnList.OrderBy(x => x.recordorder).ToList();
            return returnList;
        }

        private List<int> GetIntList(string str)
        {
            var list = new List<int>();
            if (string.IsNullOrEmpty(str))
                return list;
            
            str.Split(',').ToList().ForEach(item =>
            {
                int itemInInt;
                if (Int32.TryParse(item, out itemInInt))
                {
                    list.Add(itemInInt);
                }
            });
            return list;
        }

        //获取省份列表
        public List<ProvinceResponse> GetProvince()
        {
            var response = soilPublishDomain.AttachProvince().ToList();
            var returnList = new List<ProvinceResponse>();
            response.ForEach(item =>
            {
                var convertedItem = Mapper.DynamicMap<ProvinceResponse>(item);
                returnList.Add(convertedItem);
            });
            return returnList;
        }

        //获取省份下的市区列表
        public List<CityResponse> GetCityByProvinceID(SoilAttributeRequest request)
        {
            var response = soilPublishDomain.AttachCityByProvinceID(request.provinceid).ToList();

            var returnList = new List<CityResponse>();
            response.ForEach(item =>
            {
                var convertedItem = Mapper.DynamicMap<CityResponse>(item);
                returnList.Add(convertedItem);
            });

            returnList = returnList.OrderBy(x => x.id).ToList();
            return returnList;
        }

        //获取市区下面的区县列表
        public List<DistrictResponse> GetDistrictByCityID(SoilAttributeRequest request)
        {
            var response = soilPublishDomain.AttachDistrictByCityID(request.cityid).ToList();

            var returnList = new List<DistrictResponse>();
            response.ForEach(item =>
            {
                var convertedItem = Mapper.DynamicMap<DistrictResponse>(item);
                returnList.Add(convertedItem);
            });

            returnList = returnList.OrderBy(x => x.id).ToList();
            return returnList;
        }

        //获取土地类型 (农业地商用地)
        public List<SoilTypeResponse> GetSoilType(SoilAttributeRequest request)
        {
            var response = soilPublishDomain.AttachSoilType();
            var responseList = new List<funong_soil_type>();

            if (request.soiltypeid == -1)  //获取全部
                responseList = response.ToList();
            else
                responseList = response.Where(x => x.parentid == request.soiltypeid).ToList();

            var returnList = new List<SoilTypeResponse>();
            responseList.ForEach(item =>
            {
                var convertedItem = Mapper.DynamicMap<SoilTypeResponse>(item);
                returnList.Add(convertedItem);
            });
            returnList = returnList.OrderBy(x => x.id).ToList();
            return returnList;
        }

        //获取流转方式 (转包，招拍挂等)
        public List<ChangeTypeResponse> GetChangeType(SoilAttributeRequest request)
        {
            var response = soilPublishDomain.AttachChangeType();
            var responseList = new List<funong_change_type>();

            if (request.changetypeid == -1)  //获取全部
                responseList = response.ToList();
            else
                responseList = response.Where(x => x.id == request.changetypeid).ToList();

            var returnList = new List<ChangeTypeResponse>();
            responseList.ForEach(item =>
            {
                var convertedItem = Mapper.DynamicMap<ChangeTypeResponse>(item);
                returnList.Add(convertedItem);
            });
            returnList = returnList.OrderBy(x => x.id).ToList();
            return returnList;
        }

        //获取地上物
        public List<SoilAroundResponse> GetSoilAroundList(SoilAttributeRequest request)
        {
            var response = soilPublishDomain.AttachSoilAround(request.soilaroundflag).ToList();

            var returnList = new List<SoilAroundResponse>();
            response.ForEach(item =>
            {
                var convertedItem = Mapper.DynamicMap<SoilAroundResponse>(item);
                returnList.Add(convertedItem);
            });
            returnList = returnList.OrderBy(x => x.id).ToList();
            return returnList;
        }

        //获取指定的publishmain实体
        public SoilPublishMainResponse GetPublishMainBy(SoilPublishMainRequest request)
        {
            var result = soilMainRepo.Get(x => x.publishtitle == request.soiltitle && x.publisher == request.publisher && x.publisherphone == request.publisherphone);
            return Mapper.DynamicMap<SoilPublishMainResponse>(result);
        }

        //获取意向受让方
        public List<IntentionResponse> GetIntentionByMainID(IntentionRequest request)
        {
            int totalcount;
            var result = soilPublishDomain.GetIntentionList(request.pageCount
                                                                     , request.currentIndex
                                                                     , out totalcount, request.where
                                                                     , request.orderBy, request.propertyName
                                                                     , request.propertyValue).ToList();

            var returnList = new List<IntentionResponse>();

            if (result != null)
            {
                result.ForEach(item =>
                {
                    var convertedItem = Mapper.DynamicMap<IntentionResponse>(item);
                    convertedItem.totalcount = totalcount;
                    returnList.Add(convertedItem);
                });
            }
            return returnList;
        }

        //首页展示的土地数据
        public Dictionary<SoilTypeResponse, List<SoilPublishMainResponse>> TriggerSoilIndex()
        {
            var dict = new Dictionary<SoilTypeResponse, List<SoilPublishMainResponse>>();

            var soilIndexDict = soilPublishDomain.TriggerSoilIndex();

            if (dict == null)
            {
                return new Dictionary<SoilTypeResponse, List<SoilPublishMainResponse>>();
            }
            else
            {
                foreach (var kvp in soilIndexDict)
                {
                    var soiltypekey = kvp.Key;
                    var publishlist = kvp.Value;

                    var convertedkey = Mapper.DynamicMap<SoilTypeResponse>(soiltypekey);

                    var listConverted = new List<SoilPublishMainResponse>();

                    if (publishlist != null)
                    {
                        publishlist.ForEach(item =>
                        {
                            var convertedItem = Mapper.DynamicMap<SoilPublishMainResponse>(item);

                            var detail = soilDetailRepo.Get(x => x.publishmainid == item.id);
                            if (detail == null)
                                detail = new funong_publish_detail();

                            convertedItem.soiltypename = soilTypeRepo.Get(x => x.id == item.soiltypeid).typename;
                            convertedItem.changetypename = changeTypeRepo.Get(x => x.id == item.changetypeid).changetitle;
                            convertedItem.indeximg = detail.soilimages;
                            convertedItem.chargefee = detail.changefee.ToString();
                            listConverted.Add(convertedItem);
                        });
                    }

                    dict.Add(convertedkey, listConverted);
                }
            }
            return dict;
        }
    }
}
