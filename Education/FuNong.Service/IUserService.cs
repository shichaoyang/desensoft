﻿using System;
using FuNong.DataAccess;
using System.Collections.Generic;
using FuNong.DataContract;
namespace FuNong.Service
{
    public interface IUserService
    {
        List<UserResponse> TriggerList(UserRequest userRequest);
        UserResponse TriggerMainUserList(UserRequest userRequest);
        UserResponse UserLogin(UserValidationRequest uservalidationRequest);
        List<UserResponse> GetRequestIDByUser(UserRequest request);
        List<UserResponse> GetPublishMainIDByUser(UserRequest request);
    }
}
