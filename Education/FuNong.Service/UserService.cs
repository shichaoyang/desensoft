﻿using System.Collections.Generic;
using System.Linq;
using FuNong.Model;
using FuNong.DataContract;
using FuNong.DataAccess;
using AutoMapper;
using FuNong.Framework.Caching;
using FuNong.Framework.Logger;
using System.Data.Entity.Validation;
using System;

namespace FuNong.Service
{
    public class UserService:IUserService
    {
        public UserService(
              IUnitOfWork unitOfWork
            , ICacheManager cacheManager
            , ILoggerService logger)
        {
            this.unitOfWork         = unitOfWork;
            this.cacheManager       = cacheManager;
            this.logger             = logger;

            userMainRepo            = unitOfWork.Repository<bma_users>();
            userDetailRepo          = unitOfWork.Repository<edu_user_detail>();

            var departmentRepo      = unitOfWork.Repository<edu_department_detail>();
            var userRepo            = unitOfWork.Repository<edu_users_view>();
            var roleRepo            = unitOfWork.Repository<edu_role_detail>();
            var modelRepo           = unitOfWork.Repository<edu_model_detail>();
            var rolemodelRepo       = unitOfWork.Repository<edu_role_model>();
            var districtRepo        = unitOfWork.Repository<edu_area_district>();
            var cityRepo            = unitOfWork.Repository<edu_area_city>();
            var provinceRepo        = unitOfWork.Repository<edu_area_province>();
            var serviceRequestRepo  = unitOfWork.Repository<funong_service_request>();
            var publishmainRepo     = unitOfWork.Repository<funong_publish_main>();

            roleRepo                = unitOfWork.Repository<edu_role_detail>();
            schoolRepo              = unitOfWork.Repository<edu_school>();
            gradeRepo               = unitOfWork.Repository<edu_school_grade>();
            classRepo               = unitOfWork.Repository<edu_school_class>();

            user                    = new User(departmentRepo, userRepo, userMainRepo, roleRepo, modelRepo, rolemodelRepo, districtRepo, cityRepo, provinceRepo, serviceRequestRepo, publishmainRepo);
        }

        private readonly IUnitOfWork unitOfWork;
        private readonly ICacheManager cacheManager;
        private readonly ILoggerService logger;
        private readonly User user;

        private readonly IRepository<edu_role_detail>   roleRepo;
        private readonly IRepository<edu_school>        schoolRepo;
        private readonly IRepository<edu_school_grade>  gradeRepo;
        private readonly IRepository<edu_school_class>  classRepo;
        private readonly IRepository<bma_users>         userMainRepo;
        private readonly IRepository<edu_user_detail>   userDetailRepo;

        public List<UserResponse> TriggerList(UserRequest userRequest)
        {
            int totalcount;
            var queryableResults = user.GetUserDetails(userRequest.pageCount, userRequest.currentIndex
                                                     , out totalcount, userRequest.where
                                                     , userRequest.orderBy, userRequest.propertyName
                                                     , userRequest.propertyValue);

            var resultList = queryableResults.ToList();

            var returnList = new List<UserResponse>();
            resultList.ForEach(item =>
            {
                var itemRes = new UserResponse();

                user.UserDetailEntity = item;

                //var school = schoolRepo.Get(x => x.id == item.schoolid)??new edu_school();
               // var classe = classRepo.Get(x=>x.id==item.classid)?? new edu_school_class();
                //var grade = gradeRepo.Get(x=>x.id==classe.gradeid)?? new edu_school_grade();

                var department = user.AttachDepartment();
                if (department == null) department = new edu_department_detail();

                var role = user.AttachRole();
                if (role == null) role = new edu_role_detail();

                itemRes.idcard          = (item.idcard??string.Empty).Trim();
                itemRes.useraddress     = (item.useraddress ?? string.Empty).Trim();
                itemRes.departmentid    = item.departmentid;
                itemRes.departmentname  = (department.departmentname??string.Empty).Trim();
                itemRes.roleid          = item.roleid;
                itemRes.nickname        = (item.nickname??string.Empty).Trim();
                itemRes.usernote        = (item.usernote??string.Empty).Trim();
                itemRes.recordorder     = item.recordorder;
                itemRes.updatetime      = item.updatetime;
                itemRes.ischeck         = item.ischeck.GetValueOrDefault();
                //itemRes.schoolid        = item.schoolid;
                itemRes.smsnumber       = (item.smsnumber??string.Empty).Trim();
                itemRes.personcode      = (item.personcode??string.Empty).Trim();
                itemRes.checkincard     = (item.checkincard??string.Empty).Trim();
                itemRes.genderid        = item.genderid;
                itemRes.personstateid   = item.personstateid.GetValueOrDefault();
                itemRes.qq              = (item.qq??string.Empty).Trim();
                itemRes.host            = (item.host??string.Empty).Trim();
                itemRes.createtime      = item.createtime;
                itemRes.rolename        = (role.rolename??string.Empty).Trim();
                //itemRes.schoolname      = (school.name??string.Empty).Trim();

                //add by scy 2015-05-05
                itemRes.ismultischool   = role.ismultischool;

                //add by scy 2015-05-08
                //itemRes.gradeid         = grade.id;
                //itemRes.gradename       = grade.name;
                //itemRes.classid         = classe.id;
                //itemRes.classname       = classe.classname;

                //add by scy 2015-05-12
                //itemRes.refuid          = item.refuid;

                itemRes.uid             = item.uid;
                itemRes.username        = (item.username??string.Empty).Trim();
                itemRes.usermail        = (item.email??string.Empty).Trim();
                itemRes.userphone       = (item.mobile ?? string.Empty).Trim();
                itemRes.userpass        = item.password;
                itemRes.salt            = item.salt;

                itemRes.totalcount      = totalcount;

                returnList.Add(itemRes);
            });

            returnList = returnList.OrderBy(x => x.recordorder).ToList();
            return returnList;
        }

        public UserResponse TriggerMainUserList(UserRequest userRequest)
        {
            int totalcount;
            var queryableResults = user.GetUserMain(userRequest.pageCount, userRequest.currentIndex
                                                     , out totalcount, x => x.username == userRequest.username
                                                     , x => x.liftbantime, userRequest.propertyName
                                                     , userRequest.propertyValue);

            var resultRes = queryableResults.ToList().FirstOrDefault();
            var convertedRes = new UserResponse();
            if (resultRes == null)
            {
                convertedRes.uid = -1;
            }
            else
            {
                convertedRes.uid = resultRes.uid;
            }
            return convertedRes;
        }

        public List<UserResponse> GetRequestIDByUser(UserRequest request)
        {
            var result =  user.AttachServiceRequest(request.userid).ToList();

            var returnList = new List<UserResponse>();
            result.ForEach(item =>
            {
                var ur = new UserResponse();
                ur.requestid = item.id;
                returnList.Add(ur);
            });

            returnList = returnList.OrderBy(x => x.updatetime).ToList();
            return returnList;
        }

        public List<UserResponse> GetPublishMainIDByUser(UserRequest request)
        {
            var result = user.AttachPublishMain(request.userid).ToList();

            var returnList = new List<UserResponse>();
            result.ForEach(item =>
            {
                var ur = new UserResponse();
                ur.publishmainid = item.id;
                returnList.Add(ur);
            });

            returnList = returnList.OrderBy(x => x.updatetime).ToList();
            return returnList;
        }

        public UserResponse UserLogin(UserValidationRequest uservalidationRequest )
        {
            var departmentRepo = unitOfWork.Repository<edu_department_detail>();
            var roleRepo = unitOfWork.Repository<edu_role_detail>();

            var loginSuccess = user.ValidateLogin(uservalidationRequest.username, uservalidationRequest.userpass);
            if (loginSuccess == null )
            {
                return new UserResponse() { Success = false, Message = "用户名密码验证失败或用户未被审核，请重试！" };
            }
            else
            {
                var role = roleRepo.Get(x => x.id == loginSuccess.roleid);
                if (role == null)
                    role = new edu_role_detail();

                //var school = schoolRepo.Get(x => x.id == loginSuccess.schoolid);
                //if (school == null) school = new edu_school();

                return new UserResponse()
                {
                    Success = true,
                    uid = loginSuccess.uid,
                    roleid = loginSuccess.roleid,
                    useraddress = loginSuccess.useraddress,
                    updatetime = loginSuccess.updatetime,
                    username = loginSuccess.username,
                    nickname = loginSuccess.nickname,
                    usernote = loginSuccess.usernote,
                    usermail = loginSuccess.email,
                    userphone = loginSuccess.mobile,
                    departmentid = loginSuccess.departmentid,
                    idcard = loginSuccess.idcard,
                    ischeck = loginSuccess.ischeck.GetValueOrDefault(),
                    recordorder = loginSuccess.recordorder,
                    //schoolid = loginSuccess.schoolid,
                    //schoolname = school.name,
                    rolename = role.rolename,
                    //add by scy 2015-05-05
                    ismultischool   = role.ismultischool
                };
            }
        }
    }
}
