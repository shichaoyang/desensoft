﻿using System;
using System.Collections.Generic;
using FuNong.DataContract;
namespace FuNong.Service
{
    public interface IBaseDataService
    {
        List<BaseSettingResponse> GetSettingList(BaseSettingRequest request);
        List<VillageResponse> GetVillageList(VillageRequest request);
        List<LogResponse> GetLogList(LogRequest request);
        List<IconResponse> GetIconList(IconRequest request);
        List<FieldControlResponse> GetFieldControlResponse(FieldControlRequest request);
    }
}
