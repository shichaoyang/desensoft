﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FuNong.Service
{
    public interface IBaseService<TEntity> where TEntity:class
    {
        //批量新增
        bool AddInBatch(IList<TEntity> entityList);

        //批量更新
        bool UpdateInBatch(IList<TEntity> entityList);

        //批量删除
        bool DeleteInBatch(IList<TEntity> entityList);
    }
}
