﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FuNong.DataAccess;
using System.Linq.Expressions;
using FuNong.Framework.Caching;
using FuNong.Framework.Logger;
using System.Data.Entity.Validation;

namespace FuNong.Service
{
    public class BaseService<T> : IBaseService<T> where T : class
    {
        public BaseService(
              IUnitOfWork unitOfWork
            , ICacheManager cacheManager
            , ILoggerService logger
            )
        {
            this.unitOfWork = unitOfWork;
            this.cacheManager = cacheManager;
            this.logger = logger;
        }
        private readonly IUnitOfWork unitOfWork;
        private readonly ICacheManager cacheManager;
        private readonly ILoggerService logger;

        public virtual bool AddInBatch(IList<T> list)
        {
            try
            {
                var repository = unitOfWork.Repository<T>();
                foreach (var child in list)
                {
                    repository.Add(child);
                }
                unitOfWork.Commit();
                //logger.Info(new logContent(string.Format("批量添加了{0}的配置", typeof(T).Name)));
                return true;
            }
            catch (DbEntityValidationException dbex)
            {
                var msg = string.Empty;
                foreach (var validationErrors in dbex.EntityValidationErrors)
                    foreach (var validateionError in validationErrors.ValidationErrors)
                        msg += string.Format("Property:{0} Error:{1}", validateionError.PropertyName, validateionError.ErrorMessage);

                var fail = new Exception(msg, dbex);
                //logger.Error(string.Format("批量添加{0}失败！", typeof(T).Name));
                return false;
            }
            catch (Exception ex)
            {
                //logger.Error(string.Format("批量添加{0}失败！", typeof(T).Name));
                return false;
            }
        }

        public virtual bool UpdateInBatch(IList<T> list)
        {
            var repository = unitOfWork.Repository<T>();
            try
            {
                foreach (var child in list)
                {
                    repository.Update(child);
                }
                unitOfWork.Commit();
                //logger.Info(string.Format("批量更新了{0}的配置", typeof(T).Name));
                return true;
            }
            catch (DbEntityValidationException dbex)
            {
                var msg = string.Empty;
                foreach (var validationErrors in dbex.EntityValidationErrors)
                    foreach (var validateionError in validationErrors.ValidationErrors)
                        msg += string.Format("Property:{0} Error:{1}", validateionError.PropertyName, validateionError.ErrorMessage);

                var fail = new Exception(msg, dbex);
                //logger.Error(string.Format("批量更新{0}失败！", typeof(T).Name));
                return false;
            }
            catch (Exception ex)
            {
                //logger.Error(string.Format("批量更新{0}失败！", typeof(T).Name));
                return false;
            }
        }

        public virtual bool DeleteInBatch(IList<T> list)
        {
            try
            {
                var repository = unitOfWork.Repository<T>();
                foreach (var child in list)
                {
                    repository.Remove(child);
                }
                unitOfWork.Commit();
                //logger.Info(string.Format("批量删除了{0}的配置", typeof(T).Name));
                return true;
            }
            catch (DbEntityValidationException dbex)
            {
                var msg = string.Empty;
                foreach (var validationErrors in dbex.EntityValidationErrors)
                    foreach (var validateionError in validationErrors.ValidationErrors)
                        msg += string.Format("Property:{0} Error:{1}", validateionError.PropertyName, validateionError.ErrorMessage);

                var fail = new Exception(msg, dbex);
                //logger.Error(string.Format("批量删除{0}失败！", typeof(T).Name));
                return false;
            }
            catch (Exception Ex)
            {
                //logger.Error(string.Format("批量删除{0}失败:{1}", typeof(T).Name, Ex.Message));
                return false;
            }
        }
    }
}