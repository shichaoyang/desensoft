﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FuNong.DataAccess;
using FuNong.Framework.Caching;
using FuNong.Framework.Logger;
using FuNong.DataContract;
using AutoMapper;

namespace FuNong.Service
{
    public class ProcessService : FuNong.Service.IProcessService
    {
        public ProcessService(
              IUnitOfWork unitOfWork
            , ICacheManager cacheManager
            , ILoggerService logger)
        {
            this.unitOfWork = unitOfWork;
            this.cacheManager = cacheManager;
            this.logger = logger;

            this.soilMainRepo = unitOfWork.Repository<funong_publish_main>();
            this.soilDetailRepo = unitOfWork.Repository<funong_publish_detail>();
            this.provinceRepo = unitOfWork.Repository<edu_area_province>();
            this.cityRepo = unitOfWork.Repository<edu_area_city>();
            this.districtRepo = unitOfWork.Repository<edu_area_district>();
            this.soilTypeRepo = unitOfWork.Repository<funong_soil_type>();
            this.changeTypeRepo = unitOfWork.Repository<funong_change_type>();
            this.soilAroundRepo = unitOfWork.Repository<funong_soil_around>();
            this.serviceRequestRepo = unitOfWork.Repository<funong_service_request>();
            this.processDomain = new Model.Process(soilMainRepo
                                                         , soilDetailRepo
                                                         , soilAroundRepo
                                                         , serviceRequestRepo
                                                         );

            this.userRepo = unitOfWork.Repository<edu_users_view>();
            this.settingRepo = unitOfWork.Repository<edu_base_setting>();
        }

        private readonly IUnitOfWork unitOfWork;
        private readonly ICacheManager cacheManager;
        private readonly ILoggerService logger;

        private readonly IRepository<funong_publish_main> soilMainRepo;
        private readonly IRepository<funong_publish_detail> soilDetailRepo;
        private readonly IRepository<funong_soil_around> soilAroundRepo;
        private readonly IRepository<edu_area_province> provinceRepo;
        private readonly IRepository<edu_area_city> cityRepo;
        private readonly IRepository<edu_area_district> districtRepo;
        private readonly IRepository<funong_soil_type> soilTypeRepo;
        private readonly IRepository<funong_change_type> changeTypeRepo;
        private readonly IRepository<funong_service_request> serviceRequestRepo;
        private readonly IRepository<edu_users_view> userRepo;
        private readonly IRepository<edu_base_setting> settingRepo;

        private readonly Model.Process processDomain;

        public List<ProcessResponse> AttachUnServedRequest(ProcessRequest request)
        {
            var unservedRequestList = new List<ProcessResponse>();

            var result = processDomain.AttachUnServedRequest().Where(x => x.clientid == request.userid).ToList();
            if (result != null)
            {
                var userEntity = userRepo.Get(x => x.uid == request.userid);
                if (userEntity == null)
                    userEntity = new edu_users_view();

                result.ForEach(item =>
                {
                    var convertedItem = Mapper.DynamicMap<ProcessResponse>(item);
                    convertedItem.clientname = userEntity.username;
                    convertedItem.clientidcard = userEntity.idcard;
                    convertedItem.clientphone = userEntity.mobile;
                    convertedItem.clientaddress = userEntity.useraddress;

                    convertedItem.servername = settingRepo.Get(x => x.settinggroup == "COMPANY" && x.settingname == "companyname").settingvalue1;
                    convertedItem.serveridcard = settingRepo.Get(x => x.settinggroup == "COMPANY" && x.settingname == "representativeidcard").settingvalue1;
                    convertedItem.serverphone = settingRepo.Get(x => x.settinggroup == "COMPANY" && x.settingname == "companyphone").settingvalue1;
                    convertedItem.serverlocation = settingRepo.Get(x => x.settinggroup == "COMPANY" && x.settingname == "companylocation").settingvalue1;

                    unservedRequestList.Add(convertedItem);
                });
            }
            return unservedRequestList;
        }

        public List<ProcessResponse> AttachServedRequest(ProcessRequest request)
        {
            var unservedRequestList = new List<ProcessResponse>();

            var result = processDomain.AttachServedRequest().Where(x => x.clientid == request.userid).ToList();

            //如果funong_publish_main存在这个requestid,那么这条记录也要被排除掉，因为这个流程已经走到第4步了，第三步不能再显示出来了。
            var mainrequestIdList = soilMainRepo.GetMany(x => x.requestid != 0).Select(x => x.requestid).Distinct().ToList();
            result = result.Where(x => !mainrequestIdList.Contains(x.id)).ToList();

            if (result != null)
            {
                var userEntity = userRepo.Get(x => x.uid == request.userid);
                if (userEntity == null)
                    userEntity = new edu_users_view();

                result.ForEach(item =>
                {
                    var convertedItem = Mapper.DynamicMap<ProcessResponse>(item);
                    convertedItem.clientname = userEntity.username;
                    convertedItem.clientidcard = userEntity.idcard;
                    convertedItem.clientphone = userEntity.mobile;
                    convertedItem.clientaddress = userEntity.useraddress;

                    convertedItem.servername = settingRepo.Get(x => x.settinggroup == "COMPANY" && x.settingname == "companyname").settingvalue1;
                    convertedItem.serveridcard = settingRepo.Get(x => x.settinggroup == "COMPANY" && x.settingname == "representativeidcard").settingvalue1;
                    convertedItem.serverphone = settingRepo.Get(x => x.settinggroup == "COMPANY" && x.settingname == "companyphone").settingvalue1;
                    convertedItem.serverlocation = settingRepo.Get(x => x.settinggroup == "COMPANY" && x.settingname == "companylocation").settingvalue1;

                    unservedRequestList.Add(convertedItem);
                });
            }
            return unservedRequestList;
        }

        public ProcessResponse AttachServiceRequest(ProcessRequest request)
        {
            funong_service_request result = null;

            if (request.userid != -1)
                result = serviceRequestRepo.Get(x => x.id == request.requestid && x.clientid == request.userid);
            else
                result = serviceRequestRepo.Get(x => x.id == request.requestid );

            if (result == null)
            {
                return new ProcessResponse() { Success = false, Message = "无法获取委托申请！" };
            }
            else
            {
                var convertedResult = Mapper.DynamicMap<ProcessResponse>(result);

                var userEntity = new edu_users_view();

                if (request.userid == -1)
                    userEntity = userRepo.Get(x => x.uid == convertedResult.clientid);
                else
                    userEntity = userRepo.Get(x => x.uid == request.userid);

                convertedResult.Success = true;

                convertedResult.clientname = userEntity.username;
                convertedResult.clientidcard = userEntity.idcard;
                convertedResult.clientphone = userEntity.mobile;
                convertedResult.clientaddress = userEntity.useraddress;

                convertedResult.servername = settingRepo.Get(x => x.settinggroup == "COMPANY" && x.settingname == "companyname").settingvalue1;
                convertedResult.serveridcard = settingRepo.Get(x => x.settinggroup == "COMPANY" && x.settingname == "representativeidcard").settingvalue1;
                convertedResult.serverphone = settingRepo.Get(x => x.settinggroup == "COMPANY" && x.settingname == "companyphone").settingvalue1;
                convertedResult.serverlocation = settingRepo.Get(x => x.settinggroup == "COMPANY" && x.settingname == "companylocation").settingvalue1;

                return convertedResult;
            }
        }

        public List<ProcessResponse> TriggerList(ProcessRequest request)
        {
            int totalcount;

            var queryableResults = processDomain.GetServiceRequestList(request.pageCount, request.currentIndex, out totalcount, request.where, request.orderBy, request.propertyName, request.propertyValue);

            var resultList = queryableResults.ToList();

            var returnList = new List<ProcessResponse>();
            resultList.ForEach(item =>
            {
                var convertedItem = Mapper.DynamicMap<ProcessResponse>(item);

                edu_users_view userEntity = null;

                if (request.userid == -1) userEntity = userRepo.Get(x => x.uid == convertedItem.clientid);
                else userEntity = userRepo.Get(x => x.uid == request.userid);

                if (userEntity == null)
                    userEntity = new edu_users_view();
               
                convertedItem.totalcount = totalcount;

                convertedItem.clientname = userEntity.username;
                convertedItem.clientidcard = userEntity.idcard;
                convertedItem.clientphone = userEntity.mobile;
                convertedItem.clientaddress = userEntity.useraddress;

                convertedItem.servername = settingRepo.Get(x => x.settinggroup == "COMPANY" && x.settingname == "companyname").settingvalue1;
                convertedItem.serveridcard = settingRepo.Get(x => x.settinggroup == "COMPANY" && x.settingname == "representativeidcard").settingvalue1;
                convertedItem.serverphone = settingRepo.Get(x => x.settinggroup == "COMPANY" && x.settingname == "companyphone").settingvalue1;
                convertedItem.serverlocation = settingRepo.Get(x => x.settinggroup == "COMPANY" && x.settingname == "companylocation").settingvalue1;

                returnList.Add(convertedItem);
            });

            returnList = returnList.OrderBy(x => x.recordorder).ToList();
            return returnList;
        }
    }
}