﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FuNong.DataContract;
using FuNong.Model;
using FuNong.DataAccess;
using AutoMapper;
using FuNong.Framework.Caching;
using FuNong.Framework.Logger;
using System.Data.Entity.Validation;

namespace FuNong.Service
{
    public class DepartmentService:IDepartmentService
    {
        public DepartmentService(
              IUnitOfWork unitOfWork
            , ICacheManager cacheManager
            , ILoggerService logger
            )
        {
            this.unitOfWork = unitOfWork;
            this.cacheManager = cacheManager;
            this.logger = logger;
        }

        private readonly IUnitOfWork unitOfWork;
        private readonly ICacheManager cacheManager;
        private readonly ILoggerService logger;

        public List<DepartmentResponse> TriggerList(DepartmentRequest departmentRequest)
        {
            var department = new Department(unitOfWork.Repository<edu_department_detail>());
            int totalcount;
            var queryableResults = department.GetDepartmentDetails(departmentRequest.pageCount, departmentRequest.currentIndex
                                                                  , out totalcount, departmentRequest.where
                                                                  , departmentRequest.orderBy, departmentRequest.propertyName
                                                                  ,departmentRequest.propertyValue);

            var queryList = queryableResults.ToList();

            var returnList = new List<DepartmentResponse>();
            for (var i = 0; i < queryList.Count; i++)
            {
                var itemConverted = Mapper.DynamicMap<DepartmentResponse>(queryList[i]);
                itemConverted.totalcount = totalcount;
                returnList.Add(itemConverted);
            }

            returnList = returnList.OrderBy(x => x.recordorder).ToList();
            return returnList;
        }
    }
}