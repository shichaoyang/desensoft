﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FuNong.DataAccess;
using FuNong.Framework.Caching;
using FuNong.Framework.Logger;
using FuNong.DataContract;
using AutoMapper;
using System.Data.Entity.Validation;

namespace FuNong.Service
{
    public class ModelService:IModelService
    {
        public ModelService(
              IUnitOfWork unitOfWork
            , ICacheManager cacheManager
            , ILoggerService logger)
        {
            this.unitOfWork = unitOfWork;
            this.cacheManager = cacheManager;
            this.logger = logger;

            this.modelRepo = unitOfWork.Repository<edu_model_detail>();
            this.modelRoleRepo = unitOfWork.Repository<edu_role_model>();
            this.modelDomain = new Model.Model(modelRepo,modelRoleRepo);
        }

        private readonly IUnitOfWork unitOfWork;
        private readonly ICacheManager cacheManager;
        private readonly ILoggerService logger;
        private readonly Model.Model modelDomain;
        private readonly IRepository<edu_role_model> modelRoleRepo;
        private readonly IRepository<edu_model_detail> modelRepo; 

        public List<ModelResponse> TriggerList(ModelRequest modelRequest)
        {
            int totalcount;

            var queryableResults = modelDomain.GetModelDetails(modelRequest.pageCount, modelRequest.currentIndex, out totalcount, modelRequest.where, modelRequest.orderBy, modelRequest.propertyName, modelRequest.propertyValue);

            var resultList = queryableResults.ToList();

            var returnList = new List<ModelResponse>();
            resultList.ForEach(item =>
            {
                var convertedItem = Mapper.DynamicMap<ModelResponse>(item);
                var parentModel = modelRepo.Get(x => x.id == convertedItem.parentid);
                convertedItem.totalcount = totalcount;
                convertedItem.parentname = (parentModel == null) ? "父菜单" : parentModel.modelname;
                returnList.Add(convertedItem);
            });

            returnList = returnList.OrderBy(x => x.recordorder).ToList();
            return returnList;
        }

        //构造模块树
        public string ContructModelTree(List<ModelResponse> modelList, string modelParent)
        {
            StringBuilder sbJson = new StringBuilder();

            int parentID;
            Int32.TryParse(modelParent, out parentID);
            var nodeListTop = modelList.Where(x => x.parentid == parentID).ToList();

            if (nodeListTop.Count > 0)
            {
                sbJson.Append("[");
                foreach (var item in nodeListTop)
                {
                    string id = item.id.ToString();
                    string name = item.modelname;

                    sbJson.Append("{");
                    sbJson.AppendFormat("\"id\":\"{0}\",", id);
                    sbJson.AppendFormat("\"text\":\"{0}\"", name);

                    string children = ContructModelTree(modelList, id);

                    if (!string.IsNullOrEmpty(children))
                    {
                        sbJson.Append(",\"children\":");
                        sbJson.Append(children);
                    }
                    sbJson.Append("},");
                }
                if (sbJson.ToString().EndsWith(","))
                {
                    sbJson.Remove(sbJson.Length - 1, 1);
                }
                sbJson.Append("]");
                return sbJson.ToString();
            }
            return string.Empty;
        }

        //获取当前角色下已分配的模块
        public List<ModelResponse> GetModelApply(ModelRoleRequest modelrolerequest)
        {
            var response = new List<ModelResponse>();

            if (modelrolerequest == null)
            {
                response.Add(new ModelResponse() { Success = false, Message = "角色ID不能为空！" });
                return response;
            }

            var modelList = modelDomain.GetModelApply(modelrolerequest.roleid).ToList();

            if (modelList != null)
            {
                modelList.ForEach(item =>
                {
                    var convertedItem = AutoMapper.Mapper.DynamicMap<ModelResponse>(item);
                    response.Add(convertedItem);
                });
            }

            return response;
        }


        //获取当前角色下未分配的模块
        public List<ModelResponse> GetModelUnApply(ModelRoleRequest modelrolerequest)
        {
            var response = new List<ModelResponse>();

            if (modelrolerequest == null)
            {
                response.Add(new ModelResponse() { Success = false, Message = "角色ID不能为空！" });
                return response;
            }

            var modelList = modelDomain.GetModelUnApply(modelrolerequest.roleid).ToList();

            if (modelList != null)
            {
                modelList.ForEach(item =>
                {
                    var convertedItem = AutoMapper.Mapper.DynamicMap<ModelResponse>(item);
                    response.Add(convertedItem);
                });
            }

            return response;
        }

        //通过模块列表获取模块-角色对应数据
        public List<ModelRoleResponse> GetRoleModelByIdList(ModelRoleRequest modelrolerequest)
        {
            var response = new List<ModelRoleResponse>();

            var roleModelList = modelDomain.GetRoleModelByIdList(modelrolerequest.roleid,modelrolerequest.modelidlist).ToList();

            if (roleModelList != null)
            {
                roleModelList.ForEach(item =>
                {
                    var convertedItem = AutoMapper.Mapper.DynamicMap<ModelRoleResponse>(item);
                    response.Add(convertedItem);
                });
            }
            return response;
        }
    }
}
