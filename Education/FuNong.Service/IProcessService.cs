﻿using System.Collections.Generic;
using FuNong.DataContract;
namespace FuNong.Service
{
    public interface IProcessService
    {
        List<ProcessResponse> AttachUnServedRequest(ProcessRequest request);
        List<ProcessResponse> AttachServedRequest(ProcessRequest request);

        List<ProcessResponse> TriggerList(ProcessRequest request);

        ProcessResponse AttachServiceRequest(ProcessRequest request);
    }
}
