﻿using AutoMapper;
using FuNong.DataAccess;
using FuNong.DataContract;
using FuNong.Framework.Caching;
using FuNong.Framework.Logger;
using FuNong.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FuNong.Service
{
    public class MachineService:IMachineService
    {
        public MachineService(
              IUnitOfWork unitOfWork
            , ICacheManager cacheManager
            , ILoggerService logger
            , IRepository<edu_school> companyRepo
            , IRepository<edu_users_view> userRepo
            , IRepository<edu_base_setting> settingRepo
            , IRepository<ds_machine_list> machineRepo
            )
        {
            this.unitOfWork = unitOfWork;
            this.cacheManager = cacheManager;
            this.logger = logger;

            this.companyRepo = companyRepo;
            this.userRepo = userRepo;
            this.settingRepo = settingRepo;
            this.machineRepo = machineRepo;
        }

        private readonly IUnitOfWork unitOfWork;
        private readonly ICacheManager cacheManager;
        private readonly ILoggerService logger;

        private readonly IRepository<edu_school> companyRepo;
        private readonly IRepository<edu_users_view> userRepo;
        private readonly IRepository<edu_base_setting> settingRepo;
        private readonly IRepository<ds_machine_list> machineRepo;

        //获取设备列表
        public List<MachineResponse> TriggerList(MachineRequest machineRequest)
        {
            var machine = new Machine(unitOfWork.Repository<ds_machine_list>());
            int totalcount;

            var queryableResults = machine.GetMachineList(machineRequest.pageCount
                                                        , machineRequest.currentIndex
                                                        , out totalcount
                                                        , machineRequest.where
                                                        , machineRequest.orderBy
                                                        , machineRequest.propertyName
                                                        , machineRequest.propertyValue);

            var resultList = queryableResults.ToList();

            var returnList = new List<MachineResponse>();
            resultList.ForEach(item =>
            {
                var convertedItem = Mapper.DynamicMap<MachineResponse>(item);
                convertedItem.totalcount = totalcount;

                convertedItem.company_name = companyRepo.Get(x => x.id == item.school_id).name;

                returnList.Add(convertedItem);
            });

            returnList = returnList.OrderBy(x => x.recordorder).ToList();
            return returnList;
        }

        //获取控制设备配置列表
        public List<ControllerResponse> TriggerList(ControllerRequest controllerRequest)
        {
            var machine = new Controller(unitOfWork.Repository<ds_machine_controller>());
            int totalcount;

            var queryableResults = machine.GetMachineList(controllerRequest.pageCount
                                                        , controllerRequest.currentIndex
                                                        , out totalcount
                                                        , controllerRequest.where
                                                        , controllerRequest.orderBy
                                                        , controllerRequest.propertyName
                                                        , controllerRequest.propertyValue);

            var resultList = queryableResults.ToList();

            var returnList = new List<ControllerResponse>();
            resultList.ForEach(item =>
            {
                var convertedItem = Mapper.DynamicMap<ControllerResponse>(item);

                var selectedMachine = machineRepo.Get(x => x.machine_id == item.machine_id);
                if(selectedMachine==null) 
                    selectedMachine = new ds_machine_list();

                var selectedCompany = companyRepo.Get(x => x.id == selectedMachine.school_id);
                if (selectedCompany == null)
                    selectedCompany = new edu_school();

                 var machine_type = item.machine_type.ToString();
                 var selectedType = settingRepo.Get(x => x.settinggroup == "machinetype" && x.settingvalue1 == machine_type);
                 if (selectedType == null)
                     selectedType = new edu_base_setting();

                 convertedItem.type_name = selectedType.settingname;

                convertedItem.company_id = selectedMachine.school_id;
                convertedItem.company_name = selectedCompany.name;
                convertedItem.machine_name = selectedMachine.machine_name;
                convertedItem.totalcount = totalcount;

                returnList.Add(convertedItem);
            });

            returnList = returnList.OrderBy(x => x.recordorder).ToList();
            return returnList;
        }

        //获取采集设备配置列表
        public List<CollectorResponse> TriggerList(CollectorRequest collectorRequest)
        {
            var machine = new Collector(unitOfWork.Repository<ds_machine_collector>());
            int totalcount;

            var queryableResults = machine.GetMachineList(collectorRequest.pageCount
                                                        , collectorRequest.currentIndex
                                                        , out totalcount
                                                        , collectorRequest.where
                                                        , collectorRequest.orderBy
                                                        , collectorRequest.propertyName
                                                        , collectorRequest.propertyValue);

            var resultList = queryableResults.ToList();

            var returnList = new List<CollectorResponse>();
            resultList.ForEach(item =>
            {
                var convertedItem = Mapper.DynamicMap<CollectorResponse>(item);

                var selectedMachine = machineRepo.Get(x => x.machine_id == item.machine_id);
                if (selectedMachine == null)
                    selectedMachine = new ds_machine_list();

                var selectedCompany = companyRepo.Get(x => x.id == selectedMachine.school_id);
                if (selectedCompany == null)
                    selectedCompany = new edu_school();

                var selectedSetting = settingRepo.Get(x => x.settinggroup == "param" && x.settingvalue1 == item.param_id);
                if(selectedSetting==null)
                    selectedSetting = new edu_base_setting();

                var machine_type = item.machine_type.ToString();
                var selectedType = settingRepo.Get(x => x.settinggroup == "machinetype" && x.settingvalue1 == machine_type);
                if (selectedType == null)
                    selectedType = new edu_base_setting();

                convertedItem.type_name = selectedType.settingname;

                convertedItem.company_id = selectedMachine.school_id;
                convertedItem.company_name = selectedCompany.name;
                convertedItem.machine_name = selectedMachine.machine_name;
                convertedItem.param_name = selectedSetting.settingname;

                convertedItem.totalcount = totalcount;

                returnList.Add(convertedItem);
            });

            returnList = returnList.OrderBy(x => x.recordorder).ToList();
            return returnList;
        }
        
    }
}
