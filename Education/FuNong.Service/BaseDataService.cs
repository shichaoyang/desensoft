﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FuNong.DataAccess;
using FuNong.Framework.Caching;
using FuNong.Framework.Logger;
using FuNong.DataContract;
using AutoMapper;

namespace FuNong.Service
{
    public class BaseDataService : FuNong.Service.IBaseDataService
    {
        public BaseDataService(IUnitOfWork unitOfWork, ICacheManager cacheManager, ILoggerService logger)
        {
            this.unitOfWork = unitOfWork;
            this.cacheManager = cacheManager;
            this.logger = logger;

            this.baseRepo = unitOfWork.Repository<edu_base_setting>();

            this.provinceRepo = unitOfWork.Repository<edu_area_province>();
            this.cityRepo = unitOfWork.Repository<edu_area_city>();
            this.districtRepo = unitOfWork.Repository<edu_area_district>();
            this.villageRepo = unitOfWork.Repository<edu_area_village>();
            this.logRepo = unitOfWork.Repository<edu_log>();
            this.iconRepo = unitOfWork.Repository<edu_icon>();
            this.fieldRepo = unitOfWork.Repository<edu_field_control>();
            this.roleRepo = unitOfWork.Repository<edu_role_detail>();

            this.baseDataDomain = new Model.BaseData(baseRepo, villageRepo);
            this.logDomain = new Model.BaseQuery<edu_log, DateTime?>(logRepo);
            this.iconDomain = new Model.BaseQuery<edu_icon, DateTime?>(iconRepo);
            this.fileDomain = new Model.BaseQuery<edu_field_control, DateTime?>(fieldRepo);
        }

        private readonly IUnitOfWork unitOfWork;
        private readonly ICacheManager cacheManager;
        private readonly ILoggerService logger;

        private readonly IRepository<edu_base_setting> baseRepo;
        
        private readonly IRepository<edu_area_province> provinceRepo;
        private readonly IRepository<edu_area_city> cityRepo;
        private readonly IRepository<edu_area_district> districtRepo;
        private readonly IRepository<edu_area_village> villageRepo;
        private readonly IRepository<edu_log> logRepo;
        private readonly IRepository<edu_icon> iconRepo;
        private readonly IRepository<edu_field_control> fieldRepo;
        private readonly IRepository<edu_role_detail> roleRepo;

        private readonly Model.BaseData baseDataDomain;
        private readonly Model.BaseQuery<edu_log,DateTime?> logDomain;
        private readonly Model.BaseQuery<edu_icon, DateTime?> iconDomain;
        private readonly Model.BaseQuery<edu_field_control, DateTime?> fileDomain;

        //获取配置信息列表
        public List<BaseSettingResponse> GetSettingList(BaseSettingRequest request)
        {
            int totalcount;

            var queryableResults = baseDataDomain.GetSettingList(request.pageCount, request.currentIndex, out totalcount, request.where, request.orderBy, request.propertyName, request.propertyValue);

            var resultList = queryableResults.ToList();

            var returnList = new List<BaseSettingResponse>();
            resultList.ForEach(item =>
            {
                var convertedItem = Mapper.DynamicMap<BaseSettingResponse>(item);

                convertedItem.totalcount = totalcount;

                returnList.Add(convertedItem);
            });

            returnList = returnList.OrderBy(x => x.settinggroup).ToList();
            return returnList;
        }

        //获取日志列表
        public List<LogResponse> GetLogList(LogRequest request)
        {
            int totalcount;

            var queryableResults = logDomain.GetQuery(request.pageCount, request.currentIndex, out totalcount, request.where, request.orderBy, request.propertyName, request.propertyValue);

            var resultList = queryableResults.ToList();

            var returnList = new List<LogResponse>();
            resultList.ForEach(item =>
            {
                var convertedItem = Mapper.DynamicMap<LogResponse>(item);

                convertedItem.totalcount = totalcount;

                returnList.Add(convertedItem);
            });

            returnList = returnList.OrderByDescending(x => x.Date).ToList();
            return returnList;
        }

        //获取图标信息
        public List<IconResponse> GetIconList(IconRequest request)
        {
            int totalcount;

            var queryableResults = iconDomain.GetQuery(request.pageCount, request.currentIndex, out totalcount, request.where, request.orderBy, request.propertyName, request.propertyValue);

            var resultList = queryableResults.ToList();

            var returnList = new List<IconResponse>();
            resultList.ForEach(item =>
            {
                var convertedItem = Mapper.DynamicMap<IconResponse>(item);
                returnList.Add(convertedItem);
            });
            returnList = returnList.OrderBy(x => x.iconname).ToList();
            return returnList;
        }

        //获取数据库字段权限列表
        public List<FieldControlResponse> GetFieldControlResponse(FieldControlRequest request)
        {
            int totalcount;

            var queryableResults = fileDomain.GetQuery(request.pageCount, request.currentIndex, out totalcount, request.where, request.orderBy, request.propertyName, request.propertyValue);

            var resultList = queryableResults.ToList();

            var returnList = new List<FieldControlResponse>();
            resultList.ForEach(item =>
            {
                var convertedItem = Mapper.DynamicMap<FieldControlResponse>(item);
                var roleid = item.roleid;
                var role = roleRepo.Get(x => x.id == roleid) ?? new edu_role_detail();
                convertedItem.rolename = role.rolename;
                convertedItem._parentId = item.parentid.GetValueOrDefault();
                returnList.Add(convertedItem);
            });
            returnList = returnList.OrderBy(x => x.databasename).ToList();
            return returnList;
        }

        //获取村镇列表
        public List<VillageResponse> GetVillageList(VillageRequest request)
        {
            int totalcount;

            var queryableResults = baseDataDomain.GetVillageList(request.pageCount, request.currentIndex, out totalcount, request.where, request.orderBy, request.propertyName, request.propertyValue);

            var resultList = queryableResults.ToList();

            var returnList = new List<VillageResponse>();
            resultList.ForEach(item =>
            {
                var convertedItem = Mapper.DynamicMap<VillageResponse>(item);

                //区县
                var district = districtRepo.Get(x => x.id == convertedItem.districtid);
                if(district==null) district = new edu_area_district();

                //市
                var city = (from p in districtRepo.GetMany(x => x.id == convertedItem.districtid)
                                          join q in cityRepo.GetMany(x => x.id != 0)
                                          on p.cityid equals q.id
                                          select q).FirstOrDefault();
                if(city==null) city = new edu_area_city();

                //省
                var province = (from p in districtRepo.GetMany(x => x.id == convertedItem.districtid)
                                              join q in cityRepo.GetMany(x => x.id != 0)
                                              on p.cityid equals q.id
                                              join x in provinceRepo.GetMany(x => x.id != 0)
                                              on q.provinceid equals x.id
                                              select x).FirstOrDefault();
                if(province ==null) province = new edu_area_province();

                convertedItem.districtname = district.name;
                convertedItem.cityname = city.name;
                convertedItem.provincename = province.name;

                convertedItem.districtid = district.id;
                convertedItem.cityid = city.id;
                convertedItem.provinceid = province.id;

                convertedItem.totalcount = totalcount;

                returnList.Add(convertedItem);
            });
            returnList = returnList.OrderBy(x => x.updatetime).ToList();
            return returnList;
        }
    }
}
