﻿using System;
using System.Collections.Generic;
using FuNong.DataAccess;
using FuNong.DataContract;
namespace FuNong.Service
{
    public interface IDepartmentService
    {
        List<DepartmentResponse> TriggerList(DepartmentRequest departmentRequest);
    }
}
