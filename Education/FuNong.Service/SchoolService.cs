﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FuNong.DataAccess;
using FuNong.Framework.Caching;
using FuNong.Framework.Logger;
using FuNong.Model;
using FuNong.DataContract;
using AutoMapper;

namespace FuNong.Service
{
    public class SchoolService : FuNong.Service.ISchoolService
    {
        public SchoolService(IUnitOfWork unitOfWork, ICacheManager cacheManager, ILoggerService logger,IBaseDataService baseDataService)
        {
            this.unitOfWork = unitOfWork;
            this.cacheManager = cacheManager;
            this.logger = logger;

            this.schoolRepo = unitOfWork.Repository<edu_school>();
            this.gradeRepo = unitOfWork.Repository<edu_school_grade>();
            this.classRepo = unitOfWork.Repository<edu_school_class>();
            this.settingRepo = unitOfWork.Repository<edu_base_setting>();
            this.provinceRepo = unitOfWork.Repository<edu_area_province>();
            this.cityRepo = unitOfWork.Repository<edu_area_city>();
            this.districtRepo = unitOfWork.Repository<edu_area_district>();
            this.villageRepo = unitOfWork.Repository<edu_area_village>();
            this.userRepo = unitOfWork.Repository<edu_users_view>();
            this.courseTableRepo = unitOfWork.Repository<edu_course_table>();
            this.scApplyRepo = unitOfWork.Repository<edu_student_class_apply>();
            this.cardChangeRepo = unitOfWork.Repository<edu_card_change>();
            this.videoRepo = unitOfWork.Repository<edu_video_all>();

            this.schoolDomain = new BaseQuery<edu_school,DateTime?>(schoolRepo);
            this.gradeDomain = new BaseQuery<edu_school_grade, DateTime?>(gradeRepo);
            this.classDomain = new BaseQuery<edu_school_class, DateTime?>(classRepo);
            this.courseTableDomain = new BaseQuery<edu_course_table, DateTime?>(courseTableRepo);
            this.scApplyDomain = new BaseQuery<edu_student_class_apply, DateTime?>(scApplyRepo);
            this.cardChangeDomain = new BaseQuery<edu_card_change, DateTime?>(cardChangeRepo);
            this.videoDomain = new BaseQuery<edu_video_all, DateTime?>(videoRepo);
        }

        private readonly IUnitOfWork unitOfWork;
        private readonly ICacheManager cacheManager;
        private readonly ILoggerService logger;

        private readonly IRepository<edu_school> schoolRepo;
        private readonly IRepository<edu_school_grade> gradeRepo;
        private readonly IRepository<edu_school_class> classRepo;
        private readonly IRepository<edu_base_setting> settingRepo;
        private readonly IRepository<edu_course_table> courseTableRepo;
        private readonly IRepository<edu_student_class_apply> scApplyRepo;
        private readonly IRepository<edu_card_change> cardChangeRepo;
        private readonly IRepository<edu_video_all> videoRepo;

        private readonly IRepository<edu_area_province> provinceRepo;
        private readonly IRepository<edu_area_city> cityRepo;
        private readonly IRepository<edu_area_district> districtRepo;
        private readonly IRepository<edu_area_village> villageRepo;
        private readonly IRepository<edu_users_view> userRepo;

        private readonly BaseQuery<edu_school,DateTime?> schoolDomain;
        private readonly BaseQuery<edu_school_grade, DateTime?> gradeDomain;
        private readonly BaseQuery<edu_school_class, DateTime?> classDomain;
        private readonly BaseQuery<edu_course_table, DateTime?> courseTableDomain;
        private readonly BaseQuery<edu_student_class_apply, DateTime?> scApplyDomain;
        private readonly BaseQuery<edu_card_change, DateTime?> cardChangeDomain;
        private readonly BaseQuery<edu_video_all, DateTime?> videoDomain;

        //获取学校列表
        public List<SchoolResponse> GetSchoolResponse(SchoolRequest request)
        {
            int totalCount;
            var queryableResults = schoolDomain.GetQuery(request.pageCount, request.currentIndex, out totalCount, request.where, request.orderBy, request.propertyName, request.propertyValue);
            
            var resultList = queryableResults.ToList();
            var returnList = new List<SchoolResponse>();
            resultList.ForEach(item =>
            {
                var convertedItem = Mapper.DynamicMap<SchoolResponse>(item);

                var schoolTypeId = item.typeid.ToString();
                var schoolStateId = item.stateid.ToString();

                var schoolTypeGroup = settingRepo.Get(x => x.settinggroup == "schooltype" && x.settingvalue1 == schoolTypeId) ?? new edu_base_setting();
                var schoolStateGroup = settingRepo.Get(x => x.settinggroup == "state" && x.settingvalue1 == schoolStateId) ?? new edu_base_setting();

                //区县
                var district = districtRepo.Get(x => x.id == item.districtid)??new edu_area_district();
                //市
                var city = cityRepo.Get(x => x.id == district.cityid) ?? new edu_area_city();
                //省
                var province = provinceRepo.Get(x => x.id == city.provinceid) ?? new edu_area_province();

                convertedItem.typename = schoolTypeGroup.settingname;
                convertedItem.statename = schoolStateGroup.settingname;

                convertedItem.districtid = district.id;
                convertedItem.districtname = district.name;
                convertedItem.cityid = city.id;
                convertedItem.cityname = city.name;
                convertedItem.provinceid = province.id;
                convertedItem.provincename = province.name;

                convertedItem.totalcount = totalCount;
                
                returnList.Add(convertedItem);
            });

            returnList = returnList.OrderBy(x => x.recordorder).ToList();
            return returnList;
        }

        //获取年级列表
        public List<GradeResponse> GetGradeResponse(GradeRequest request)
        {
            int totalCount;
            var queryableResults = gradeDomain.GetQuery(request.pageCount, request.currentIndex, out totalCount, request.where, request.orderBy, request.propertyName, request.propertyValue);

            var resultList = queryableResults.ToList();
            var returnList = new List<GradeResponse>();
            resultList.ForEach(item =>
            {
                var convertedItem = Mapper.DynamicMap<GradeResponse>(item);

                var gradeStateId = item.stateid.ToString();
                var schoolStateGroup = settingRepo.Get(x => x.settinggroup == "state" && x.settingvalue1 == gradeStateId) ?? new edu_base_setting();
                var school = schoolRepo.Get(x => x.id == item.schoolid) ?? new edu_school();

                convertedItem.statename = schoolStateGroup.settingname;
                convertedItem.schoolname = school.name;

                convertedItem.totalcount = totalCount;

                returnList.Add(convertedItem);
            });

            returnList = returnList.OrderBy(x => x.recordorder).ToList();
            return returnList;
        }

        //获取班级列表
        public List<ClassResponse> GetClassResponse(ClassRequest request)
        {
            int totalCount;
            var queryableResults = classDomain.GetQuery(request.pageCount, request.currentIndex, out totalCount, request.where, request.orderBy, request.propertyName, request.propertyValue);

            var resultList = queryableResults.ToList();
            var returnList = new List<ClassResponse>();
            resultList.ForEach(item =>
            {
                var convertedItem = Mapper.DynamicMap<ClassResponse>(item);

                var stateId = item.stateid.ToString();
                var state = settingRepo.GetMany(x => x.settinggroup == "state" && x.settingvalue1 == stateId).FirstOrDefault();
                if (state == null) state = new edu_base_setting();

                var graduateId = item.isgraduate.ToString();
                var graduate = settingRepo.GetMany(x => x.settinggroup == "judge" && x.settingvalue1 == graduateId).FirstOrDefault();
                if (graduate == null) graduate = new edu_base_setting();

                var upgradeId = item.isupgrade.ToString();
                var upgrade = settingRepo.GetMany(x => x.settinggroup == "judge" && x.settingvalue1 == upgradeId).FirstOrDefault();
                if (upgrade == null) upgrade = new edu_base_setting();

                var schoolStateGroup = settingRepo.GetMany(x => x.settinggroup == "state" && x.settingvalue1 == stateId).FirstOrDefault() ?? new edu_base_setting();
                var grade = gradeRepo.GetMany(x => x.id == item.gradeid).FirstOrDefault() ?? new edu_school_grade();
                var school = schoolRepo.GetMany(x => x.id == grade.schoolid).FirstOrDefault() ?? new edu_school();

                convertedItem.schoolid = school.id;
                convertedItem.schoolname = school.name;
                convertedItem.gradename = grade.name;

                convertedItem.statename = state.settingname;
                convertedItem.isgraduatename = graduate.settingname;
                convertedItem.isupgradename = upgrade.settingname;

                convertedItem.totalcount = totalCount;

                returnList.Add(convertedItem);
            });

            returnList = returnList.OrderBy(x => x.recordorder).ToList();
            return returnList;
        }

        //获取课程表设置相关的数据
        public CourseTableResponse GetCourseTableFillData(CourseTableRequest request)
        {
            int totalCount;
            var queryableResults = courseTableDomain.GetQuery(request.pageCount, request.currentIndex, out totalCount, request.where, request.orderBy, request.propertyName, request.propertyValue);

            var result = queryableResults.ToList().FirstOrDefault();
            var convertedItem = new CourseTableResponse();

            var course = settingRepo.GetMany(x => x.settinggroup == "course").ToList();

            var period = settingRepo.GetMany(x => x.settinggroup == "period").ToList();

            var week = settingRepo.GetMany(x => x.settinggroup == "week").ToList();

            var user = userRepo.GetMany(x => x.roleid == request.roleid).ToList();

            var grade = gradeRepo.GetMany(x => x.schoolid == request.schoolid).ToList();

            var classe = (from p in classRepo.GetMany(x => x.id != 0)
                          join x in gradeRepo.GetMany(x => x.schoolid == request.schoolid)
                          on p.gradeid equals x.id
                          select p).ToList();

            convertedItem.courselist = course;
            convertedItem.weeklist = week;
            convertedItem.periodlist = period;
            convertedItem.userlist = user;

            convertedItem.gradelist = grade;
            convertedItem.classlist = classe;

            return convertedItem;
        }

        //获取课程表设置相关的数据
        public CourseTableResponse GetMasterByClassid(CourseTableRequest request)
        {
            int totalCount;
            var queryableResults = courseTableDomain.GetQuery(request.pageCount, request.currentIndex, out totalCount, request.where, request.orderBy, request.propertyName, request.propertyValue);

            var result = queryableResults.ToList().FirstOrDefault();
            var convertedItem = new CourseTableResponse();
            //var user = userRepo.Get(x => x.classid == request.classid && x.schoolid == request.schoolid) ?? new edu_users_view();

            //convertedItem.masterid = user.uid;
            //convertedItem.mastername = user.nickname;
            //convertedItem.mastercode = user.personcode;

            return convertedItem;
        }

        //获取调班列表
        public List<StudentClassApplyResponse> GetStudentClassApplyResponse(StudentClassApplyRequest request)
        {
            int totalCount;
            var queryableResults = scApplyDomain.GetQuery(request.pageCount, request.currentIndex, out totalCount, request.where, request.orderBy, request.propertyName, request.propertyValue);

            var resultList = queryableResults.ToList();
            var returnList = new List<StudentClassApplyResponse>();
            resultList.ForEach(item =>
            {
                var convertedItem = Mapper.DynamicMap<StudentClassApplyResponse>(item);

                var user = userRepo.Get(x => x.uid == item.uid) ?? new edu_users_view();
                var classe = classRepo.Get(x => x.id == item.classid) ?? new edu_school_class();
                var grade = gradeRepo.Get(x => x.id == classe.gradeid) ?? new edu_school_grade();
                var school = schoolRepo.Get(x => x.id == request.schoolid) ?? new edu_school();

                convertedItem.username = user.username;
                convertedItem.nickname = user.nickname;
                convertedItem.personcode = user.personcode;
                convertedItem.classname = classe.classname;
                convertedItem.gradename = grade.name;
                convertedItem.schoolname = school.name;

                convertedItem.gradeid = grade.id;
                convertedItem.classid = item.classid;
                
                convertedItem.totalcount = totalCount;

                returnList.Add(convertedItem);
            });

            returnList = returnList.OrderBy(x => x.recordorder).ToList();
            return returnList;
        }

        //卡变更管理
        public List<CardChangeResponse> GetCardChangeResponse(CardChangeRequest request)
        {
            int totalCount;
            var queryableResults = cardChangeDomain.GetQuery(request.pageCount, request.currentIndex, out totalCount, request.where, request.orderBy, request.propertyName, request.propertyValue);

            var resultList = queryableResults.ToList();
            var returnList = new List<CardChangeResponse>();
            resultList.ForEach(item =>
            {
                var convertedItem = Mapper.DynamicMap<CardChangeResponse>(item);

                var school   = schoolRepo.Get(x => x.id == request.schoolid) ?? new edu_school();
                var district = districtRepo.Get(x => x.id == school.districtid) ?? new edu_area_district();
                var city     = cityRepo.Get(x => x.id == district.cityid) ?? new edu_area_city();
                var province = provinceRepo.Get(x => x.id == city.provinceid) ?? new edu_area_province();

                var cardtypeid = item.cardtypeid.ToString();
                var operationtypeid = item.operationtypeid.ToString();

                var cardtype = settingRepo.Get(x => x.settinggroup == "cardtype" && x.settingvalue1 == cardtypeid) ?? new edu_base_setting();
                var operationtype = settingRepo.Get(x => x.settinggroup == "operationtype" && x.settingvalue1 == operationtypeid) ?? new edu_base_setting();

                convertedItem.schoolid = school.id;
                convertedItem.schoolname = school.name;

                convertedItem.cardtypename = cardtype.settingname;
                convertedItem.operationtypename = operationtype.settingname;

                convertedItem.totalcount = totalCount;

                returnList.Add(convertedItem);
            });

            returnList = returnList.OrderBy(x => x.recordorder).ToList();
            return returnList;
        }

        //视频监控配置管理
        public List<VideoResponse> GetVideoResponse(VideoRequest request)
        {
            int totalCount;
            var queryableResults = videoDomain.GetQuery(request.pageCount, request.currentIndex, out totalCount, request.where, request.orderBy, request.propertyName, request.propertyValue);

            var resultList = queryableResults.ToList();
            var returnList = new List<VideoResponse>();
            resultList.ForEach(item =>
            {
                var convertedItem = Mapper.DynamicMap<VideoResponse>(item);


                var school = schoolRepo.Get(x => x.id == item.schoolid) ?? new edu_school();

                var video = settingRepo.Get(x => x.settinggroup == "video" && x.settingvalue1 == item.videocompany) ?? new edu_base_setting();

                convertedItem.videocompanyshow = video.settingname;

                convertedItem.companyname = school.name;

                convertedItem.jin = school.jin;
                convertedItem.wei = school.wei;

                convertedItem.totalcount = totalCount;

                returnList.Add(convertedItem);
            });

            return returnList;
        }
    }
}
