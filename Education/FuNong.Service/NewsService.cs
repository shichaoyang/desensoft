﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FuNong.DataAccess;
using FuNong.Framework.Caching;
using FuNong.Framework.Logger;
using FuNong.DataContract;
using AutoMapper;

namespace FuNong.Service
{
    public class NewsService : INewsService
    {
        public NewsService(
              IUnitOfWork unitOfWork
            , ICacheManager cacheManager
            , ILoggerService logger
          )
        {
            this.unitOfWork = unitOfWork;
            this.cacheManager = cacheManager;
            this.logger = logger;

            this.newsTypeRepo = unitOfWork.Repository<edu_cms_news_type>();
            this.newsRepo = unitOfWork.Repository<edu_cms_news>();
            this.soilTypeRepo = unitOfWork.Repository<funong_soil_type>();
            this.mapRepo = unitOfWork.Repository<funong_news_soil_map>();
            this.villageRepo = unitOfWork.Repository<edu_area_village>();
            this.newssoilmapRepo = unitOfWork.Repository<funong_news_soil_map>();
            this.userRepo = unitOfWork.Repository<edu_users_view>();
            this.schoolRepo = unitOfWork.Repository<edu_school>();
            this.settingRepo = unitOfWork.Repository<edu_base_setting>();

            this.newsDomain = new Model.News(newsRepo,newsTypeRepo,mapRepo);
            this.baseDataDomain = new Model.BaseData(settingRepo,villageRepo);
        }

        private readonly IUnitOfWork unitOfWork;
        private readonly ICacheManager cacheManager;
        private readonly ILoggerService logger;
        
        private readonly Model.News newsDomain;
        private readonly Model.BaseData baseDataDomain;

        private readonly IRepository<edu_cms_news> newsRepo;
        private readonly IRepository<edu_cms_news_type> newsTypeRepo;
        private readonly IRepository<funong_soil_type> soilTypeRepo;
        private readonly IRepository<funong_news_soil_map> mapRepo;
        private readonly IRepository<edu_users_view> userRepo;
        private readonly IRepository<funong_news_soil_map> newssoilmapRepo;
        private readonly IRepository<edu_school> schoolRepo;
        private readonly IRepository<edu_base_setting> settingRepo;
        private readonly IRepository<edu_area_village> villageRepo;

        #region 注释部分
        //获取资讯首页信息
        //-2-List<NewsResponse>  今日热点 4 条
        //-1-List<NewsResponse>  今日推荐 8条
        //2-List<NewsResponse>   土地市场 10条
        //3-List<NewsResponse>   媒体报道 10条
        //4-List<NewsResponse>   土地新闻 10条
        //5-List<NewsResponse>   流转资讯 10条
        //6-List<NewsResponse>   图片新闻 10条

        //8-List<NewsResponse>   农业新闻 10条
        //9-List<NewsResponse>   农业项目 10条
        //10-List<NewsResponse>  聚焦三农 10条
        //11-List<NewsResponse>  视频新闻 10条
        //public Dictionary<int, List<NewsResponse>> TriggerList(NewsTypeRequest newsTypeRequest)
        //{
        //    int totalcount;
        //    Dictionary<int, List<NewsResponse>> dict = new Dictionary<int, List<NewsResponse>>();

        //    //今日热点
        //    var todayHot = newsDomain.GetNewsList(4, 0, out totalcount, x => x.todaytop == 1 && x.isapprove == 1, x => x.updatetime, string.Empty, string.Empty);
        //    var hotList = todayHot.ToList();
        //    if (hotList.Count < 4)
        //        for (var i = 0; i < 4 - hotList.Count; i++)
        //            hotList.Add(new edu_cms_news() { newscontent = string.Empty, newsnote = string.Empty, newstitle = string.Empty, typeid = -1 });
        //    dict.Add(-2, ListConversion(hotList));

        //    //今日推荐
        //    var todayRecommand = newsDomain.GetNewsList(8, 0, out totalcount, x => x.todaytop != 1 && x.todayrecommand == 1 && x.isapprove == 1, x => x.updatetime, string.Empty, string.Empty);
        //    var recommandList = todayRecommand.ToList();
        //    if (recommandList.Count < 8)
        //        for (var i = 0; i < 8 - recommandList.Count; i++)
        //            recommandList.Add(new edu_cms_news() { newscontent = string.Empty, newsnote = string.Empty, newstitle = string.Empty, typeid = -1 });

        //    var returnList = new List<NewsResponse>();
        //    recommandList.ForEach(item =>
        //    {
        //        var convertedItem = Mapper.DynamicMap<NewsResponse>(item);

        //        var newsType = newsTypeRepo.Get(x => x.id == item.typeid);
        //        if(newsType==null)
        //            newsType = new edu_cms_news_type();

        //        convertedItem.typename = newsType.typename;
        //        returnList.Add(convertedItem);
        //    });
        //    dict.Add(-1, returnList);

        //    //获取其他的子项目
        //    var newsTypeList = GetTypeResponseList(newsTypeRequest);

        //    if (newsTypeList != null)
        //    {
        //        newsTypeList.ForEach(item =>
        //        {
        //            var newsList = newsDomain.GetNewsList(10, 0, out totalcount, x => x.todaytop != 1 && x.todayrecommand != 1 && x.typeid == item.id && x.isapprove == 1, x => x.updatetime, string.Empty, string.Empty);
        //            var nList = newsList.ToList();
        //            if (nList.Count < 10)
        //                for (var i = 0; i < 10 - nList.Count; i++)
        //                    nList.Add(new edu_cms_news() { newscontent = string.Empty, newsnote = string.Empty, newstitle = string.Empty, typeid = -1 });

        //            dict.Add(item.id, ListConversion(nList));
        //        });
        //    }
        //    return dict;
        //}
        #endregion

        public Dictionary<NewsTypeResponse, List<NewsResponse>> TriggerList(NewsTypeRequest newsTypeRequest)
        {
            var totalcount = 0;
            var dict = new Dictionary<NewsTypeResponse, List<NewsResponse>>();

            //今日热点
            var todayHot = newsDomain.GetNewsList(4, 0, out totalcount, x => x.todaytop == 1 , x => x.updatetime, string.Empty, string.Empty);
            var hotList = todayHot.ToList();
            if (hotList.Count < 4)
            {
                for (var i = 0; i < 4 - hotList.Count; i++)
                {
                    hotList.Add(new edu_cms_news() { newscontent = string.Empty, newsnote = string.Empty, newstitle = string.Empty, typeid = "-2" });
                }
            }
            dict.Add(new NewsTypeResponse() { id = -2 }, ListConversion(hotList));

            //今日推荐
            var todayRecommand = newsDomain.GetNewsList(8, 0, out totalcount, x => x.todaytop != 1 && x.todayrecommand == 1 , x => x.updatetime, string.Empty, string.Empty);
            var recommandList = todayRecommand.ToList();
            if (recommandList.Count < 8)
            {
                for (var i = 0; i < 8 - recommandList.Count; i++)
                {
                    recommandList.Add(new edu_cms_news() { newscontent = string.Empty, newsnote = string.Empty, newstitle = string.Empty, typeid = "-1" });
                }
            }

            var returnList = new List<NewsResponse>();
            recommandList.ForEach(item =>
            {
                var convertedItem = Mapper.DynamicMap<NewsResponse>(item);


                var selectedSetting = settingRepo.Get(x => x.settinggroup == "newstype" && x.settingvalue1 == item.typeid);
                if (selectedSetting == null)
                    selectedSetting = new edu_base_setting();
                convertedItem.typename = selectedSetting.settingname;

                returnList.Add(convertedItem);
            });
            dict.Add(new NewsTypeResponse() { id = -1 }, returnList);

            //获取其他的子项目
            var newsTypeList = settingRepo.GetMany(x => x.settinggroup == "newstype").ToList();

            if (newsTypeList != null)
            {
                newsTypeList.ForEach(item =>
                {
                    var newsList = newsDomain.GetNewsList(10, 0, out totalcount,  x=>x.typeid == item.settingvalue1 , x => x.updatetime, string.Empty, string.Empty);
                    var nList = newsList.ToList();
                    if (nList.Count < 10)
                    {
                        for (var i = 0; i < 10 - nList.Count; i++)
                        {
                            nList.Add(new edu_cms_news() { newscontent = string.Empty, newsnote = string.Empty, newstitle = string.Empty, typeid = string.Empty, pageimg = string.Empty });
                        }
                    }
                    dict.Add(new NewsTypeResponse() { typename=item.settingname, typenote=item.settingvalue1 }, ListConversion(nList));
                });
            }
            return dict;
        }

        public List<NewsResponse> GetNewsBySoilTypeId(NewsRequest request)
        {
            return null;
            //var resultQuery = from p in newssoilmapRepo.GetMany(x => x.id != 0)
            //                  join x in newsTypeRepo.GetMany(x => x.id != 0)
            //                  on p.newstypeid equals x.parentid
            //                  join n in newsRepo.GetMany(x => x.id != 0)
            //                  on x.id equals n.typeid
            //                  where p.soiltypeid == request.soiltypeid
            //                  select new
            //                  {
            //                      n.id,
            //                      n.newstitle,
            //                      n.updatetime,
            //                      n.newscontent,
            //                      n.newsnote,
            //                      n.isapprove,
            //                      n.todaytop,
            //                      x.typename
            //                  };
            //int skipRows = 0;
            //if (request.currentIndex > 0) skipRows = request.currentIndex * request.pageCount;

            //var resultList = resultQuery.OrderBy(x=>x.updatetime).Skip(skipRows).Take(request.pageCount).ToList();
            //var returnList = new List<NewsResponse>();

            //resultList.ForEach(item =>
            //{
            //    var convertedItem = Mapper.DynamicMap<NewsResponse>(item);
            //    returnList.Add(convertedItem);
            //});
            //return returnList;
        }            


        public NewsResponse GetNewDetailById(NewsRequest request)
        {
            var responseEntity = newsRepo.Get(x => x.id == request.newsid);
            var converteditem =  Mapper.DynamicMap<NewsResponse>(responseEntity);

            var selectedSetting = settingRepo.Get(x => x.settinggroup == "newstype" && x.settingvalue1 == responseEntity.typeid);
            if (selectedSetting == null)
                selectedSetting = new edu_base_setting();

            converteditem.typename = selectedSetting.settingname;

            var userobj = userRepo.Get(x => x.uid == responseEntity.uid) ?? new edu_users_view();
            converteditem.uname = userobj.nickname ?? string.Empty;

            //var schoolobj = schoolRepo.Get(x => x.id == userobj.schoolid) ?? new edu_school();
            //converteditem.schoolname = schoolobj.name ?? string.Empty;

            return converteditem;
        }

        public List<NewsResponse> GetNewsList(NewsRequest request)
        {
            int totalcount;
            var resultQuery = newsDomain.GetNewsList(request.pageCount, request.currentIndex, out totalcount, request.where, request.orderBy, request.propertyName, request.propertyValue);
            var resultList = resultQuery.ToList();

            var returnList = new List<NewsResponse>();
            resultList.ForEach(item =>
            {
                var convertedItem = Mapper.DynamicMap<NewsResponse>(item);

                var selectedSetting = settingRepo.Get(x => x.settinggroup == "newstype" && x.settingvalue1 == item.typeid);
                if (selectedSetting == null)
                    selectedSetting = new edu_base_setting();
                convertedItem.typename = selectedSetting.settingname;
                
                //var userobj = userRepo.Get(x=>x.uid==item.uid)??new edu_users_view();
                //convertedItem.uname = userobj.nickname ?? string.Empty;

                //var schoolobj = schoolRepo.Get(x => x.id == userobj.schoolid) ?? new edu_school();
                //convertedItem.schoolname = schoolobj.name ?? string.Empty;

                convertedItem.totalcount = totalcount;
                returnList.Add(convertedItem);
            });

            return returnList;
        }

        public List<NewsSoilMapResponse> GetNewsSoilMap(NewsSoilMapRequest request)
        {
            int totalcount;
            var resultQuery = newsDomain.GetNewsTypeSoilTypeList(request.pageCount, request.currentIndex, out totalcount, request.where, request.orderBy, request.propertyName, request.propertyValue);
            var resultList = resultQuery.ToList();

            var returnList = new List<NewsSoilMapResponse>();
            resultList.ForEach(item =>
            {
                var convertedItem = Mapper.DynamicMap<NewsSoilMapResponse>(item);

                var newstype = newsTypeRepo.Get(x => x.id == item.newstypeid);
                if (newstype == null)
                    newstype = new edu_cms_news_type();

                var soiltype = soilTypeRepo.Get(x=>x.id==item.soiltypeid);
                if (soiltype == null)
                    soiltype = new funong_soil_type();

                convertedItem.newstypename = newstype.typename;
                convertedItem.soiltypename = soiltype.typename;
                convertedItem.totalcount = totalcount;
                returnList.Add(convertedItem);
            });

            return returnList;
        }

        //获取新闻类别
        public List<NewsTypeResponse> GetTypeResponseList(NewsTypeRequest newsTypeRequest)
        {
            int totalcount;

            var queryableResults = newsDomain.GetNewsTypeList(newsTypeRequest.pageCount, newsTypeRequest.currentIndex, out totalcount, newsTypeRequest.where, x=>x.recordorder, newsTypeRequest.propertyName, newsTypeRequest.propertyValue);

            var resultList = queryableResults.ToList();

            var returnList = new List<NewsTypeResponse>();
            resultList.ForEach(item =>
            {
        
                var convertedItem = Mapper.DynamicMap<NewsTypeResponse>(item);

                //var parent = newsTypeRepo.Get(x => item.parentid == x.id && x.parentid == 0);
                //if (parent == null)
                //    parent = new edu_cms_news_type();
                //convertedItem.parentname = parent.typename;
                returnList.Add(convertedItem);
            });

            returnList = returnList.OrderBy(x => x.recordorder).ToList();
            return returnList;
        }

        //获取新闻类别
        public List<BaseSettingResponse> GetTypeResponseListEx(BaseSettingRequest bRequest)
        {
            int totalcount;

            var queryableResults = baseDataDomain.GetSettingList(bRequest.pageCount, bRequest.currentIndex, out totalcount, bRequest.where, x => x.updatetime, bRequest.propertyName, bRequest.propertyValue);

            var resultList = queryableResults.ToList();

            var returnList = new List<BaseSettingResponse>();
            resultList.ForEach(item =>
            {

                var convertedItem = Mapper.DynamicMap<BaseSettingResponse>(item);

                returnList.Add(convertedItem);
            });

            returnList = returnList.OrderBy(x => x.recordorder).ToList();
            return returnList;
        }

        private List<NewsResponse> ListConversion(List<edu_cms_news> newsList)
        {
            var returnList = new List<NewsResponse>();
            newsList.ForEach(item =>
            {
                var convertedItem = Mapper.DynamicMap<NewsResponse>(item);
                returnList.Add(convertedItem);
            });
            return returnList;
        }
    }
}
