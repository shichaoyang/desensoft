﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace FuNong.DataAccess
{
    public class UnitOfWork:IUnitOfWork
    {
        public UnitOfWork(IFuNongContext context)
        {
            this.context = context;
        }

        private readonly IFuNongContext context;

        private Hashtable repositorys;  //存储
        private bool disposed;          //标记

        public IRepository<T> Repository<T>() where T : class
        {
            if (repositorys == null) repositorys = new Hashtable();

            var type = typeof(T).Name;

            if (!repositorys.ContainsKey(type))
            {
                var repositoryType = typeof(Repository<>);
                var repositoryInstance = Activator.CreateInstance(repositoryType.MakeGenericType(typeof(T)), context);
                repositorys.Add(type, repositoryInstance);
            }
            return (IRepository<T>)repositorys[type];
        }

        public void Commit()
        {
           context.SaveChanges();
        }

        public void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
                disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
