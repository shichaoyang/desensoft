//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace FuNong.DataAccess
{
    public partial class edu_user_detail
    {
        #region Primitive Properties
    
        public virtual int uid
        {
            get;
            set;
        }
    
        public virtual int roleid
        {
            get;
            set;
        }
    
        public virtual Nullable<int> departmentid
        {
            get;
            set;
        }
    
        public virtual string smsnumber
        {
            get;
            set;
        }
    
        public virtual string personcode
        {
            get;
            set;
        }
    
        public virtual string idcard
        {
            get;
            set;
        }
    
        public virtual string checkincard
        {
            get;
            set;
        }
    
        public virtual Nullable<int> genderid
        {
            get;
            set;
        }
    
        public virtual Nullable<int> personstateid
        {
            get;
            set;
        }
    
        public virtual Nullable<int> ischeck
        {
            get;
            set;
        }
    
        public virtual string qq
        {
            get;
            set;
        }
    
        public virtual string host
        {
            get;
            set;
        }
    
        public virtual string useraddress
        {
            get;
            set;
        }
    
        public virtual string usernote
        {
            get;
            set;
        }
    
        public virtual Nullable<System.DateTime> createtime
        {
            get;
            set;
        }
    
        public virtual Nullable<int> recordorder
        {
            get;
            set;
        }
    
        public virtual Nullable<System.DateTime> updatetime
        {
            get;
            set;
        }

        #endregion

    }
}
