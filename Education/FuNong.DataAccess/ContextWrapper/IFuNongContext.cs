﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Collections;

namespace FuNong.DataAccess
{
    public interface IFuNongContext
    {
        IDbSet<T> DbSet<T>() where T : class;

        int SaveChanges();

        //DbSqlQuery<T> SqlQuery<T>(string sql, params object[] parameters) where T : class;
        IEnumerable SqlQuery<T>(string sql, params object[] parameters) where T : class;

        DbEntityEntry Entry(object o);

        void Dispose();
    }
}
