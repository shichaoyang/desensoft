﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Collections;

namespace FuNong.DataAccess
{
    public class FuNongContext : DbContext, IFuNongContext
    {
        //POCO 参考文章如下： http://blog.csdn.net/fangxing80/article/details/6452549
        public FuNongContext()
            : base("FuNongEntities")
        { }

        public IDbSet<T> DbSet<T>() where T : class
        {
            var context = this;
           // var connection = ((System.Data.Entity.DbContext)(context)).Database.Connection;
            return context.Set<T>();
        }

        public IEnumerable SqlQuery<T>(string sql, params object[] parameters) where T : class
        {
            try
            {
                //return base.Set<T>().SqlQuery(sql, parameters);
                return this.Database.SqlQuery(typeof(T), sql, parameters);
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public override int SaveChanges()
        {
            try
            {
                return base.SaveChanges();
            }
            catch (DbEntityValidationException dbex)
            {
                var msg = string.Empty;
                foreach (var validationErrors in dbex.EntityValidationErrors)
                    foreach (var validateionError in validationErrors.ValidationErrors)
                        msg += string.Format("属性:{0} 错误:{1}", validateionError.PropertyName, validateionError.ErrorMessage);
                var fail = new Exception(msg, dbex);
                throw fail;
            }
        }
    }
}
