﻿using FuNong.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace FuNong.Model
{
    public class Collector : Base<ds_machine_collector>
    {
        public Collector(IRepository<ds_machine_collector> collectorRepo)
        {
            this.collectorRepo = collectorRepo;
        }

        private readonly IRepository<ds_machine_collector> collectorRepo;

        public IQueryable<ds_machine_collector> GetMachineList(
                                       int pageCount
                                     , int currentIndex
                                     , out int totalCount
                                     , Expression<Func<ds_machine_collector, bool>> where
                                     , Expression<Func<ds_machine_collector, DateTime?>> orderBy
                                     , string propertyName = ""
                                     , string propertyValue = "")
        {
            IQueryable<ds_machine_collector> queryObjects = null;
            int skipRows = 0;
            if (currentIndex > 0) skipRows = currentIndex * pageCount;

            if (!string.IsNullOrEmpty(propertyName))
                queryObjects = CreateDynamicQuery<ds_machine_collector>(propertyName, propertyValue, where, collectorRepo);
            else
                queryObjects = collectorRepo.GetMany(where);
            totalCount = queryObjects.Count();

            return queryObjects.OrderByDescending(orderBy).Skip(skipRows).Take(pageCount);
        }
    }
}
