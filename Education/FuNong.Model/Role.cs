﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FuNong.DataAccess;
using System.Linq.Expressions;

namespace FuNong.Model
{
    public class Role : Base<edu_role_detail>
    {
        public Role(IRepository<edu_role_detail> roleRepo)
        {
            this.roleRepo = roleRepo;
        }

        private readonly IRepository<edu_role_detail> roleRepo;

        public IQueryable<edu_role_detail> GetRoleDetails(
                                        int pageCount
                                      , int currentIndex
                                      , out int totalCount
                                      , Expression<Func<edu_role_detail, bool>> where
                                      , Expression<Func<edu_role_detail, DateTime?>> orderBy
                                      , string propertyName = ""
                                      , string propertyValue = "")
        {
            IQueryable<edu_role_detail> queryObjects = null;
            int skipRows = 0;
            if (currentIndex > 0) skipRows = currentIndex * pageCount;

            if (!string.IsNullOrEmpty(propertyName))
                queryObjects = CreateDynamicQuery<edu_role_detail>(propertyName, propertyValue, where, roleRepo);
            else
                queryObjects = roleRepo.GetMany(where);
            totalCount = queryObjects.Count();

            return queryObjects.OrderByDescending(orderBy).Skip(skipRows).Take(pageCount);
        }
    }
}
