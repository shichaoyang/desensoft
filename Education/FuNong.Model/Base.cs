﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using FuNong.DataAccess;
using System.Linq.Dynamic;

namespace FuNong.Model
{
    public class Base<T> where T : class
    {
        public IQueryable<T> CreateDynamicQuery<T>(string propertyName, string propertyValue, Expression<Func<T, bool>> where, IRepository<T> repo) where T : class
        {
            IQueryable<T> queryResult = null;
            if (propertyName.Contains('$'))  //嵌套实体查询
                propertyName = propertyName.Replace('$', '.');
            queryResult = repo.GetMany(where).Where("" + propertyName + ".Contains(\"" + propertyValue + "\")");
            return queryResult;
        }
    }
}
