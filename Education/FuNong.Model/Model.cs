﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Linq.SqlClient;
using System.Text;
using FuNong.DataAccess;
using System.Linq.Expressions;

namespace FuNong.Model
{
    public class Model : Base<edu_model_detail>
    {
        public Model(IRepository<edu_model_detail> modelRepo, IRepository<edu_role_model> roleModelRepo)
        {
            this.modelRepo = modelRepo;
            this.roleModelRepo = roleModelRepo;
        }

        private readonly IRepository<edu_model_detail> modelRepo;
        private readonly IRepository<edu_role_model> roleModelRepo;

        //获取模块分页数据
        public IQueryable<edu_model_detail> GetModelDetails(
                                        int pageCount
                                      , int currentIndex
                                      , out int totalCount
                                      , Expression<Func<edu_model_detail, bool>> where
                                      , Expression<Func<edu_model_detail, DateTime?>> orderBy
                                      , string propertyName = ""
                                      , string propertyValue = "")
        {
            IQueryable<edu_model_detail> queryObjects = null;
            int skipRows = 0;
            if (currentIndex > 0) skipRows = currentIndex * pageCount;

            if (!string.IsNullOrEmpty(propertyName))
                queryObjects = CreateDynamicQuery<edu_model_detail>(propertyName, propertyValue, where, modelRepo);
            else
                queryObjects = modelRepo.GetMany(where);
            totalCount = queryObjects.Count();

            return queryObjects.OrderByDescending(orderBy).Skip(skipRows).Take(pageCount);
        }

        //获取当前角色下已分配的模块
        public IQueryable<edu_model_detail> GetModelApply(int roleid)
        {
            var modelQuery = GetModelApplyListByRoleid(roleid);

            //获取已经分配的模块的父菜单
            IQueryable<edu_model_detail> modelQueryWithParent = from p in modelRepo.GetMany(x => x.id != 0)
                                                                   join x in modelQuery
                                                                   on p.id equals x.parentid
                                                                   select p;
            //连接子菜单和父菜单，以便于创建树
            var resultQuery = modelQuery.Concat(modelQueryWithParent).Distinct();

            return resultQuery;
        }

        private IQueryable<edu_model_detail> GetModelApplyListByRoleid(int roleid)
        {
            IQueryable<edu_role_model> roleModelQuery = roleModelRepo.GetMany(x => x.roleid == roleid);

            //获取已经分配的模块的具体列表
            IQueryable<edu_model_detail> modelQuery = from p in roleModelRepo.GetMany(x => x.id != 0)
                                                         join q in modelRepo.GetMany(x => x.id != 0)
                                                         on p.modelid equals q.id
                                                         where p.roleid == roleid
                                                         select q;
            return modelQuery;
        }

        //获取当前角色下未分配的模块
        public IQueryable<edu_model_detail> GetModelUnApply(int roleid)
        {
            //获取已经分配的模块的列表
            var modelApply = GetModelApplyListByRoleid(roleid);

            //在所有模块列表中，去除掉已经分配的模块，即为未分配模块列表
            var modelUnApply = from q in modelRepo.GetMany(x => x.id != 0)
                               where !(from p in modelApply select p.id).Contains(q.id) 
                               select q;

            //获取已经分配的模块的父菜单
            IQueryable<edu_model_detail> modelQueryWithParent = from p in modelRepo.GetMany(x => x.id != 0)
                                                                   join x in modelUnApply
                                                                   on p.id equals x.parentid
                                                                   select p;

            //连接子菜单和父菜单，以便于创建树
            var resultQuery = modelUnApply.Concat(modelQueryWithParent).Distinct();

            return resultQuery;
        }

        public IQueryable<edu_role_model> GetRoleModelByIdList(int roleid, string modelidlist)
        {
            var idListInInt = new List<int>();

            modelidlist.Split(',').ToList().ForEach(item =>
            {
                int id;
                if (!string.IsNullOrEmpty(item) && Int32.TryParse(item, out id))
                {
                    idListInInt.Add(id);
                }
            });

            var roleModelQuery = from p in roleModelRepo.GetMany(x => x.id != 0)
                                 where idListInInt.Contains(p.modelid) && p.roleid == roleid
                                 select p;
            return roleModelQuery;
        }
    }
}
