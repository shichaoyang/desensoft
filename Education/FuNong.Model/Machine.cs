﻿using FuNong.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace FuNong.Model
{
    public class Machine : Base<ds_machine_list>
    {
        public Machine(IRepository<ds_machine_list> machineRepo)
        {
            this.machineRepo = machineRepo;
        }

        private readonly IRepository<ds_machine_list> machineRepo;

        public IQueryable<ds_machine_list> GetMachineList(
                                       int pageCount
                                     , int currentIndex
                                     , out int totalCount
                                     , Expression<Func<ds_machine_list, bool>> where
                                     , Expression<Func<ds_machine_list, DateTime?>> orderBy
                                     , string propertyName = ""
                                     , string propertyValue = "")
        {
            IQueryable<ds_machine_list> queryObjects = null;
            int skipRows = 0;
            if (currentIndex > 0) skipRows = currentIndex * pageCount;

            if (!string.IsNullOrEmpty(propertyName))
                queryObjects = CreateDynamicQuery<ds_machine_list>(propertyName, propertyValue, where, machineRepo);
            else
                queryObjects = machineRepo.GetMany(where);
            totalCount = queryObjects.Count();

            return queryObjects.OrderByDescending(orderBy).Skip(skipRows).Take(pageCount);
        }
    }
}
