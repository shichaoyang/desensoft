﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FuNong.Infrastructure.Json;
using FuNong.Service;

namespace FuNong.UserInterface.Controllers
{
    public class OperationBase<T> where T:class
    {
        public JsonResult Commit(IBaseService<T> userService, string inserted, string updated, string deleted, Func<List<T>, JsonResult> actionInsert, Func<List<T>, JsonResult> actionUpdate, Func<List<T>, JsonResult> actionDelete)
        {
            if (!string.IsNullOrEmpty(inserted))
            {
                var listInserted = JsonParser.JsonDeserializer<List<T>>(inserted);
                if (listInserted != null)
                    if (listInserted.Count > 0)
                    {
                        if (actionInsert != null)
                            return actionInsert(listInserted);
                    }
            }

            if (updated != null)
            {
                var listUpdated = JsonParser.JsonDeserializer<List<T>>(updated);
                if (listUpdated != null)
                    if (listUpdated.Count > 0)
                    {
                        if (actionUpdate != null)
                            return actionUpdate(listUpdated);
                    }
            }

            if (deleted != null)
            {
                var listDeleted = JsonParser.JsonDeserializer<List<T>>(deleted);
                if (listDeleted != null)
                    if (listDeleted.Count > 0)
                    {
                        if (actionDelete != null)
                            return actionDelete(listDeleted);
                    }
            }
            return null;
        }
    }
}