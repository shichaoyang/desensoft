﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FuNong.Framework.Cookie;
using FuNong.Framework.Logger;
using FuNong.Framework.PoolCenter;

namespace FuNong.UserInterface.Controllers
{
    public class BaseController : Controller
    {
        public BaseController(ICookie cookie, ILoggerService logger, IEventPool eventPool, bool auth)
        {
            this.cookie = cookie;
            this.logger = logger;
            this.eventPool = eventPool;
            this.auth = auth;
        }

        private readonly ICookie cookie;
        private readonly ILoggerService logger;
        private readonly IEventPool eventPool;
        private bool auth = false;

        public int CurrentLoginSchoolID { get; set; }
        public string CurrentLoginSchoolName { get; set; }

        public int CurrentLoginUserID { get; set; }
        public string CurrentLoginUserName { get; set; }
        public string CurrentLoginUserRealName { get; set; }

        public int CurrentLoginRoleID { get; set; }
        public string CurrentLoginRoleName { get; set; }

        public string IsMultiSchool { get; set; }

        protected override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (auth)
            {
                var collection = cookie.GetCookieCollection("FuNong.UserInfo.Login");
                if (collection == null)
                {
                    filterContext.Result = new RedirectResult("Home/Login");
                }
                else
                {
                    var uid = collection["FuNong.Login.UserID"];
                    var uname = HttpUtility.UrlDecode(collection["FuNong.Login.UserName"].Trim());
                    var realname = HttpUtility.UrlDecode(collection["FuNong.Login.NickName"].Trim());

                    var roleid = collection["FuNong.Login.RoleID"];
                    var rolename = HttpUtility.UrlDecode(collection["FuNong.Login.RoleName"].Trim()); ;

                    int uidInt;
                    int roleidInt;

                    if (Int32.TryParse(uid, out uidInt)) CurrentLoginUserID = uidInt;
                    if (Int32.TryParse(roleid, out roleidInt)) CurrentLoginRoleID = roleidInt;

                    CurrentLoginRoleName = rolename;
                    CurrentLoginUserName = uname;
                    CurrentLoginUserRealName = realname;
                }
            }
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            var collection = cookie.GetCookieCollection("FuNong.UserInfo.Login");
            if (collection != null)
            {
                var userID = collection["FuNong.Login.UserID"];
                var typeid = "2";
                var exceptionMessage = filterContext.Exception.Message;
                var exceptionStack = filterContext.Exception.StackTrace;
                logger.Error(new LogContent("发生错误:" + exceptionMessage, userID, typeid));
                logger.Error(new LogContent("错误堆栈:" + exceptionStack, userID, typeid));

                filterContext.Controller.TempData["ExceptionMsg"] = exceptionMessage;
                filterContext.ExceptionHandled = true;
                filterContext.Result = new RedirectResult(Url.Action("index", "error", new { msg = HttpUtility.UrlEncode((exceptionMessage)) }));
            }
        }
    }
}