﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FuNong.Framework.Cookie;
using FuNong.Framework.Logger;
using FuNong.UserInterface.Models;
using FuNong.Service;
using FuNong.DataAccess;
using FuNong.DataContract;
using System.IO;
using System.Text;
using DotNet.Utilities;
using FuNong.Framework.PoolCenter;

namespace FuNong.UserInterface.Controllers
{
    public class ProcessController : BaseController
    {
        public ProcessController(
               IBaseService<edu_department_detail> departmentCRUDService
             , IBaseService<edu_role_detail> roleCRUDService
             , IBaseService<edu_user_detail> userCRUDService
             , IBaseService<edu_model_detail> modelCRUDService
             , IBaseService<edu_role_model> roleModelCRUDService
             , IBaseService<funong_publish_main> publishMainCRUDService
             , IBaseService<funong_publish_detail> publishDetailCRUDService
             , IBaseService<funong_service_request> requestCRUDService
             , IBaseService<funong_intention> intentionCRUDService
             , IBaseService<funong_deal> dealCRUDService
             , IDepartmentService departmentService
             , IRoleService roleService
             , IUserService userService
             , IModelService modelService
             , INewsService newsService
             , ISoilPublishService soilService
             , IProcessService processService
             , IDealService dealService
             , ICookie cookie
             , ILoggerService logger
             , IEventPool eventPool
            ): base(cookie, logger,eventPool,false)
        {
            this.departmentCRUDService = departmentCRUDService;
            this.roleCRUDService = roleCRUDService;
            this.userCRUDService = userCRUDService;
            this.modelCRUDService = modelCRUDService;
            this.roleModelCRUDService = roleModelCRUDService;

            this.publishMainCRUDService = publishMainCRUDService;
            this.publishDetailCRUDService = publishDetailCRUDService;
            this.requestCRUDService = requestCRUDService;
            this.intentionCRUDService = intentionCRUDService;
            this.dealCRUDService = dealCRUDService;

            this.departmentService = departmentService;
            this.roleService = roleService;
            this.userService = userService;
            this.modelService = modelService;

            this.newsService = newsService;
            this.soilService = soilService;

            this.processService  = processService;
            this.dealService = dealService;

            this.cookie = cookie;
            this.logger = logger;
            this.eventPool = eventPool;
        }

        private readonly IBaseService<edu_department_detail> departmentCRUDService;
        private readonly IBaseService<edu_role_detail> roleCRUDService;
        private readonly IBaseService<edu_user_detail> userCRUDService;
        private readonly IBaseService<edu_model_detail> modelCRUDService;
        private readonly IBaseService<edu_role_model> roleModelCRUDService;

        private readonly IBaseService<funong_publish_main> publishMainCRUDService;
        private readonly IBaseService<funong_publish_detail> publishDetailCRUDService;
        private readonly IBaseService<funong_service_request> requestCRUDService;
        private readonly IBaseService<funong_intention> intentionCRUDService;
        private readonly IBaseService<funong_deal> dealCRUDService;

        private readonly IDepartmentService departmentService;
        private readonly IRoleService roleService;
        private readonly IUserService userService;
        private readonly IModelService modelService;
        private readonly INewsService newsService;
        private readonly ISoilPublishService soilService;
        private readonly IProcessService processService;
        private readonly IDealService dealService;

        private readonly ICookie cookie;
        private readonly ILoggerService logger;
        private readonly IEventPool eventPool;

        #region process1
        //委托申请
        [ValidateInput(false)]
        public ActionResult Process_1()
        {
            var field1 = Request.Form["field1"];
            var field2 = Request.Form["field2"];
            var field3 = Request.Form["field3"];
            var field4 = Request.Form["field4"];
            var field5 = Request.Form["field5"];
            var field6 = Request.Form["field6"];
            var field7 = Request.Form["field7"];
            var field8 = Request.Form["field8"];
            var field9 = Request.Form["field9"];
            var field10 = Request.Form["field10"];
            var field11 = Request.Form["field11"];
            var field12 = Request.Form["field12"];
            var field13 = Request.Form["field13"];
            var field14 = Request.Form["field14"];
            var field15 = Request.Form["field15"];
            //var hidetemplate = Request.Form["hidetemplate"];

            var requestContent = new StringBuilder();

            if (string.IsNullOrEmpty(field1))
                return View(ValidateField("表单未填写完整，请重试！"));

            requestContent.Append(field1).Append("$").Append(field2).Append("$").Append(field3).Append("$").Append(field4).Append("$")
                          .Append(field5).Append("$").Append(field6).Append("$").Append(field7).Append("$").Append(field8).Append("$")
                          .Append(field9).Append("$").Append(field10).Append("$").Append(field11).Append("$").Append(field12).Append("$")
                          .Append(field13).Append("$").Append(field14).Append("$").Append(field15);

            var userIDInt = GetCurrentLoginUserID();
            
            var serverGroup = "COMPANY"; //base_setting中为COMPANY的配置组

            var process1Model = new funong_service_request();
            process1Model.clientid = userIDInt;
            process1Model.isapprove = 0;  //未被认证
            process1Model.servergroup = serverGroup;
            process1Model.recordorder = 1;
            process1Model.updatetime = DateTime.Now;
            process1Model.requestcontent = requestContent.ToString();

            var list = new List<funong_service_request>();
            list.Add(process1Model);

            if (requestCRUDService.AddInBatch(list))
            {
                return View(new Process4Model() { success = true, message = "委托申请提交成功，请继续下一步操作！" });
            }
            else
            {
                return View(ValidateField("委托申请提交失败，请仔细检查后再操作！"));
            }
        }
        #endregion 

        #region process2
        //形式审查
        public ActionResult Process_2(Process2Model p)
        {
            if (string.IsNullOrEmpty(p.indeximg))
                return View(new Process2Model() { success = false, message = "请上传身份证正反面照片复印件!" });

            var process2Model = new funong_service_request();

            var request = new ProcessRequest();
            request.requestid = p.requestid;
            request.userid = GetCurrentLoginUserID();

            var existEntity = processService.AttachServiceRequest(request);

            if (existEntity.Success)
            {
                //更新数据库

                var list = new List<funong_service_request>();

                existEntity.idcardimages = p.indeximg;
                existEntity.soilcertificateimages = p.indeximg1;

                var serviceRequestEntity = new funong_service_request();
                serviceRequestEntity.id = existEntity.id;
                serviceRequestEntity.clientid = existEntity.clientid;
                serviceRequestEntity.servergroup = existEntity.servergroup;
                serviceRequestEntity.requestcontent = existEntity.requestcontent;
                serviceRequestEntity.requesttemplate = existEntity.requesttemplate;
                serviceRequestEntity.idcardimages = existEntity.idcardimages;
                serviceRequestEntity.soilcertificateimages = existEntity.soilcertificateimages;
                serviceRequestEntity.isapprove = existEntity.isapprove;
                serviceRequestEntity.recordorder = existEntity.recordorder.GetValueOrDefault();
                serviceRequestEntity.updatetime = DateTime.Now;
                list.Add(serviceRequestEntity);

                if (requestCRUDService.UpdateInBatch(list))
                {
                    //用户上传完毕图片，则发送需要权属审核到站内信
                    eventPool.Publish("FN.ApproveProcess3");

                    return View(new Process2Model() { success = true, message = "形式审查操作成功！" });
                }
                else
                {
                    return View(new Process2Model() { success = false, message = "形式审查操作失败，请重试！" });
                }
            }
            else
            {
                return View(new Process2Model() {success=false, message=existEntity.Message });
            }
        }
        #endregion

        #region process3
        //确认权属
        public ActionResult Process_3()
        {
            return View();
        }

        public ActionResult Process_3_Approve()
        {
            return View();
        }

        [HttpPost]
        public JsonResult ApproveProcess3()
        {
            string inserted = Request.Form["inserted"];
            string updated = Request.Form["updated"];
            string deleted = Request.Form["deleted"];

            var operationbase = new OperationBase<funong_service_request>();
            return operationbase.Commit(requestCRUDService, inserted, updated, deleted, (list) =>
            {
                return Json(new { success = false, message = "此操作不存在!" }, JsonRequestBehavior.AllowGet);
            }, (list) =>
            {
                foreach (var model in list)
                    model.updatetime = DateTime.Now;

                if (requestCRUDService.UpdateInBatch(list))
                    return Json(new { success = true, message = "权属信息审核成功!" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = false, message = "权属信息审核失败，请重试！" }, JsonRequestBehavior.AllowGet);
            }, (list) =>
            {
                return Json(new { success = false, message = "此操作不存在!" }, JsonRequestBehavior.AllowGet);
            });
        }

        [HttpPost]
        public JsonResult GetProcess3List()
        {
            int page = Int32.Parse(Request.Form["page"]) - 1;
            int rows = Int32.Parse(Request.Form["rows"]);

            string propertyName = Request.Form["Field"];
            string propertyValue = Request.Form["Value"];

            var currentloginuserid = GetCurrentLoginUserID();
            var currentloginroleid = GetCurrentLoginRoleID();

            var request = new ProcessRequest();
            request.pageCount = rows;
            request.currentIndex = page;
            request.propertyName = propertyName;
            request.propertyValue = propertyValue;
            request.orderBy = x => x.updatetime;

            if (currentloginroleid == 2)
            {
                request.userid = currentloginuserid;
                request.where = x => x.clientid == currentloginuserid;
            }
            else
            {
                request.userid = -1;
                request.where = x => x.clientid != 0;
            }

            var processList = processService.TriggerList(request);

            int totalCount = 0;
            if (processList != null && processList.Count > 0)
            {
                totalCount = processList[0].totalcount;
            }

            var json = new
            {
                total = totalCount,
                rows = processList
            };
            JsonResult result = Json(json, JsonRequestBehavior.AllowGet);
            return result;
        }

        public ActionResult Process_3_Detail(int id)
        {
            var currentloginuserid = GetCurrentLoginUserID();
            var currentloginroleid = GetCurrentLoginRoleID();

            var request = new ProcessRequest();
          

             if (currentloginroleid == 2)
            {
                request.requestid = id;
                request.userid = currentloginuserid;
            }
            else
            {
                request.requestid = id;
                request.userid = -1;
            }

            var existEntity = processService.AttachServiceRequest(request);

            return View(existEntity);
        }
        #endregion 

        #region process4
        //信息发布
        [ValidateInput(false)]
        public ActionResult Process_4(Process4Model p)
        {
            if (Request.Form.Count == 0)
                return View();

            if (p.requestid <= 0)
                return View(ValidateField("无审核完毕的委托申请，不能发布供应信息！"));

            if (p.soiltypeid <= 0)
                 return View( ValidateField("土地类型不能为空！"));

            if (p.changetypeid<=0)
                 return View( ValidateField("流转方式不能为空！"));

            if (p.districtid<=0)
                 return View( ValidateField("所在地区不能为空！"));

            if (string.IsNullOrEmpty(p.publishtitle))
                 return View( ValidateField("信息标题不能为空！"));

            if (string.IsNullOrEmpty(p.locationdetail))
                 return View( ValidateField("位置与四至不能为空！"));

            if (p.sparechangeyear <= 0)
                 return View( ValidateField("剩余流转年限不能为空！"));

            if (p.canchangeyear <= 0)
                return View(ValidateField("可流转年限不能为空！"));

            if (p.soilarea <= 0)
                return View(ValidateField("土地面积不能为空或0！"));

            if (string.IsNullOrEmpty(p.publishtitle))
                return View(ValidateField("信息标题不能为空"));

            if (string.IsNullOrEmpty(p.publisher))
                return View(ValidateField("联系人姓名不能为空"));

            if (string.IsNullOrEmpty(p.publisherphone))
                return View(ValidateField("联系人电话不能为空"));

            var publishMainEntity = new funong_publish_main();
            publishMainEntity.canchangeyear = p.canchangeyear;
            publishMainEntity.changetypeid = p.changetypeid;
            publishMainEntity.description = p.description;
            publishMainEntity.districtid = p.districtid;
            publishMainEntity.isapprove = 0;  //TODO：默认值为0
            publishMainEntity.locationdetail = p.locationdetail;
            publishMainEntity.publisher = p.publisher;
            publishMainEntity.publisherphone = p.publisherphone;
            publishMainEntity.publishtitle = p.publishtitle;
            publishMainEntity.recordorder = 1;
            publishMainEntity.requestid = p.requestid;  //TODO：后面会修改
            publishMainEntity.soilarea = p.soilarea;
            publishMainEntity.soiltypeid = p.soiltypeid;
            publishMainEntity.sparechangeyear = p.sparechangeyear;
            publishMainEntity.updatetime = DateTime.Now;

            var mainList = new List<funong_publish_main>();
            mainList.Add(publishMainEntity);
            bool success = publishMainCRUDService.AddInBatch(mainList);
            if (success)
            {
                var request = new SoilPublishMainRequest() { soiltitle = publishMainEntity.publishtitle, publisher = publishMainEntity.publisher, publisherphone = publishMainEntity.publisherphone };
                var publishMainEntityWithID = soilService.GetPublishMainBy(request);
                if (publishMainEntityWithID != null)
                {
                    var feeex = p.chargefee;
                    decimal? feex = null;
                    if (!string.IsNullOrEmpty(feeex))
                    {
                        decimal temp;
                        if (decimal.TryParse(feeex, out temp))
                        {
                            feex = temp;
                        }
                    }

                    var publishDetailEntity = new funong_publish_detail();
                    publishDetailEntity.changefee = feex;
                    publishDetailEntity.changeunit = "万元";
                    publishDetailEntity.publishermail = p.publishermail;
                    publishDetailEntity.publisherqq = p.publisherqq;
                    publishDetailEntity.soilimages = p.indeximg;
                    publishDetailEntity.soilgroundlist = p.soilground;
                    publishDetailEntity.earthtypelist = p.earthtype;
                    publishDetailEntity.waterlist = p.water;
                    publishDetailEntity.aroundlist = p.around;
                    publishDetailEntity.publishmainid = publishMainEntityWithID.id;

                    var detailList = new List<funong_publish_detail>();
                    detailList.Add(publishDetailEntity);
                    if (publishDetailCRUDService.AddInBatch(detailList))
                    {
                        //用户发不完土地信息，则发送需要土地信息审核的信息到站内信中
                        eventPool.Publish("FN.ApproveProcess4");

                        return View(new Process4Model() { success = true, message = "信息发布成功，请等待管理员审核通过！" });
                    }
                    else
                        return View(new Process4Model() { success = false, message = "信息发布失败，信息从表未能成功更新！" });
                }
            }
            return View(new Process4Model() { success = false, message = "信息发布失败，请重试！" });
        }

        private Process4Model ValidateField(string msg)
        {
            return new Process4Model() { success = false, message = msg };
        }

        //步骤四：管理员审核
        public ActionResult Process_4_Approve()
        {
            return View();
        }

        //当前登录用户发布的供应信息
        public ActionResult Process_4_List()
        {
            return View();
        }

        [HttpPost]
        public JsonResult GetProcess4ApproveList()
        {
            int page = Int32.Parse(Request.Form["page"]) - 1;
            int rows = Int32.Parse(Request.Form["rows"]);

            string propertyName = Request.Form["Field"];
            string propertyValue = Request.Form["Value"];

            var soilMainRequest = new SoilPublishMainRequest();
            soilMainRequest.currentIndex = page;
            soilMainRequest.pageCount = rows;
            soilMainRequest.where = x => x.id != 0 && x.isapprove == 0;  //未审核列表
            soilMainRequest.orderBy = x => x.updatetime;
            soilMainRequest.propertyName = propertyName;
            soilMainRequest.propertyValue = propertyValue;

            var result = soilService.TriggerList(soilMainRequest);

            if (result != null)
            {
                result.ForEach(item =>
                {
                    item.description = DotNet.Utilities.HTMLHelper.NoHTML(item.description);
                });
            }

            int totalCount = 0;
            if (result != null && result.Count > 0)
            {
                totalCount = result[0].totalcount;
            }

            var json = new
            {
                total = totalCount,
                rows = result
            };

            return Json(json, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetProcess4List()
        {
            int page = Int32.Parse(Request.Form["page"]) - 1;
            int rows = Int32.Parse(Request.Form["rows"]);

            string propertyName = Request.Form["Field"];
            string propertyValue = Request.Form["Value"];

            var currentuserid = GetCurrentLoginUserID();
            var currentroleid = GetCurrentLoginRoleID();

            var request = new UserRequest();
            if (currentroleid == 2)  //如果只是普通会员
                request.userid = currentuserid;
            else
                request.userid = -1;  //如果不是普通会员，则请求全部的列表

            var requestIdList = userService.GetRequestIDByUser(request);

            var list = new List<int?>();
            if (requestIdList != null)
            {
                requestIdList.ForEach(item =>
                {
                    list.Add(item.requestid);
                });
            }

            var soilMainRequest = new SoilPublishMainRequest();
            soilMainRequest.currentIndex = page;
            soilMainRequest.pageCount = rows;
            soilMainRequest.where = x => list.Contains(x.requestid);
            soilMainRequest.orderBy = x => x.updatetime;
            soilMainRequest.propertyName = propertyName;
            soilMainRequest.propertyValue = propertyValue;

            var result = soilService.TriggerList(soilMainRequest);

            int totalCount = 0;
            if (result != null && result.Count > 0)
            {
                totalCount = result[0].totalcount;
            }

            var json = new
            {
                total = totalCount,
                rows = result
            };

            return Json(json, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateInput(false)]
        public JsonResult ApproveProcess4()
        {
            string inserted = Request.Form["inserted"];
            string updated = Request.Form["updated"];
            string deleted = Request.Form["deleted"];

            var operationbase = new OperationBase<funong_publish_main>();
            return operationbase.Commit(publishMainCRUDService, inserted, updated, deleted, (list) =>
            {
                return Json(new { success = false, message = "此操作不存在!" }, JsonRequestBehavior.AllowGet);
            }, (list) =>
            {
                foreach (var model in list)
                    model.updatetime = DateTime.Now;

                if (publishMainCRUDService.UpdateInBatch(list))
                    return Json(new { success = true, message = "发布信息审核成功!" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = false, message = "发布信息审核失败，请重试！" }, JsonRequestBehavior.AllowGet);
            }, (list) =>
            {
                return Json(new { success = false, message = "此操作不存在!" }, JsonRequestBehavior.AllowGet);
            });
        }

        [HttpPost]
        [ValidateInput(false)]
        public JsonResult CommitProcess4()
        {
            string inserted = Request.Form["inserted"];
            string updated = Request.Form["updated"];
            string deleted = Request.Form["deleted"];

            var operationbase = new OperationBase<funong_publish_main>();
            return operationbase.Commit(publishMainCRUDService, inserted, updated, deleted, (list) =>
            {
                return Json(new { success = false, message = "此操作不存在!" }, JsonRequestBehavior.AllowGet);
            }, (list) =>
            {
                return Json(new { success = false, message = "此操作不存在!" }, JsonRequestBehavior.AllowGet);
            }, (list) =>
            {
                foreach (var model in list)
                {
                    if (model.isapprove == 1)
                    {
                        return Json(new { success = "发布的土地信息存在被审核项，无法进行删除操作!" }, JsonRequestBehavior.AllowGet);
                    }
                }

                if (publishMainCRUDService.DeleteInBatch(list))
                    return Json(new { success = "发布的土地信息移除成功!" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { error = "发布的土地信息移除失败，请重试！" }, JsonRequestBehavior.AllowGet);
            });

        }

        #endregion 

        #region process5
        //意向受让方
        public ActionResult Process_5()
        {
            return View();
        }

        public PartialViewResult Process_5_Frame(int id)
        {
            return PartialView(id);
        }

        public ActionResult Process_5_Detail(int id)
        {
            return View(id);
        }

        public JsonResult Process5DetailAjax(int id)
        {
            int page = Int32.Parse(Request.Form["page"]) - 1;
            int rows = Int32.Parse(Request.Form["rows"]);

            string propertyName = Request.Form["Field"];
            string propertyValue = Request.Form["Value"];


            var request = new IntentionRequest();
            request.currentIndex = page;
            request.pageCount = rows;
            request.where = x => x.publishmainid == id;
            request.orderBy = x => x.updatetime;
            request.propertyName = propertyName;
            request.propertyValue = propertyValue;

            var result = soilService.GetIntentionByMainID(request);

            int totalCount = 0;
            if (result != null && result.Count > 0)
            {
                totalCount = result[0].totalcount;
            }

            var json = new
            {
                total = totalCount,
                rows = result
            };

            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region process6

        public ActionResult Process_6()
        {
            return View();
        }

        public PartialViewResult Process_6_Frame(int id)
        {
            return PartialView(id);
        }

        public ActionResult Process_6_Detail(int id)
        {
            return View(id);
        }

        [HttpPost]
        public JsonResult ApproveProcess6()
        {
            string inserted = Request.Form["inserted"];
            string updated = Request.Form["updated"];
            string deleted = Request.Form["deleted"];

            var operationbase = new OperationBase<funong_intention>();
            return operationbase.Commit(intentionCRUDService, inserted, updated, deleted, (list) =>
            {
                return Json(new { success = false, message = "此操作不存在!" }, JsonRequestBehavior.AllowGet);
            }, (list) =>
            {
                foreach (var model in list)
                    model.updatetime = DateTime.Now;

                if (intentionCRUDService.UpdateInBatch(list))
                    return Json(new { success = true, message = "交易信息审核成功!" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = false, message = "交易信息信息审核失败，请重试！" }, JsonRequestBehavior.AllowGet);
            }, (list) =>
            {
                return Json(new { success = false, message = "此操作不存在!" }, JsonRequestBehavior.AllowGet);
            });
        }

        [HttpPost]
        public JsonResult Process6DetailAjax(int id)
        {
            int page = Int32.Parse(Request.Form["page"]) - 1;
            int rows = Int32.Parse(Request.Form["rows"]);

            string propertyName = Request.Form["Field"];
            string propertyValue = Request.Form["Value"];


            var request = new IntentionRequest();
            request.currentIndex = page;
            request.pageCount = rows;
            request.where = x => x.publishmainid == id;
            request.orderBy = x => x.updatetime;
            request.propertyName = propertyName;
            request.propertyValue = propertyValue;

            var result = soilService.GetIntentionByMainID(request);

            int totalCount = 0;
            if (result != null && result.Count > 0)
            {
                totalCount = result[0].totalcount;
            }

            var json = new
            {
                total = totalCount,
                rows = result
            };

            return Json(json, JsonRequestBehavior.AllowGet);
        }


        #endregion

        #region process7

        public ActionResult Process_7()
        {
            return View();
        }

        public PartialViewResult Process_7_Frame(int id)
        {
            return PartialView(id);
        }

        public ActionResult Process_7_Detail(int id)
        {
            return View(id);
        }

        public JsonResult ApproveProcess7()
        {
            string inserted = Request.Form["inserted"];
            string updated = Request.Form["updated"];
            string deleted = Request.Form["deleted"];

            var operationbase = new OperationBase<funong_intention>();
            return operationbase.Commit(intentionCRUDService, inserted, updated, deleted, (list) =>
            {
                return Json(new { success = false, message = "此操作不存在!" }, JsonRequestBehavior.AllowGet);
            }, (list) =>
            {
                foreach (var model in list)
                {
                    model.updatetime = DateTime.Now;
                }

                if (intentionCRUDService.UpdateInBatch(list))
                    return Json(new { success = true, message = "成交签约信息审核成功!" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = false, message = "成交签约信息信息审核失败，请重试！" }, JsonRequestBehavior.AllowGet);
            }, (list) =>
            {
                return Json(new { success = false, message = "此操作不存在!" }, JsonRequestBehavior.AllowGet);
            });
        }

        public JsonResult Process7DetailAjax(int id)
        {
            int page = Int32.Parse(Request.Form["page"]) - 1;
            int rows = Int32.Parse(Request.Form["rows"]);

            string propertyName = Request.Form["Field"];
            string propertyValue = Request.Form["Value"];


            var request = new IntentionRequest();
            request.currentIndex = page;
            request.pageCount = rows;
            request.where = x => x.publishmainid == id;
            request.orderBy = x => x.updatetime;
            request.propertyName = propertyName;
            request.propertyValue = propertyValue;

            var result = soilService.GetIntentionByMainID(request);

            int totalCount = 0;
            if (result != null && result.Count > 0)
            {
                totalCount = result[0].totalcount;
            }

            var json = new
            {
                total = totalCount,
                rows = result
            };

            return Json(json, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region process8

        public ActionResult Process_8()
        {
            return View();
        }

        [HttpPost]
        public JsonResult CommitCharge(int rid, int mid, decimal fee)
        {
            if (rid <= 0)
                return Json(new { success = false, message = "流转流水号无效，不能进行缴费操作！" }, JsonRequestBehavior.AllowGet);
            if (mid <= 0)
                return Json(new { success = false, message = "土地发布信息序号无效，不能进行缴费操作！" }, JsonRequestBehavior.AllowGet);
            if (fee <= 0)
                return Json(new { success = false, message = "缴费金额必须大于0！" }, JsonRequestBehavior.AllowGet);

            var entity = new funong_deal();
            entity.requestid = rid;
            entity.publishmainid = mid;
            entity.chargefee = fee;
            entity.isapprove = 0;
            entity.updatetime = DateTime.Now;
            entity.recordorder = 1;

            var list = new List<funong_deal>();
            list.Add(entity);

            var flag = dealCRUDService.AddInBatch(list);
            if (flag)
            {
                //用户缴费信息确认，则发送缴费信息到站内信中
                eventPool.Publish("FN.ApproveProcess8");
                return Json(new { success = true, message = "缴费成功！" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = true, message = "缴费失败，请重试！" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GetALLUserList()
        {
            UserRequest request = new UserRequest();
            request.currentIndex = 0;
            request.pageCount = Int32.MaxValue;
            request.propertyName = string.Empty;
            request.propertyValue = string.Empty;
            request.where = x => x.uid != 0 && x.ischeck == 1 && x.roleid==2;
            request.orderBy = x => x.updatetime;

            var list = userService.TriggerList(request);

            return Json(list,JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetPublishMainByUser(int uid)
        {
            var request = new UserRequest();
            request.userid = uid; 

            var requestIdList = userService.GetRequestIDByUser(request);

            var list = new List<int?>();
            if (requestIdList != null)
            {
                requestIdList.ForEach(item =>
                {
                    list.Add(item.requestid);
                });
            }

            var soilMainRequest = new SoilPublishMainRequest();
            soilMainRequest.currentIndex = 0;
            soilMainRequest.pageCount = Int32.MaxValue;
            soilMainRequest.where = x => list.Contains(x.requestid);
            soilMainRequest.orderBy = x => x.updatetime;
            soilMainRequest.propertyName = string.Empty;
            soilMainRequest.propertyValue = string.Empty;

            var result = soilService.TriggerList(soilMainRequest);

            return Json(new { success = true, data = result }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult CommitProcess8()
        {
            string inserted = Request.Form["inserted"];
            string updated = Request.Form["updated"];
            string deleted = Request.Form["deleted"];

            var operationbase = new OperationBase<funong_deal>();
            return operationbase.Commit(dealCRUDService, inserted, updated, deleted, (list) =>
            {
                foreach (var model in list)
                {
                    model.updatetime = DateTime.Now;
                }

                if (dealCRUDService.AddInBatch(list))
                    return Json(new { success = "用户详细信息新增成功!" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { error = "用户详细信息新增失败，请重试！" }, JsonRequestBehavior.AllowGet);
            }, (list) =>
            {
                foreach (var model in list)
                    model.updatetime = DateTime.Now;

                if (dealCRUDService.UpdateInBatch(list))
                    return Json(new { success = "用户详细信息更新成功!" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { error = "用户详细信息更新失败，请重试！" }, JsonRequestBehavior.AllowGet);
            }, (list) =>
            {
                if (dealCRUDService.DeleteInBatch(list))
                    return Json(new { success = "用户详细信息移除成功!" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { error = "用户详细信息移除失败，请重试！" }, JsonRequestBehavior.AllowGet);
            });
        }

        [HttpPost]
        public JsonResult GetChargingListForProcess8()
        {
            int page = Int32.Parse(Request.Form["page"]) - 1;
            int rows = Int32.Parse(Request.Form["rows"]);

            string propertyName = Request.Form["Field"];
            string propertyValue = Request.Form["Value"];

            var currentuserid = GetCurrentLoginUserID();
            var currentroleid = GetCurrentLoginRoleID();

            var request = new UserRequest();
            if (currentroleid == 2)  //如果只是普通会员
                request.userid = currentuserid;
            else
                request.userid = -1;  //如果不是普通会员，则请求全部的列表

            var mainIdList = userService.GetPublishMainIDByUser(request);

            var list = new List<int?>();
            if (mainIdList != null)
            {
                mainIdList.ForEach(item =>
                {
                    list.Add(item.publishmainid);
                });
            }

            var dealRequest = new DealRequest();
            dealRequest.currentIndex = page;
            dealRequest.pageCount = rows;
            dealRequest.where = x => list.Contains(x.id);
            dealRequest.orderBy = x => x.updatetime;
            dealRequest.propertyName = propertyName;
            dealRequest.propertyValue = propertyValue;

            var result = dealService.TriggerList(dealRequest);

            int totalCount = 0;
            if (result != null && result.Count > 0)
            {
                totalCount = result[0].totalcount;
            }

            var json = new
            {
                total = totalCount,
                rows = result
            };

            return Json(json, JsonRequestBehavior.AllowGet);
        }
        #endregion 

        #region process9

        public ActionResult Process_9()
        {
            return View();
        }

        [HttpPost]
        public JsonResult CommitJSJG(int rid, int mid)
        {
            if (rid <= 0)
                return Json(new { success = false, message = "流转流水号无效，不能进行缴费操作！" }, JsonRequestBehavior.AllowGet);
            if (mid <= 0)
                return Json(new { success = false, message = "土地发布信息序号无效，不能进行缴费操作！" }, JsonRequestBehavior.AllowGet);

            var request = new DealRequest();
            request.currentIndex = 0;
            request.pageCount = 1;
            request.where = x => x.requestid == rid && x.publishmainid == mid;
            request.orderBy = x => x.updatetime;

            var response = dealService.TriggerList(request);

            if(response==null)
                return Json(new { success = false, message = "数据库无此数据，不能进行结算交割！" }, JsonRequestBehavior.AllowGet);

            var list = new List<funong_deal>();
            response.ForEach(item =>
            {
                var convertedItem = new funong_deal();
                convertedItem.id = item.id;
                convertedItem.requestid = item.requestid;
                convertedItem.publishmainid = item.publishmainid;
                convertedItem.changetypeid = item.changetypeid;
                convertedItem.signcontent = item.signcontent;
                convertedItem.signtime = DateTime.Now;
                convertedItem.chargefee = item.chargefee;
                convertedItem.isapprove = 1;
                convertedItem.recordorder = item.recordorder;
                convertedItem.updatetime = DateTime.Now;
                list.Add(convertedItem);
            });

            var flag = dealCRUDService.UpdateInBatch(list);
            if (flag)
            {
                return Json(new { success = true, message = "结算交割成功！" }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = true, message = "结算交割失败，请重试！" }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region process10

        public ActionResult Process_10(int? rid,int? mid)
        {
            if (rid == null || mid == null || rid <= 0 || mid <= 0)
                return View(new JZSEntity());

            var request = new IntentionRequest();
            request.currentIndex = 0;
            request.pageCount = 1;
            request.where = x => x.publishmainid == mid && x.requestid == rid && x.isarrange == 1 && x.issuccess == 1;
            request.orderBy = x => x.updatetime;

            var response = soilService.GetIntentionByMainID(request);

            var reques1 = new ProcessRequest();
            reques1.requestid = rid.GetValueOrDefault();
            reques1.userid = -1;
            var response1 = processService.AttachServiceRequest(reques1);

            if (response1 == null)
            {
                return View(new JZSEntity() { Success = false, Message = "附加结算交割信息失败，请重试！" });
            }

            if (response == null)
            {
                return View(new JZSEntity() { Success = false, Message = "无意向用户或者意向用户提交的申请未通过审核，请重试！" });
            }

            if (response.Count == 0)
            {
                return View(new JZSEntity() { Success = false, Message = "无意向用户或者意向用户提交的申请未通过审核，请重试！" });
            }

            var userrequest = new UserRequest();
            userrequest.currentIndex=0;
            userrequest.pageCount=1;
            userrequest.where = x => x.uid == response1.clientid;
            userrequest.orderBy = x=>x.updatetime;

            var user = userService.TriggerList(userrequest);
            if (user == null)
            {
                return View(new JZSEntity() { Success = false, Message = "附加结算交割信息失败，请重试！" });
            }

            if (user.Count == 0)
            {
                return View(new JZSEntity() { Success = false, Message = "无法找到所选择的结算交割信息，请重试！" });
            }

            var entity = new JZSEntity();

            entity.requestname = response[0].requestname;
            entity.clientname = user[0].nickname;
            entity.servername = "通辽流转大厅";

            return View(entity);
        }

        #endregion

        /// <summary>
        /// 获取尚未审核的委托申请
        /// </summary>
        [HttpPost]
        public JsonResult GetUnServedRequestList()
        {
            try
            {
                var request = new ProcessRequest();
                request.userid = GetCurrentLoginUserID();

                var result = processService.AttachUnServedRequest(request);

                if (result != null)
                {
                    result.ForEach(item =>
                    {
                        item.requestcontent = item.requestcontent.Replace('$',' ');
                    });
                }

                return Json(new { success = true, data = result }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// 获取审核完毕的委托申请
        /// </summary>
        [HttpPost]
        public JsonResult GetServedRequestList()
        {
            try
            {
                var request = new ProcessRequest();
                request.userid = GetCurrentLoginUserID();

                var result = processService.AttachServedRequest(request);

                if (result != null)
                {
                    result.ForEach(item =>
                    {
                        item.requestcontent = item.requestcontent.Replace('$', ' ');
                    });
                }

                return Json(new { success = true, data = result }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        private int GetCurrentLoginUserID()
        {
            var nv = cookie.GetCookieCollection("FuNong.UserInfo.Login");
            string userid = string.Empty;
            if (nv != null)
                userid = nv["FuNong.Login.UserID"];
            else
                throw new Exception("用户登陆过期！");

            int userIDInt;
            if (!Int32.TryParse(userid, out userIDInt))
            {
                cookie.ClearCooke("FuNong.Login.UserID");
                throw new Exception("用户编号存储错误！");
            }

            return userIDInt;
        }

        private int GetCurrentLoginRoleID()
        {
            var nv = cookie.GetCookieCollection("FuNong.UserInfo.Login");
            string userid = string.Empty;
            if (nv != null)
                userid = nv["FuNong.Login.RoleID"];
            else
                throw new Exception("用户登陆过期！");

            int roleIDInt;
            if (!Int32.TryParse(userid, out roleIDInt))
            {
                cookie.ClearCooke("FuNong.Login.RoleID");
                throw new Exception("角色编号存储错误！");
            }

            return roleIDInt;
        }

        /// <summary>
        /// 文件上传
        /// </summary>
        [HttpPost]
        public JsonResult SoilImagesUpload()
        {
            var fileObj = Request.Files["upload"];
            if (fileObj == null)
                return Json(new { success = false, message = "无法侦测到上传文件!" });

            var fileName = fileObj.FileName;
            var fileDirectory  = "../UploadFile/SoilImages/";
            var timestamp = DateTime.Now;

            var savePath = Server.MapPath(fileDirectory) + timestamp.ToString("yyyyMMdd") + "/";
            var changedFileName = timestamp.ToString("yyyyMMddhhmmss") + fileName;
            
            if (!Directory.Exists(savePath))
                Directory.CreateDirectory(savePath);

            var fullPath = Path.Combine(savePath, changedFileName);
            var absoPath = Path.Combine(fileDirectory, timestamp.ToString("yyyyMMdd") + "/", changedFileName);


            try
            {
                fileObj.SaveAs(fullPath);
                return Json(new { success = true, path = absoPath, message = "文件上传成功!" });
            }
            catch(Exception ex)
            {
                return Json(new { success = false, message = ex.Message });
            }
        }

        /// <summary>
        /// 通过parentid获取相关信息
        /// parentid为空或者0 获取顶级土壤类别
        /// parentid不为空，不为0，获取子级土壤类别
        /// </summary>
        [HttpPost]
        public JsonResult GetSoilTypeList(string parentid)
        {
            int idConvert = 0;
            if (!string.IsNullOrEmpty(parentid) && !parentid.Equals(0))
            {
                if (!Int32.TryParse(parentid, out idConvert))
                {
                    logger.Error("流程处理部分：土壤编号不能为非数字型！");
                    return Json(new { success = false, message = "土壤编号不能为非数字型！" }, JsonRequestBehavior.AllowGet);
                }
            }

            var soilRequest = new SoilAttributeRequest() { soiltypeid = idConvert };
            var soilList = soilService.GetSoilType(soilRequest);

            return Json(new { success = true, data = soilList },JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 获取流转方式列表
        /// </summary>
        [HttpPost]
        public JsonResult GetChangeTypeList()
        {
            var changeRequest = new SoilAttributeRequest() { changetypeid = -1 };
            var changeList = soilService.GetChangeType(changeRequest);

            return Json(new { success = true, data = changeList }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 获取省份，市区，区县列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetLocationList(string provinceid, string cityid)
        {
            int provinceidInt, cityidInt;

            if (string.IsNullOrEmpty(provinceid))
                return Json(new { success = false, message = "省份编号不能为空！" }, JsonRequestBehavior.AllowGet);
            if (string.IsNullOrEmpty(cityid))
                return Json(new { success = false, message = "市区编号不能为空！" }, JsonRequestBehavior.AllowGet);

            if (!Int32.TryParse(provinceid, out provinceidInt))
                return Json(new { success = false, message = "省份编号不能为非数字型！" }, JsonRequestBehavior.AllowGet);
            if (!Int32.TryParse(cityid, out cityidInt))
                return Json(new { success = false, message = "市区编号不能为非数字型！" }, JsonRequestBehavior.AllowGet);

            if (provinceidInt == -1 && cityidInt == -1)  //获取所有的省份
            {
                var provinceList = soilService.GetProvince();
                return Json(new { success = true, data = provinceList }, JsonRequestBehavior.AllowGet);
            }

            if (provinceidInt != -1)
            {
                var cityList = soilService.GetCityByProvinceID(new SoilAttributeRequest() { provinceid = provinceidInt });
                return Json(new { success = true, data = cityList }, JsonRequestBehavior.AllowGet);
            }

            if (cityidInt != -1)
            {
                var districtList = soilService.GetDistrictByCityID(new SoilAttributeRequest() { cityid = cityidInt });
                return Json(new { success = true, data = districtList }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { success = false, message = "您的参数有误，请重试！" }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 获取可流转年限，剩余流转年限
        /// </summary>
        [HttpPost]
        public JsonResult GetChangeYears()
        {
            List<ChangeYear> list = new List<ChangeYear>();
            for (var i = 1; i <= 70; i++)
            {
                list.Add(new ChangeYear() { id = i, value = i.ToString() });
            }

            return Json(new { success = true, data = list }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 获取土地周边
        /// </summary>
        [HttpPost]
        public JsonResult GetSoilAround(string flag)
        {
            if (string.IsNullOrEmpty(flag))
                return Json(new { success = false, message = "周边设施标记不能为空！" }, JsonRequestBehavior.AllowGet);

            SoilAttributeRequest request = new SoilAttributeRequest();

            if (flag.ToLower().Equals("soilground"))  //地上物
                request = new SoilAttributeRequest() { soilaroundflag = "SoilGround" };

            if (flag.ToLower().Equals("earthtype"))  //土壤类型
                request = new SoilAttributeRequest() { soilaroundflag = "EarthType" };

            if (flag.ToLower().Equals("water"))   //水源
                request = new SoilAttributeRequest() { soilaroundflag = "Water" };

            if (flag.ToLower().Equals("around"))  //周边设备
                request = new SoilAttributeRequest() { soilaroundflag = "Around" };

            var result = soilService.GetSoilAroundList(request);
            return Json(new { success = true, data = result }, JsonRequestBehavior.AllowGet);
        }

        #region 测试数据
        [HttpPost]
        public JsonResult Process8Demo()
        {
            var list = new List<Process8DemoEntity>();

            var p2 = new Process8DemoEntity();
            p2.id = 1;
            p2.requestname = "刘辉";
            p2.requestphone = "158200000125";
            p2.clientname = "张川";
            p2.clientphone = "13724589654";
            p2.infomain = "通辽蔬菜大棚出售";
            p2.changetype = "转包";
            p2.chargefee = "手续费：876.00元整";
            p2.isapprove = 1;
            p2.signtime = DateTime.Now.AddDays(-11);
            p2.recordorder = 1;
            p2.updatetime = DateTime.Now.AddDays(-10);
            list.Add(p2);


            var p3 = new Process8DemoEntity();
            p3.id = 1;
            p3.requestname = "刘军";
            p3.requestphone = "1593234234";
            p3.clientname = "张川";
            p3.clientphone = "13724589654";
            p3.infomain = "通辽八年生幼林林地转让";
            p3.changetype = "转租";
            p3.chargefee = "手续费：1450.00元整";
            p3.isapprove = 1;
            p3.signtime = DateTime.Now.AddDays(-11);
            p3.recordorder = 1;
            p3.updatetime = DateTime.Now.AddDays(-10);
            list.Add(p3);

            var json = new
            {
                total = 3,
                rows = list
            };
            return Json(json, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Process5Demo()
        {
            var list = new List<Process5DemoEntity>();
            var p1 = new Process5DemoEntity();
            p1.id = 1;
            p1.requestname = "刘成";
            p1.requestphone = "15939393952";
            p1.infomain = "内蒙古20000余亩草原转让";
            p1.isarrange = 0;
            p1.issuccess = 0;
            p1.recordorder = 1;
            p1.updatetime = DateTime.Now.AddDays(-10);
            list.Add(p1);

            var p2 = new Process5DemoEntity();
            p2.id = 2;
            p2.requestname = "刘辉";
            p2.requestphone = "158200000125";
            p2.infomain = "通辽蔬菜大棚出售";
            p2.isarrange = 1;
            p2.issuccess = 1;
            p2.recordorder = 1;
            p2.updatetime = DateTime.Now.AddDays(-10);
            list.Add(p2);

            var p21 = new Process5DemoEntity();
            p21.id = 2;
            p21.requestname = "王建业";
            p21.requestphone = "13724589654";
            p21.infomain = "通辽蔬菜大棚出售";
            p21.isarrange = 1;
            p21.issuccess = 0;
            p21.recordorder = 1;
            p21.updatetime = DateTime.Now.AddDays(-10);
            list.Add(p21);

            var p3 = new Process5DemoEntity();
            p3.id = 3;
            p3.requestname = "刘军";
            p3.requestphone = "1593234234";
            p3.infomain = "通辽八年生幼林林地转让";
            p3.isarrange = 1;
            p3.issuccess = 1;
            p3.recordorder = 1;
            p3.updatetime = DateTime.Now.AddDays(-10);
            list.Add(p3);

            var json = new
            {
                total = 3,
                rows = list
            };
            return Json(json, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Process3Demo()
        {
            var list = new List<Process3DemoEntity>();
            var p1 = new Process3DemoEntity();
            p1.id =  1;
            p1.clientidcard = "4115555555555555";
            p1.clientname = "张川";
            p1.soilcertificateimages = "";
		    p1.serveridcard= "4115555555555555";
            p1.servername= "通辽土地流转交易大厅";
            p1.recordorder =  1;
            p1.updatetime = DateTime.Now.AddDays(-1);
            p1.isapprove = 1;
            list.Add(p1);

            var p2 = new Process3DemoEntity();
            p2.id = 2;
            p2.clientidcard = "41155555568794123";
            p2.clientname = "刘杰";
            p2.soilcertificateimages = "";
            p2.serveridcard = "4115555555555555";
            p2.servername = "通辽土地流转交易大厅";
            p2.recordorder = 1;
            p2.updatetime = DateTime.Now.AddDays(-2);
            p2.isapprove = 0;
            list.Add(p2);

            var p3 = new Process3DemoEntity();
            p3.id = 3;
            p3.clientidcard = "3723271829587462";
            p3.clientname = "郝建安";
            p3.soilcertificateimages = "";
            p3.serveridcard = "4115555555555555";
            p3.servername = "通辽土地流转交易大厅";
            p3.recordorder = 1;
            p3.updatetime = DateTime.Now.AddDays(-5);
            p3.isapprove = 1;
            list.Add(p3);

            var json = new
            {
                total = 3,
                rows=list
            };
            return Json(json,JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}
