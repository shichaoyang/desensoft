﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FuNong.UserInterface.Models
{
    public class functions
    {

        public string functionflag { get; set; }
        public string functionname { get; set; }
        public string functionicon { get; set; }

        public string functionorder { get; set; }
        public bool state { get; set; }
    }
}