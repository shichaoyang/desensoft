﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FuNong.UserInterface.Models
{
    public class JZSEntity
    {
        public string clientname { get; set; }   //委托方
        public string requestname { get; set; }  //意向方
        public string servername { get; set; }  //受托方

        public bool Success { get; set; }
        public string Message { get; set; }
    }
}