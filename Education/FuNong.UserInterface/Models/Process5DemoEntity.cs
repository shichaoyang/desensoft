﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FuNong.UserInterface.Models
{
    public class Process5DemoEntity
    {
        public int id { get; set; }
        public string requestname  { get; set; }
        public string requestphone { get; set; }
        public string requestnote { get; set; }
        public string infomain { get; set; }
        public int isarrange { get; set; }
        public int issuccess  { get; set; }
        public int? recordorder { get; set; }
        public DateTime? updatetime { get; set; }
    }
}