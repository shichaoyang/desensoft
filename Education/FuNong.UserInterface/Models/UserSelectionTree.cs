﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FuNong.UserInterface.Models
{
    public class UserSelectionTree
    {
        public string id { get; set; }

        public string pId { get; set; }

        public string name { get; set; }

        public bool isParent { get; set; }

        public string iconOpen { get; set; }  //设置打开图标
          
        public string iconClose { get; set; }  //设置收拢图标

        public bool open { get; set; }   //是否展开
          
        public bool doCheck { get; set; }   //当前节点是否允许被勾选
    }
}