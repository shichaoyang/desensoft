﻿var pageSize = 17;
var pageList = [17, 30, 50];
var DetailHeight = 210;
var DataURL = "";
var DetailURL = "";
var ToolBar = [];

$(document).ready(function () {
    $('#dg').datagrid({
        url: DataURL,
        singleSelect: true,
        height: 'auto',
        nowrap: false,
        striped: true,
        border: true,
        collapsible: true,
        fit: true,
        pagination: true, //分页控件   
        pageSize: pageSize, //每页显示的记录条数
        pageList: pageList, //可以设置每页记录条数的列表   
        method: 'post',
        rownumbers: true, //行号   
        toolbar:ToolBar,
        columns: ColumnData,
        view: detailview,
        detailFormatter: function (index, row) {
            return '<div class="ddv" style="padding:5px 0"></div>';
        },
        onExpandRow: function (index, row) {
            var ddv = $(this).datagrid('getRowDetail', index).find('div.ddv');
            ddv.panel({
                height: DetailHeight,
                border: false,
                cache: false,
                href: eval(DetailURL),
                onLoad: function () {
                    $('#dg').datagrid('fixDetailRowHeight', index);
                }
            });
            $('#dg').datagrid('fixDetailRowHeight', index);
        }
    });

    //设置分页控件   
    var p = $('#dg').datagrid('getPager');
    $(p).pagination({
        beforePageText: '第', //页数文本框前显示的汉字   
        afterPageText: '页    共 {pages} 页',
        displayMsg: '当前显示 {from} - {to} 条记录   共 {total} 条记录'
    });
 //   AddSearchBar();
});

var AddSearchBar = function () {
    var fields = $('#dg').datagrid('getColumnFields');
    for (var i = 0; i < fields.length; i++) {
        var opts = $('#dg').datagrid('getColumnOption', fields[i]);
        if (opts.title != undefined) {
            var muit = "<div name='" + fields[i] + "'>" + opts.title + "</div>";
            $('#TypeSelect').html($('#TypeSelect').html() + muit);
        }
    }
    $('.searchbox').appendTo('.datagrid-toolbar').css("margin-top", "3px");
    $('#ValueInput').appendTo('.datagrid-toolbar');
    $(".datagrid-toolbar table").css("float", "left");   //让SearchBox在同一行显示的关键语句
    $('#ValueInput').searchbox({
        menu: '#TypeSelect',
        prompt: '请输入待查询的值',
        searcher: function (value, name) {
            if (value == "") {
                alert("无输入值，请重试！");
            }
            else {
                alert(name + ":" + value);
            }
        }
    });
}

var formatDateTimeJSON = function (value) {
    if (value == null || value == '') {
        return '';
    }
    var dt;
    if (value instanceof Date) {
        dt = value;
    }
    else {
        dt = new Date(value);
        if (isNaN(dt)) {
            value = value.replace(/\/Date\((-?\d+)\)\//, '$1'); //标红的这段是关键代码，将那个长字符串的日期值转换成正常的JS日期格式
            dt = new Date();
            dt.setTime(value);
        }
    }
    return dt.getFullYear() + "年" + (dt.getMonth() + 1) + "月" + dt.getDate() + "日";   //这里用到一个javascript的Date类型的拓展方法，这个是自己添加的拓展方法，在后面的步骤3定义
}