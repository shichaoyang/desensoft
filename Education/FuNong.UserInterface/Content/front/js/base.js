﻿
$(document).ready(function () {
    navigate();
    scrollnews();
});

function navigate() {
    var menuid = getUrlVars()["menu"];
    if (menuid == undefined)
        menuid = 1;
    resetclass();
    $("#menu" + menuid).attr("class", "menuselected");
}

function resetclass() {
    $(".menu a").each(function () {
        $(this).attr("class", "menuunselected");
    });
}

function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

function scrollnews() {
        $('.notifycontent a').eq(0).fadeOut("slow", function () {
            $(this).clone().appendTo($(this).parent()).fadeIn("slow");
            $(this).remove();
    });
}
setInterval('scrollnews()', 3000);