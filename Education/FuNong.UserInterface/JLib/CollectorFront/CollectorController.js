﻿app.controller('collectorController', ['$scope', '$cookies', '$timeout','$compile', 'baseService', 'collectorService', 'uiGridConstants', function ($scope, $cookies,$timeout,$compile, baseService, collectorService, uiGridConstants) {
    var self = this;

    $scope.ProvinceData = null;
    $scope.CityData = null;
    $scope.DistrictData = null;
    $scope.CompanyData = null;
    $scope.MachineData = null;
    $scope.RealTimeData = null;
    $scope.HistoryData = null;

    $scope.selectedProvince = null;
    $scope.selectedCity = null;
    $scope.selectedDistrict = null;
    $scope.selectedCompany = null;
    $scope.selectedMachine = null;

    $scope.starttime = null;
    $scope.endtime = null;

    $scope.visible = false;  //datetimepicker是否显示

    //art.dialog({ title: '加载提示', icon: 'face-smile', fixed: true,left:'50%',top:0, time:3, content: "日期选择跨度不要过大，否则会因为数据量过大而无法加载图表和列表！", padding: 0 });

    //监测省份的变化，如果发生了变化，则加载城市列表
    $scope.$watchCollection('selectedProvince', function (oldval, newval) {
        $scope.GetCityList();
    });

    //监测城市变化，如果发生了变化，则加载地区列表
    $scope.$watchCollection('selectedCity', function (oldval, newval) {
        $scope.GetDistrictList();
    });
    //监测地区变化，如果发生了变化，则加载公司列表
    $scope.$watchCollection('selectedDistrict', function (oldval, newval) {
        $scope.GetCompanyList();
    });
    //监测公司变化，如果发生了变化，则加载机器列表
    $scope.$watchCollection('selectedCompany', function (oldval, newval) {
        $scope.GetMachineList();
    });

    //绑定列表
    $scope.gridOptions = {
        enableRowSelection: true,
        enableSelectAll: false,
        selectionRowHeaderWidth: 35,
        rowHeight: 35,
        showGridFooter: false,
        multiSelect: true,
        enablePaginationControls: true,
        paginationPageSizes: [9, 15, 20],
        paginationPageSize: 9
    };

    $scope.gridOptions.columnDefs = [
			{ name: 'Param_name', displayName: '参数名称' },
			{ name: 'Param_unit', displayName: '参数单位' },
            { name: 'Param_data', displayName: '参数值' },
            { name: 'Param_time', displayName: '采集时间' }
    ];

    $scope.gridOptions.onRegisterApi = function (gridApi) {
        $scope.gridApi = gridApi;
    };

    //省份绑定
    collectorService.GetProvinceData().then(function (data) {
        var flag = data.data.success;
        if (flag) {
            $scope.ProvinceData = data.data.data;
            $scope.selectedProvince = baseService.getSelectedDataMapper($scope.ProvinceData, 'province');
        }
    }, null);

    $scope.GetCityList = function () {
        var selectedProvinceId;
        if ($scope.selectedProvince != undefined)
            selectedProvinceId = $scope.selectedProvince.id;
        else
            return;
        //市区绑定
        collectorService.GetCityData(selectedProvinceId).then(function (data) {
            var flag = data.data.success;
            if (flag) {
                $scope.CityData = data.data.data;
                $scope.selectedCity = baseService.getSelectedDataMapper($scope.CityData, 'city');
            }
        }, null);
    }

    $scope.GetDistrictList = function () {
        var selectedCityId;
        if ($scope.selectedCity != undefined)
            selectedCityId = $scope.selectedCity.id;
        else
            return;
        //区县绑定
        collectorService.GetDistrictData(selectedCityId).then(function (data) {
            var flag = data.data.success;
            if (flag) {
                $scope.DistrictData = data.data.data;
                $scope.selectedDistrict = baseService.getSelectedDataMapper($scope.DistrictData, 'district');
            }
        }, null);
    }

    $scope.GetCompanyList = function () {
        var selectedDistrictId;
        if ($scope.selectedDistrict != undefined)
            selectedDistrictId = $scope.selectedDistrict.id;
        else
            return;
        //公司绑定
        collectorService.GetCompanyData(selectedDistrictId).then(function (data) {
            var flag = data.data.success;
            if (flag) {
                $scope.CompanyData = data.data.data;
                $scope.selectedCompany = baseService.getSelectedDataMapper($scope.CompanyData, 'company');
            }
        }, null);
    }

    $scope.GetMachineList = function () {
        var selectedCompanyId;
        if ($scope.selectedCompany != undefined)
            selectedCompanyId = $scope.selectedCompany.id;
        else
            return;
        //设备绑定
        collectorService.GetMachineList(selectedCompanyId).then(function (data) {
            var flag = data.data.success;
            if (flag) {
                $scope.MachineData = data.data.data;
                $scope.selectedMachine = baseService.getSelectedDataMapper($scope.MachineData, 'machine');
            }
        }, null);
    }

    //获取实时数据
    $scope.GetRealTimeDataByMachine = function () {
        timeCheck($scope.starttime, $scope.endtime);
        var starttimeFmt = timeFmt($scope.starttime);
        var endtimeFmt = timeFmt($scope.endtime);
        //获取实时数据
        var machineId = $scope.selectedMachine.machine_id;
        collectorService.GetRealDataList(machineId).then(function (data) {
            var flag = data.data.success;
            if (flag) {
                $scope.RealTimeData = data.data.data;
            }
        }, null);
        //获取列表数据
        collectorService.GetHistoryDataList(machineId,starttimeFmt,endtimeFmt).then(function (data) {
            var flag = data.data.success;
            if(flag)
            {
                $scope.HistoryData = data.data.data;
                $scope.gridOptions.data = data.data.list;
            }
        }, null);
        //将级联列表项放到cookie中，以便于之后的操作简易化
        var expireDate = new Date();
        expireDate.setDate(expireDate.getDate() + 7);
        delete $cookies['frontselection'];

        var cookieData = JSON.stringify({
            province: $scope.selectedProvince,
            city: $scope.selectedCity,
            district: $scope.selectedDistrict,
            company: $scope.selectedCompany,
            machine: $scope.selectedMachine
        });
        $cookies.put('frontselection', cookieData, { 'expires': expireDate });
    }

    $scope.ClickToGetParamDataList = function (paramId) {
      
        timeCheck($scope.starttime,$scope.endtime);

        var starttimeFmt = timeFmt($scope.starttime);
        var endtimeFmt = timeFmt($scope.endtime);

        var machineId = $scope.selectedMachine.machine_id;
        collectorService.GetHistoryDataListByParamId(machineId, paramId, starttimeFmt, endtimeFmt).then(function (data) {
            var flag = data.data.success;
            if (flag) {
                $scope.gridOptions.data = data.data.list;
            }
        });
    }

    //显示隐藏时间段选择
    $scope.ClickToShowTimePicker = function () {
        $scope.visible = !$scope.visible;
    }

    $scope.onTimeSet = function (newDate, oldDate) {
        var startTimeFmt = moment($scope.starttime).format("YYYY-MM-DD");
        var endTimeFmt = moment(startTimeFmt).add(1, 'day').format("YYYY-MM-DD");
     
        //时间框置空
        $("#endtime input").val("");
        //移除原有的datetimepicker对象
        $("#endContainer").children().remove();
        //设置config
        $scope.config = { dropdownSelector: "#endtime", startView: "day", minView: "day" };
        //动态编译datetimepicker directive
        var compiledeHTML = $compile('<datetimepicker data-ng-model="endtime"  data-before-render="beforeRender($view, $dates, $leftDate, $upDate, $rightDate)" data-datetimepicker-config="{{config}}" />')($scope);
        //放入html容器
        $("#endContainer").append(compiledeHTML);
    }

    //接收事件，并重置页面
    $scope.$on('days-check', function (e, d) {
        for (var i = 0; i < d.length; i++) {
            //初始设置为不可选状态,不选中状态
            d[i].active = false;
            d[i].selectable = false;
            //当前loop的值
            var currentDate = moment(d[i].utcDateValue).format("YYYY-MM-DD");
            //当前选定的开始时间
            var startTimeFmt = moment($scope.starttime).format("YYYY-MM-DD");
            //允许选定的最大的结束时间
            var endTimeFmt = moment(startTimeFmt).add(1, 'day').format("YYYY-MM-DD");
            //比较并设置可选日期
            if (currentDate >= startTimeFmt && currentDate <= endTimeFmt) {
                d[i].selectable = true;
            }
        }
    });

    //当选择开始时间，会进入onTimeSet事件，执行到$compile的时候，就会触发下面的beforeRender事件
    //触发beforeReder事件后，会抛出一个days-check事件出去，并附带所有的当页时间对象。
    $scope.beforeRender = function ($view, $dates, $leftDate, $upDate, $rightDate) {
        $timeout(function () {
            $scope.$broadcast('days-check', $dates);
        });
    }

    var timeCheck = function(start,end)
    {
        if (start == null && end!=null)
        {
            art.dialog({ title: '提示', icon: 'error', time: 6, content: "必须选择开始日期，请重试！", padding: 0 });
            return;
        }
        if (start!=null && end == null)
        {
            art.dialog({ title: '提示', icon: 'error', time: 6, content: "必须选择结束日期，请重试！", padding: 0 });
            return;
        }
        if (start != null && end != null && start > end) {
            art.dialog({ title: '提示', icon: 'error', time: 6, content: "开始时间不能大于结束时间，请重试！", padding: 0 });
            return;
        }
    }

    var timeFmt = function(time)
    {
        if (time == null)
            time = "";
        else
            time = time.toLocaleDateString();
        return time;
    }
}]);

