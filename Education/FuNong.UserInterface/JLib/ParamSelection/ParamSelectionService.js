﻿app.service('paramService', ['$http',function ($http) {
    var getData = function () {
        return $http({
            method: 'POST',
            url: '/BaseData/GetBaseSettingBy?groupname=param'
        });
    }

    return {
        paramData: getData,
    };
}]);
