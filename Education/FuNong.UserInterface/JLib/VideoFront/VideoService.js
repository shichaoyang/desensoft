﻿app.service('videoService', ['$http', function ($http) {
    var provinceData = function () {
        return $http({
            method: 'POST',
            url: '/Process/GetLocationList?provinceid=-1&cityid=-1'
        });
    }

    var cityData = function (provinceId) {
        return $http({
            method: 'POST',
            url: '/Process/GetLocationList?provinceid=' + provinceId + '&cityid=-1'
        });
    }

    var districtData = function (cityId) {
        return $http({
            method: 'POST',
            url: '/Process/GetLocationList?provinceid=-1&cityid=' + cityId
        });
    }

    var companyData = function (districtId) {
        return $http({
            method: 'POST',
            url: '/BaseData/GetSchoolByDistrictId?districtId=' + districtId
        });
    }

    var videoList = function (companyId) {
        return $http({
            method: 'POST',
            url: '/Machine/GetVideoListBy?companyId=' + companyId 
        });
    }

    return {
        GetProvinceData: provinceData,
        GetCityData: cityData,
        GetDistrictData: districtData,
        GetCompanyData: companyData,
        GetMonitorList: videoList
    };
}]);
