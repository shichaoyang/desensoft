﻿app.service('hubService', ['$http',function ($http) {

    //添加对自动生成的Hub的引用
    var chat = $.connection.chatHub;
    
    //$.connection.hub.start().done(function () { });
    //启动链接
    var hubReady = $.connection.hub.start();

    if (hubReady.done(function () {
        setInterval(function () {
                //启动心跳
                chat.server.sendHeartBeatToMiddleware();
            }, 20000);
    }));

    //获取数据
    var machineList = function (companyId, machineId) {
        return $http({
            method: 'POST',
            url: '/Machine/GetMachineDetailsByID?companyId=' + companyId + "&machineId=" + machineId
        });
    }

    //连接服务端
    var connectServer = function () {
        if (hubReady.done(function () {
           chat.server.connectToServer();
        }).fail(function () {
           alert("Hub Service 启动失败，请重试!");
        }));
    }

    //发送命令到服务端
    var sendCommand = function (machineid,routerid, commandInfo) {
        if (hubReady.done(function () {
            chat.server.sendCommandToServer(chat.connection.id,machineid,routerid,commandInfo);
        }).fail(function () {
            alert("Hub Service 启动失败，请重试!");
        }));
    }

    //发送数据到hub端
    var sendData = function (dataInfo) {
        if (hubReady.done(function () {
            chat.server.getCurrentControllerData(dataInfo);
        }).fail(function () {
            alert("Hub Service 启动失败，请重试!");
        }));
    }

    //接收服务端命令
    var commandReceived = function (callback) {
        if (callback) {
            //调用Hub的callback回调方法
            chat.client.printCommand = function (command) {
                var data = machineList();
                var obj = { data: data, command: command };
                return callback(obj);
            }
        }
    }

    return {
        commandSent: sendCommand,
        commandReceived: commandReceived,
        dataSent:sendData,
        controllerData: machineList,
        serverConnect: connectServer
    };
}]);