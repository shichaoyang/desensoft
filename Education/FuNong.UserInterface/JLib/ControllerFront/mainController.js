﻿var mainController = app.controller('mainController', ['$scope', '$timeout', '$window', '$cacheFactory', '$cookies', 'baseService', 'hubService', 'collectorService', function ($scope, $timeout, $window, $cacheFactory, $cookies, baseService, hubService, collectorService) {

    //加载省市县公司联动菜单
    $scope.ProvinceData = null;
    $scope.CityData = null;
    $scope.DistrictData = null;
    $scope.CompanyData = null;
    $scope.MachineData = null;

    $scope.selectedProvince = null;
    $scope.selectedCity = null;
    $scope.selectedDistrict = null;
    $scope.selectedCompany = null;
    $scope.selectedMachine = null;

    $scope.controlData = null;

    $scope.commandMessage = null;

    $scope.cacheCurrentKey = null;

    $scope.keys = [];
    $scope.cache = $cacheFactory("controllercachedata");

    //省份绑定
    collectorService.GetProvinceData().then(function (data) {
        var flag = data.data.success;
        if (flag) {
            $scope.ProvinceData = data.data.data;
            $scope.selectedProvince = baseService.getSelectedDataMapper($scope.ProvinceData, 'province');
        }
    }, null);

    $scope.GetCityList = function () {
        var selectedProvinceId = $scope.selectedProvince.id;
        //市区绑定
        collectorService.GetCityData(selectedProvinceId).then(function (data) {
            var flag = data.data.success;
            if (flag) {
                $scope.CityData = data.data.data;
                $scope.selectedCity = baseService.getSelectedDataMapper($scope.CityData, 'city');
            }
        }, null);
    }

    $scope.GetDistrictList = function () {
        var selectedCityId = $scope.selectedCity.id;
        //区县绑定
        collectorService.GetDistrictData(selectedCityId).then(function (data) {
            var flag = data.data.success;
            if (flag) {
                $scope.DistrictData = data.data.data;
                $scope.selectedDistrict = baseService.getSelectedDataMapper($scope.DistrictData, 'district');
            }
        }, null);
    }
  
    $scope.GetCompanyList = function () {
        var selectedDistrictId = $scope.selectedDistrict.id;
        //公司绑定
        collectorService.GetCompanyData(selectedDistrictId).then(function (data) {
            var flag = data.data.success;
            if (flag) {
                $scope.CompanyData = data.data.data;
                $scope.selectedCompany = baseService.getSelectedDataMapper($scope.CompanyData, 'company');
            }
        }, null);
    }

    $scope.GetMachineList = function () {
        var selectedCompanyId = $scope.selectedCompany.id;
        //设备绑定
        collectorService.GetMachineList(selectedCompanyId).then(function (data) {
            var flag = data.data.success;
            if (flag) {
                $scope.MachineData = data.data.data;
                $scope.selectedMachine = baseService.getSelectedDataMapper($scope.MachineData, 'machine');
            }
        }, null);
    }

    $scope.RefreshPage = function () {
        var expireDate = new Date();
        expireDate.setDate(expireDate.getDate() + 7);
        delete $cookies['frontselection'];
        var cookieData = JSON.stringify({
            province: $scope.selectedProvince,
            city: $scope.selectedCity,
            district: $scope.selectedDistrict,
            company: $scope.selectedCompany,
            machine: $scope.selectedMachine
        });

        $cookies.put('frontselection', cookieData, { 'expires': expireDate });

        $timeout(function () {
            $window.location.reload();
        }, 10);
    }

    //监测省份的变化，如果发生了变化，则加载城市列表
    $scope.$watch('selectedProvince', function (oldval, newval) {
        $scope.GetCityList();
    });

    //监测城市变化，如果发生了变化，则加载地区列表
    $scope.$watch('selectedCity', function (oldval, newval) {
        $scope.GetDistrictList();
    });
    //监测地区变化，如果发生了变化，则加载公司列表
    $scope.$watch('selectedDistrict', function (oldval, newval) {
        $scope.GetCompanyList();
    });
    //监测公司变化，如果发生了变化，则加载机器列表
    $scope.$watch('selectedCompany', function (oldval, newval) {
        $scope.GetMachineList();
    });

    //获取控制器列表
    $scope.LoadControllerList = function () {
        var selectedCompnayId = $scope.selectedCompany.id;
        var selectedMachineId = $scope.selectedMachine.machine_id;

        var key = selectedCompnayId + "|" + selectedMachineId;
        $scope.cacheCurrentKey = key;

        //如果cache中有值，则直接从cache中取
        var result = $scope.cache.get(key);
        console.log(result);
        if (result == undefined || result == null) {
            //否则从service中通过http请求，然后做一下缓存
            hubService.controllerData(selectedCompnayId, selectedMachineId).then(function (data) {
                if (data.data.success) {
                    var controllerData = data.data.data[0];
                    //需要在这里对数据进行缓存
                    put(key, controllerData);

                    $scope.controlData = controllerData;
                    //发送machine信息到后台，以便于通知后台当前加载的设备信息
                    hubService.dataSent(controllerData);
                }
            });
        }
        else {
            $scope.controlData = result;
            hubService.dataSent(result);
        }

        $scope.commandMessage = "发出请求:请求设备状态中 " + $scope.showCurrentTime();

        //将级联列表项放到cookie中，以便于之后的操作简易化
        debugger;
        var expireDate = new Date();
        expireDate.setDate(expireDate.getDate() + 7);
        delete $cookies['frontselection'];

        var cookieData = JSON.stringify({
            province: $scope.selectedProvince,
            city: $scope.selectedCity,
            district: $scope.selectedDistrict,
            company: $scope.selectedCompany,
            machine: $scope.selectedMachine
        });

        $cookies.put('frontselection', cookieData, { 'expires': expireDate });
    }

    //将数据放入缓存中
    var put = function (key, value) {
        if (angular.isUndefined($scope.cache.get(key))) {
            $scope.keys.push(key);
        }
        $scope.cache.put(key, angular.isUndefined(value) ? null : value);
    }
  
    //绑定控制部分
    hubService.commandReceived(function (result) {
        var command = result.command;
        $scope.commandMessage = "收到反馈:" + command + " " + $scope.showCurrentTime();

        if (command.indexOf("掉线") > -1) {
            hubService.serverConnect();
            $scope.commandMessage = "正在重试上线中,请稍候..." + $scope.showCurrentTime();
        }

        if (command.indexOf('|') < 0)
            return;

        var cacheData = $scope.cache.get($scope.cacheCurrentKey);
        debugger;

        var commandArray = command.split("|");

        var acceptMsgType = commandArray[2];  // 007:开关主动上报  008:指示灯主动上报
        var acceptMachine = commandArray[3];  //设备ID
        var acceptRrState = commandArray[4];  //路数和状态

        var acceptRrStateArray = acceptRrState.split('');
        var acceptRrStateArrayLength = acceptRrStateArray.length;

        //确认设备
        if (cacheData.machineid == acceptMachine) {
            //开关主动上报
            if (acceptMsgType == "007") {
                //如果有的设备没有控制器部分
                if (cacheData.routers.length == 0)
                    return;

                for (var i = 0; i < acceptRrStateArrayLength; i++) {
                    var currentRouteID = i + 1;
                    var currentRouteFlag = acceptRrStateArray[i];

                    for (var j = 0; j < acceptRrStateArrayLength; j++) {
                        var loopRouteID = cacheData.routers[j].routeid;
                        //如果属于同一个
                        if (loopRouteID == currentRouteID) {
                            var funcLength = cacheData.routers[j].functionsList.length;
                            for (var x = 0; x < funcLength; x++) {

                                if (cacheData.routers[j].functionsList[x].functionflag == currentRouteFlag) {
                                    cacheData.routers[j].functionsList[x].state = true;
                                }
                                else {
                                    cacheData.routers[j].functionsList[x].state = false;
                                }
                            }
                        }
                    }
                }
            }
            debugger;
            //指示灯主动上报
            if (acceptMsgType == "008") {
                //如果有设备没有指示灯部分
                if (cacheData.leds.length == 0)
                    return;

                for (var i = 0; i < acceptRrStateArrayLength; i++) {
                    var currentRouteID = i + 1;
                    var currentRouteFlag = acceptRrStateArray[i];

                    for (var j = 0; j < acceptRrStateArrayLength; j++) {
                        var loopRouteID = cacheData.leds[j].routerid;
                        //如果属于同一个
                        if (loopRouteID == currentRouteID) {
                            var funcLength = cacheData.leds[j].functionsList.length;
                            for (var x = 0; x < funcLength; x++) {
                                if (cacheData.leds[j].functionsList[x].functionflag == currentRouteFlag) {
                                    cacheData.leds[j].functionsList[x].state = true;
                                }
                                else {
                                    cacheData.leds[j].functionsList[x].state = false;
                                }
                            }
                        }
                    }
                }
            }
        }

        $scope.controlData = cacheData;
        $scope.$digest();
    });

    $scope.showCurrentTime = function () {
        var date = new Date();
        var seperator1 = "-";
        var seperator2 = ":";
        var year = date.getFullYear();
        var month = date.getMonth() + 1;
        var strDate = date.getDate();
        if (month >= 1 && month <= 9) {
            month = "0" + month;
        }
        if (strDate >= 0 && strDate <= 9) {
            strDate = "0" + strDate;
        }
        //var currentdate = year + seperator1 + month + seperator1 + strDate
        //        + " " + date.getHours() + seperator2 + date.getMinutes()
        //        + seperator2 + date.getSeconds();
        var currentdate = date.getHours() + seperator2 + date.getMinutes() + seperator2 + date.getSeconds();
        return currentdate;
    }

}]);