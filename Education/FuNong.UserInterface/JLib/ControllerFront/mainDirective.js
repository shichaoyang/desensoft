﻿//主框架
app.directive('mainPart', ['$compile', function ($compile) {
    return {
        restrict: 'AE',
        replace: true,
        scope: { options: "=" },
        //link: function(scope, element, attrs) {
        //    scope.$watchCollection('options', function (newValue, oldValue) {
        //        debugger;
        //        console.log("I see a data change!");
        //    });
        //},
        template: '<div class="panel panel-default" style="margin:10px 10px 10px 10px">'
                + '  <div class="panel-heading">'
                + '    <h3 class="panel-title glyphicon glyphicon-user"><strong>{{options.title}} </strong><span style="color:green;font-size:14px;font-weight:normal;">{{$parent.commandMessage}}</span></h3>'
                + '  </div>'
                + '  <div class="panel-body">'
                + '    <controller-part options="options.routers"></controller-part>'
                + '    <led-part options="options.leds"></led-part>'
                + '  </div>'
                + '</div>'
    };
}]);
//可控制的控制器部分
app.directive('controllerPart', function () {
    return {
        restrict: 'AE',
        replace: true,
        scope: { options: "=options" },
        template: '<div class="panel panel-default">'
                + '  <div class="panel-heading">'
                + '    <h3 class="panel-title">控制器部分</h3>'
                + '  </div>'
                + '  <div class="panel-body">'
                + '    <controller-instance options="options"></controller-instance>'
                + '  </div>'
                + '</div>'
    };
});
//不可控制的指示灯部分
app.directive('ledPart', function () {
    return {
        restrict: 'AE',
        replace: true,
        scope: { options: "=options" },
        template: '<div class="panel panel-default">'
                + '  <div class="panel-heading">'
                + '    <h3 class="panel-title">指示灯部分</h3>'
                + '  </div>'
                + '  <div class="panel-body">'
                + '    <led-instance options="options"></led-instance>'
                + '  </div>'
                + '</div>'
    };
});
//控制器具体的路数
app.directive('controllerInstance', ['hubService', function (hubService) {
    return {
        restrict: 'AE',
        replace: true,
        scope: { options: "=options" },
        controller: function ($scope) {
            //由于包含了两层ng-repeat，所以这里得利用$parent.$parent 两层parent才能找到想要触发的对象
            $scope.sendCmdToServer = function (routerid,func) {
                //console.log(routerid);
                //console.log(func);
                hubService.commandSent($scope.$parent.$parent.options.machineid, routerid, func);
            }
        },
        template: '<div class="panel panel-success" style="float:left;margin-left:20px;" ng-repeat="router in options">'
                 + '   <div class="panel-heading">'
                 + '       <h3 class="panel-title">{{router.title}}(第{{router.routeid}}路)</h3>'
                 + '   </div>'
                 + '   <div class="panel-body">'
                 + '       <div class="btn-group" role="group">'
                 + '           <button type="button" ng-click="$parent.$parent.sendCmdToServer(router.routeid,function)" ng-repeat="function in router.functionsList" tagorder="{{function.functionorder}}" tagflag={{function.functionflag}} class="btn {{function.state|onlineConverter}} glyphicon {{function.functionicon}}">{{function.functionname}}</button>'
                 + '       </div>'
                 + '   </div>'
                 + '</div>'
    };
}]);
//指示器具体的路数
app.directive('ledInstance', function () {
    return {
        restrict: 'AE',
        replace: true,
        scope: { options: "=options" },
        //无需操控，所以不用绑定click事件
        //controller: function ($scope) {
        //    //由于包含了两层ng-repeat，所以这里得利用$parent.$parent 两层parent才能找到想要触发的对象
        //    $scope.sendCmdToServer = function (routerid, func) {
        //        console.log(routerid);
        //        console.log(func);
        //    }
        //},
        template: '<div class="panel panel-success" style="float:left;margin-left:20px;" ng-repeat="led in options">'
                + '  <div class="panel-heading">'
                + '     <h3 class="panel-title">{{led.title}}(第{{led.routerid}}路)</h3>'
                + '  </div>'
                + '  <div class="panel-body">'
                + '     <div class="btn-group" role="group">'
                + '         <button type="button" ng-repeat="function in led.functionsList" tagorder="{{function.functionorder}}" tagflag={{function.functionflag}} class="btn {{function.state|onlineConverter}} glyphicon {{function.functionicon}}">{{function.functionname}}</button>'
                + '     </div>'
                + ' </div>'
                + '</div>'
    };
});

//此过滤器主要是为了过滤工作状态的，将true和false转变为具体的css样式。
app.filter('onlineConverter', function () {
    return function (input) {
        if (input) {
            return "btn-success";
        }
        else {
            return "btn-default";
        }
    }
});