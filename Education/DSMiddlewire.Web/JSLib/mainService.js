﻿app.service('dataService', function () {
    var getData = function () {
        /*
        数据结构中的state代表当前路是否是工作状态
        */
        var controllerData =
            {
                title: '中国联通对接设备',
                controller: [{
                    title: '风机',
                    functionlist: [{ functionname: '停止', functionicon: 'glyphicon-off', functionorder: '0', state: true }, { functionname: '正转', functionicon: 'glyphicon-chevron-up', functionorder: '1', state: false }, { functionname: '反转', functionicon: 'glyphicon-chevron-down', functionorder: '2', state: false }]
                }, {
                    title: '湿帘',
                    functionlist: [{ functionname: '停止', functionicon: 'glyphicon-off', functionorder: '0', state: false }, { functionname: '正转', functionicon: 'glyphicon-chevron-up', functionorder: '1', state: false }, { functionname: '反转', functionicon: 'glyphicon-chevron-down', functionorder: '2', state: true }]
                }, {
                    title: '暖灯',
                    functionlist: [{ functionname: '停止', functionicon: 'glyphicon-off', functionorder: '0', state: false }, { functionname: '高光', functionicon: 'glyphicon-chevron-up', functionorder: '1', state: true }, { functionname: '低光', functionicon: 'glyphicon-chevron-down', functionorder: '2', state: false }]
                }],
                led: [{
                    title: '电源',
                    functionlist: [{ functionname: '灭', functionicon: 'glyphicon-eye-close', functionorder: '0', state: false }, { functionname: '亮', functionicon: 'glyphicon-eye-open', functionorder: '1', state: true }]
                }, {
                    title: '转轴',
                    functionlist: [{ functionname: '正转', functionicon: 'glyphicon-eye-close', functionorder: '0', state: true }, { functionname: '反转', functionicon: 'glyphicon-eye-open', functionorder: '1', state: false }]
                }, {
                    title: '浇灌',
                    functionlist: [{ functionname: '关闭', functionicon: 'glyphicon-eye-close', functionorder: '0', state: true }, { functionname: '打开', functionicon: 'glyphicon-eye-open', functionorder: '1', state: false }]
                }, {
                    title: '电压',
                    functionlist: [{ functionname: '正常', functionicon: 'glyphicon-eye-close', functionorder: '0', state: true }, { functionname: '异常', functionicon: 'glyphicon-eye-open', functionorder: '1', state: false }]
                }]
            };

        return controllerData;
    }

    return {
        controllerData: getData,
    };
});

app.service('hubService', ['dataService', function (dataService) {

    //添加对自动生成的Hub的引用
    var chat = $.connection.chatHub;

    //启动链接
    $.connection.hub.start().done(function () { });

    var getData = function () {
        return dataService.controllerData();
    }

    return {
        commandReceived: function(callback)
        {
            if(callback)
            {
                //调用Hub的callback回调方法
                chat.client.printCommand = function (command) {
                    var data = getData();
                    var obj = { data: data, command: command };
                    return callback(obj);
                }
            }
        },
        controllerData:getData
    };
}]);