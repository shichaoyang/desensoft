﻿var mainController = app.controller('dsController', ['$scope', 'hubService', function ($scope, hubService) {

    var currentData = hubService.controllerData();
    
    $scope.controllerData = currentData;

    hubService.commandReceived(function (result) {
        debugger;

        var command = result.command;
        var data = result.data;

        if (command != undefined) {
            if (command.localeCompare("aaaa")==0)
                data.controller[1].title = "哈哈哈";
            else if (command.localeCompare("bbbb") == 0)
                data.controller[0].title = "呵呵呵";
        }
      
        $scope.controllerData = data;

        $scope.$digest();
    });

}]);