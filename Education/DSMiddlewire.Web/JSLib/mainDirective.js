﻿//主框架
app.directive('mainPart',['$compile',function ($compile) {
    return {
        restrict: 'AE',
        replace: true,
        scope: { options: "=" },
        link: function(scope, element, attrs) {
            scope.$watchCollection('options', function (newValue, oldValue) {
                    console.log("I see a data change!");
            });
        },
        template: '<div class="panel panel-info" style="margin:10px 10px 10px 10px">'
                + '  <div class="panel-heading">'
                + '    <h3 class="panel-title glyphicon glyphicon-user"><strong>{{options.title}}</strong></h3>'
                + '  </div>'
                + '  <div class="panel-body">'
                + '    <controller-part options="options.controller"></controller-part>'
                + '    <led-part options="options.led"></led-part>'
                + '  </div>'
                + '</div>'
    };
}]);
//可控制的控制器部分
app.directive('controllerPart', function () {
    return {
        restrict: 'AE',
        replace: true,
        scope: { options: "=options" },
        template: '<div class="panel panel-default">'
                + '  <div class="panel-heading">'
                + '    <h3 class="panel-title">控制器部分</h3>'
                + '  </div>'
                + '  <div class="panel-body">'
                + '    <controller-instance options="options"></controller-instance>'
                + '  </div>'
                + '</div>'
    };
});
//不可控制的指示灯部分
app.directive('ledPart', function () {
    return {
        restrict: 'AE',
        replace: true,
        scope: { options: "=options" },
        template: '<div class="panel panel-default">'
                + '  <div class="panel-heading">'
                + '    <h3 class="panel-title">指示灯部分</h3>'
                + '  </div>'
                + '  <div class="panel-body">'
                + '    <led-instance options="options"></led-instance>'
                + '  </div>'
                + '</div>'
    };
});
//控制器具体的路数
app.directive('controllerInstance', function () {
    return {
        restrict: 'AE',
        replace: true,
        scope: { options: "=options" },
        template: '<div class="panel panel-success" style="float:left;margin-left:20px;" ng-repeat="controller in options">'
                 + '   <div class="panel-heading">'
                 + '       <h3 class="panel-title">{{controller.title}}(在线)</h3>'
                 + '   </div>'
                 + '   <div class="panel-body">'
                 + '       <div class="btn-group" role="group">'
                 + '           <button type="button"  ng-repeat="function in controller.functionlist" tag="{{function.functionorder}}" class="btn {{function.state|onlineConverter}} glyphicon {{function.functionicon}}">{{function.functionname}}</button>'
                 //+ '           <button type="button" class="btn btn-default glyphicon glyphicon-off">停止</button>'
                 //+ '           <button type="button" class="btn btn-default glyphicon glyphicon-chevron-up btn-success">正转</button>'
                 //+ '           <button type="button" class="btn btn-default glyphicon glyphicon-chevron-down">反转</button>'
                 + '       </div>'
                 + '   </div>'
                 + '</div>'
    };
});
//指示器具体的路数
app.directive('ledInstance', function () {
    return {
        restrict: 'AE',
        replace: true,
        scope: { options: "=options" },
        template: '<div class="panel panel-success" style="float:left;margin-left:20px;" ng-repeat="led in options">'
                + '  <div class="panel-heading">'
                + '     <h3 class="panel-title">{{led.title}}(在线)</h3>'
                + '  </div>'
                + '  <div class="panel-body">'
                + '     <div class="btn-group" role="group">'
                + '         <button type="button"  ng-repeat="function in led.functionlist" tag="{{function.functionorder}}" class="btn {{function.state|onlineConverter}} glyphicon {{function.functionicon}}">{{function.functionname}}</button>'
                //+ '         <button type="button" class="btn btn-success glyphicon glyphicon-eye-close">灭</button>'
                //+ '         <button type="button" class="btn btn-default glyphicon glyphicon-eye-open">亮</button>'
                + '     </div>'
                + ' </div>'
                + '</div>'
    };
});

//此过滤器主要是为了过滤工作状态的，将true和false转变为具体的css样式。
app.filter('onlineConverter', function () {
    return function (input) {
        if (input) {
            return "btn-success";
        }
        else {
            return "btn-default";
        }
    }
});