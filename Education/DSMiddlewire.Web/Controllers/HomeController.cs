﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ChatRoom.Controllers
{
    public class HomeController : Controller
    {
        //首页
        public ActionResult Index()
        {
            return View();
        }

        //设备控制响应
        public ActionResult Controller()
        {

            return View();
        }

        //设备控制界面
        public ActionResult ControllerUI()
        {
            return View();
        }

        //设备控制示例
        public ActionResult ControllerDemo()
        {
            return View();
        }
    }
}
