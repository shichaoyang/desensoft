﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using System.Threading.Tasks;
using ChatRoom.Models;
using System.Net.Sockets;
using System.Net;
using System.Threading;

namespace ChatRoom.Hubs
{
    public class ChatHub:Hub, IChatHub
    {
        private IList<UserChat> userList = ChatUserCache.userList;

        //这里需要根据真实情况改进。
        //假如用户有三台设备，那么这里其实应该包含三条数据的。
        private IDictionary<string, TcpClient> clientList = ChatUserCache.clientList;

        #region Hub Events
        public override Task OnConnected()
        {
            var sessionID = Context.ConnectionId;
            var tcpClient = new TcpClient();

            var endPoint = new IPEndPoint(IPAddress.Parse("219.235.3.215"), 60000);
            try
            {
                if (tcpClient != null)
                {
                    if (!tcpClient.Connected)
                    {
                        tcpClient.Connect(endPoint);

                        SendHeartBeat(tcpClient);

                        if (!clientList.ContainsKey(sessionID))
                        {
                            clientList.Add(sessionID, tcpClient);
                        }

                        while (true)
                        {
                            ReceiveServerCommands(tcpClient);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //throw new Exception(ex.Message);
                Clients.All.printCommand(ex.Message);
            }

            return base.OnConnected();
        }

        public override Task OnDisconnected()
        {
            var disconnectedSessionID = Context.ConnectionId;
            TcpClient disconnectedClient;
            if(clientList.TryGetValue(disconnectedSessionID,out disconnectedClient))
            {
                SendClientRequests(disconnectedClient,"client*** disconnected!");

                Thread.Sleep(3000);
                disconnectedClient.GetStream().Close();
                disconnectedClient.Client.Disconnect(true);
                disconnectedClient.Client.Dispose();
                disconnectedClient.Close();
                clientList.Remove(disconnectedSessionID);
            }
            return base.OnDisconnected();
        }

        public override Task OnReconnected()
        {
            return base.OnReconnected();
        }
        #endregion 

        //发送心跳包给服务器端
        public void SendHeartBeat(TcpClient tcpClient)
        {
            //var heartBeatTimer = new System.Timers.Timer(5000);
            //heartBeatTimer.Elapsed += (sender, args) =>
            //{
            //    SendClientRequests(tcpClient,"Heart Beat from Client!");
            //};
            //heartBeatTimer.Start();
        }

        //发送客户端请求
        public void SendClientRequests(TcpClient tcpClient,string message)
        {
            NetworkStream ns = tcpClient.GetStream();
            if (ns.CanWrite)
            {
                var sendBytes = System.Text.Encoding.ASCII.GetBytes(message);
                ns.Write(sendBytes, 0, sendBytes.Length);
            }
            else
            {
                ns.Close();
            }
        }

        //接收服务器端信息并处理
        public void ReceiveServerCommands(TcpClient tcpClient)
        {
            NetworkStream ns = tcpClient.GetStream();
            if(ns.CanRead)
            {
                var receiveBytes = new byte[tcpClient.ReceiveBufferSize];
                ns.Read(receiveBytes, 0, (int)tcpClient.ReceiveBufferSize);
                string command = System.Text.Encoding.ASCII.GetString(receiveBytes);
                Clients.All.printCommand(command);
            }
            else
            {
                ns.Close();
            }
        }

        #region 这些是参考部分

        public void SendChat(string id, string name, string message)
        {
            Clients.All.addNewMessageToPage(id, name + " " + DateTime.Now.ToString("yyyy/MM/dd hh:mm:ss"), message);
        }

        public void TriggerHeartbeat(string id, string name)
        {
            var userInfo = userList.Where(x => x.ID.Equals(id) && x.Name.Equals(name)).FirstOrDefault();
            userInfo.count = 0;  //收到心跳，重置计数器
        }

        public void SendLogin(string id,string name)
        {
            var userInfo = new UserChat() { ID = id, Name = name };
            userInfo.action += () =>
            {
                //用户20s无心跳包应答，则视为掉线，会抛出事件，这里会接住，然后处理用户掉线动作。
                SendLogoff(id, name);
            };

            var comparison = new ChatUserCompare();
            if (!userList.Contains<UserChat>(userInfo, comparison))
                userList.Add(userInfo);
            Clients.All.loginUser(userList);
            SendChat(id, name, "<====用户 " + name + " 加入了讨论组====>");
        }

        public void SendLogoff(string id,string name)
        {
            var userInfo = userList.Where(x => x.ID.Equals(id) && x.Name.Equals(name)).FirstOrDefault();
            if (userInfo != null)
            {
                if (userList.Remove(userInfo))
                {
                    Clients.All.logoffUser(userList);
                    SendChat(id, name, "<====用户 " + name + " 退出了讨论组====>");
                }
            }
        }

        #endregion
    }
}