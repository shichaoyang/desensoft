﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using DSMiddlewire.Core;
using DSMiddlewire.Lib;
using System.Net;
using FuNong.Framework.Logger;
using FuNong.Framework.Caching;
using System.Linq;
using FuNong.DataAccess;
using FuNong.Service;
using FuNong.UserInterface.Models;

/*名称：DSServer
 *描述：只是一个注册了接受和发送数据的服务端，不负责处理具体的业务 
 */

namespace DSMiddlewire.Server
{
    public class DSServer
    {
        public DSServer(ICacheManager cacheManager
                      , ILoggerService loggerService
                      , IFuNongContext context
                      , IUnitOfWork unitOfWork
                      , IBaseService<ds_collector_data> dataService
                      , ConsoleLogWithColor logConsole
                      , IRepository<edu_school> companyRepo
                      , IRepository<edu_users_view> userRepo
                      , IRepository<edu_base_setting> settingRepo
                      , IRepository<ds_machine_list> machineRepo
                      , IDSCollectorService collectorService
                      , IDSControllerService controllerService
                      , IDeviceService deviceService
            )
        {
            this.cacheManager = cacheManager;
            this.loggerService = loggerService;
            this.context = context;
            this.unitOfWork = unitOfWork;
            this.dataService = dataService;
            this.logConsole = logConsole;
            this.companyRepo = companyRepo;
            this.userRepo = userRepo;
            this.settingRepo = settingRepo;
            this.machineRepo = machineRepo;
            this.collectorService = collectorService;
            this.deviceService = deviceService;
            this.controllerService = controllerService;

            if (clientList == null)
                clientList = new Dictionary<string, DSClientEntity>();

            if (deviceAll == null)
                deviceAll = deviceService.GetAllMachineDetails();

            if (socketServer == null)
            {
                socketServer = new SocketServer(IPAddress.Any, 60000, 1, 1000, Int16.MaxValue);
                socketServer.OnStarted = OnStarted;
                socketServer.OnStopped = OnStopped;
                socketServer.OnConnected = OnConnected;
                socketServer.OnDisconnect = OnDisconnected;
                socketServer.OnReceive = OnReceive;
            }
            socketServer.Start();

            logConsole.WriteLine("--------->初始缓存容器完毕<---------", ConsoleColor.Green);
            logConsole.WriteLine("--------->初始日志容器完毕<---------", ConsoleColor.Green);
            logConsole.WriteLine("--------->初始各种服务完毕<---------", ConsoleColor.Green);
            logConsole.WriteLine("--------->请求硬件信息完毕<---------", ConsoleColor.Green);

           // if (!cacheManager.IsSet("machineList"))
           // {
           //    var deviceAll = deviceService.GetAllMachineDetails();
           //logConsole.WriteLine("--------->请求硬件信息完毕<---------", ConsoleColor.Green);
           //放入缓存中备用,存取时间为5天
           //     cacheManager.Set("MachineList", deviceAll, 60 * 24 * 5);
           //      logConsole.WriteLine("--------->缓存硬件信息完毕<---------", ConsoleColor.Green);
           // }
        }

        private ICacheManager cacheManager;                        //缓存实例
        private ILoggerService loggerService;                      //log4net实例
        private IFuNongContext context;                            //数据库实例
        private IUnitOfWork unitOfWork;                            //单元提交实例
        private IBaseService<ds_collector_data> dataService;       //采集数据访问服务实例
        private IRepository<edu_school> companyRepo;               //公司访问仓储
        private IRepository<edu_users_view> userRepo;              //用户信息仓储
        private IRepository<edu_base_setting> settingRepo;         //配置信息仓储
        private IRepository<ds_machine_list> machineRepo;          //设备信息仓储
        private IDSCollectorService collectorService;              //采集器
        private IDSControllerService controllerService;            //控制器
        private IDeviceService deviceService;                      //设备访问器
        private ConsoleLogWithColor logConsole;                    //console和日志记录器

        private SocketServer socketServer;                         //服务器实例
        private static Dictionary<string, DSClientEntity> clientList;  //客户端列表
        private static List<controller> deviceAll;

        private readonly object lockObject = new object();
        private readonly object syncRoot = new object();

        #region 服务端注册事件：服务端启动，停止，客户端连接，接收数据
        //服务端启动
        private void OnStarted(Socket socket)
        {
            logConsole.WriteLine("=========>中间件服务器启动<=========", ConsoleColor.Green);
            logConsole.WriteLine(string.Empty, ConsoleColor.Green);
        }

        //服务端停止
        private void OnStopped()
        {
            logConsole.WriteLine("=========>中间件服务器停止<=========", ConsoleColor.Green);
        }

        //客户端连接（参数socket代表连接上来的客户端套接字）
        private void OnConnected(Socket socket)
        {
            logConsole.WriteLine("[" + socket.RemoteEndPoint + "] 已连接到服务器！", ConsoleColor.DarkYellow);

            lock (lockObject)
            {
                //新客户端连接，保存到客户端连接列表中
                IncludeOnlineClientAction(socket, null);
            }
        }

        //客户端断开
        private void OnDisconnected(SocketAsyncEventArgs e)
        {
            lock (lockObject)
            {
                //老客户端断开，从客户端连接列表中移除
                RemoveOfflineClientAction(e.AcceptSocket);
                
            }
        }

        //服务端收到数据
        private void OnReceive(SocketAsyncEventArgs e)
        {
            //获取整个数组
            var messageLength = e.BytesTransferred;
            var messageAllBytes = new byte[messageLength];
            Array.Copy(e.Buffer, e.Offset, messageAllBytes, 0, messageLength);

            lock (syncRoot)
            {
                HandleCenter(messageAllBytes, e.AcceptSocket);
            }
        }
        #endregion

        //中间件接收数据处理核心
        private void HandleCenter(byte[] messageAllBytes,Socket acceptSocket)
        {
            var messageReceivedAll = System.Text.Encoding.Default.GetString(messageAllBytes);

            //如果消息为空或者不符合要求，抛弃不处理
            if (string.IsNullOrEmpty(messageReceivedAll)) return;
            if (!messageReceivedAll.Contains("|")) return;

            //获取传输的数据信息(不包含CRC)
            string messageReceived;

            var flag = CrcChcek.CheckMessageCRC(messageAllBytes, out messageReceived);

            if (flag)
            {
                logConsole.WriteLine("[" + acceptSocket.RemoteEndPoint + "]:" + messageReceived, ConsoleColor.Yellow);
                //拆包分发：如果是采集包，分发给采集管道；如果是控制包，分发给控制管道；如果是心跳包，分发给心跳控制部分
                //封包格式：dst|数据来源(手机，PC，硬件等)| 功能码|硬件id|数据内容|延时时间|crc
                var messageSlice = messageReceived.Split('|');
                var messageEntity = new DSMessageEntity();
                messageEntity.MessageHeader = messageSlice[0];
                messageEntity.MessageSource = (CommandSource)Enum.Parse(typeof(CommandSource), messageSlice[1]);
                messageEntity.FunctionCode = (MessageType)Enum.Parse(typeof(MessageType), messageSlice[2]);
                messageEntity.MachineID = messageSlice[3];
                messageEntity.MessageContent = messageSlice[4];
                messageEntity.MessageContentAppendix = messageSlice[5];

                //不识别的封包，抛弃；带有dst头的包才会进行处理
                if (messageEntity.MessageHeader == "dst")
                {
                    string key = acceptSocket.RemoteEndPoint.ToString();
                    //用户不存在，则重新添加到列表
                    IncludeOnlineClientAction(acceptSocket,messageEntity);

                    //如果源发送机器不在列表中，则抛弃此包
                    if (!clientList.ContainsKey(key))
                        return;

                    switch (messageEntity.FunctionCode)
                    {
                        case MessageType.CollectorBroadcast:
                            //处理采集数据
                            collectorService.ProcessCommandFromCollector(messageEntity);
                            break;
                        case MessageType.HeartBeat:
                            //用户已存在，则处理硬件或者是用户端传来的心跳包
                            HeartbeatCommand(messageEntity, key);
                            break;
                        case MessageType.LedBroadcast:
                            //上传硬件的指示灯状态命令
                            UploadCommand(messageEntity);
                            break;
                        case MessageType.RequestLed:
                            DownloadCommand(messageEntity);
                            break;
                        case MessageType.SwitchBroadcast:
                            //上传硬件的开关量状态命令
                            UploadCommand(messageEntity);
                            break;
                        case MessageType.RequestSwitch:
                            DownloadCommand(messageEntity);
                            break;
                        case MessageType.SwitchTransfer:
                            //下发用户的控制命令
                            DownloadCommand(messageEntity);
                            break;
                        case MessageType.BulletSend:
                            //这里只有BulletSend
                            //中间件不会处理BulletReceive，因为BulletReceive是目标硬件处理的
                            MakeBulletCommand(messageEntity);
                            break;
                        default: break;
                    }
                }
            }
        }

        #region 控制命令处理
        //透传命令处理
        private void MakeBulletCommand(DSMessageEntity messageEntity)
        {
            if (messageEntity.MessageSource == CommandSource.HardWare)
            {
                //如果没有附加任何目标设备信息，则直接退出，不进行处理
                if (string.IsNullOrEmpty(messageEntity.MessageContent))
                    return;
                //目标设备列表
                var targetList = messageEntity.MessageContent.Split('*').Where(x => x != "" && x != null).ToList();
                targetList.ForEach(target =>
                { 
                    //寻找关联的硬件端socket
                    var harwareList = clientList.Where(x => x.Value.MachineID == target && x.Value.Source == CommandSource.HardWare)
                                            .Select(x => x.Value)
                                            .ToList();
                    if (harwareList != null)
                    {
                        if (harwareList.Count > 0)
                        {
                            harwareList.ForEach(entity =>
                            {
                                var messageEntityX = (DSMessageEntity)messageEntity.Clone();
                                messageEntityX.FunctionCode = MessageType.BulletReceive;
                                messageEntityX.MessageContent = messageEntity.MessageContentAppendix;
                                messageEntityX.MessageContentAppendix = "0000";
                                //处理来自于硬件端的透传命令
                                controllerService.ProcessBullteFromHardware(messageEntityX, entity.ClientSocket);
                            });
                        }
                    }
                });
            }
        }

        //上传控制命令
        private void UploadCommand(DSMessageEntity messageEntity)
        {
            if (messageEntity.MessageSource == CommandSource.HardWare)
            {
                //寻找关联的Web端socket
                var webList = clientList.Where(x => x.Value.MachineID == messageEntity.MachineID && x.Value.Source == CommandSource.UserInterface)
                                        .Select(x => x.Value)
                                        .ToList();

                foreach(var kvp in clientList)
                {
                    var k = kvp.Key;
                    var v = kvp.Value;
                    logConsole.WriteLine("test:" + k + "-" + v.MachineID + "-" + v.ClientSocket.RemoteEndPoint.ToString(), ConsoleColor.Cyan);
                }

                    if (webList != null)
                    {
                        if (webList.Count > 0)
                        {
                            webList.ForEach(entity =>
                            {
                                //处理来自于硬件终端的上报命令
                                controllerService.ProcessCommandFromHardware(messageEntity, entity.ClientSocket);
                            });
                        }
                        else
                        {
                            logConsole.WriteLine("未找到待上传用户端连接,放弃上传控制状态!", ConsoleColor.DarkRed);
                        }
                    }
                    else
                    {
                        logConsole.WriteLine("未找到待上传用户端连接,放弃上传控制状态!", ConsoleColor.DarkRed);
                    }
            }
        }

        //下发用户命令
        private void DownloadCommand(DSMessageEntity messageEntity)
        {
            if (messageEntity.MessageSource == CommandSource.UserInterface)
            {
                //寻找关联的硬件socket
                var hardwareList = clientList.Where(x => x.Value.MachineID == messageEntity.MachineID && x.Value.Source == CommandSource.HardWare)
                                            .Select(x => x.Value)
                                            .ToList();
                if (hardwareList != null)
                {
                    if (hardwareList.Count > 0)
                    {
                        hardwareList.ForEach(entity =>
                        {
                            //处理来自于用户发送的控制命令
                            controllerService.ProcessCommandFromWeb(messageEntity, entity.ClientSocket);
                        });
                    }
                }
            }
        }

        //处理硬件或者是用户端传来的心跳包
        private void HeartbeatCommand(DSMessageEntity messageEntity,string key)
        {
            //重置计数器，在这里需注意，不需要处理else条件，因为这样可以减少额外的信息干扰，保证服务器的安全性
            if (clientList.ContainsKey(key))
            {
                var deviceEntity = clientList[key];
                //已经存在的连接，重置计数器
                deviceEntity.HeartBeatCount = 0;

                //deviceEntity.Source = messageEntity.MessageSource;
                //deviceEntity.MachineID = messageEntity.MachineID;
            }
        }
        #endregion

        #region 控制动作处理
        //移除掉线用户动作
        private void RemoveOfflineClientAction(Socket clientSocket)
        {

            string key = clientSocket.RemoteEndPoint.ToString();
            //设备掉线
            if (clientList.ContainsKey(key))
            {
                var deviceEntity = clientList[key];
                logConsole.WriteLine("[" + clientSocket.RemoteEndPoint + "] 断开与服务器的连接！", ConsoleColor.Red);
                //移除对象
                clientList.Remove(key);
                ////关闭连接
                //clientSocket.Shutdown(SocketShutdown.Both);
                //clientSocket.Close();
                
                //logConsole.WriteLine("[" + clientSocket.RemoteEndPoint + "]:断开连接！", ConsoleColor.Red);
                //TODO
                //当硬件客户端掉线，需要向用户端发送设备掉线通知,硬件掉线，这是必须发送的内容
                //当用户端掉线，不予操作，通过source判断
            }
        }

        //添加用户动作
        private void IncludeOnlineClientAction(Socket clientSocket,DSMessageEntity messageEntity)
        {
            //新客户端连接，保存到客户端连接列表中
            string key = clientSocket.RemoteEndPoint.ToString();
            if (!clientList.ContainsKey(key))
            {
                var client = new DSClientEntity();
                //client.ClientEndPoint = clientSocket.RemoteEndPoint;
                client.ClientSocket = clientSocket;
                if (messageEntity != null)
                {
                    client.MachineID = messageEntity.MachineID;
                    client.Source = messageEntity.MessageSource;
                }
                //注册心跳检测事件，当客户端连续三次没发送心跳包，视为设备掉线
                client.heartBeatAction += () =>
                {
                    //RemoveOfflineClientAction(clientSocket);
                };
                clientList.Add(key, client);
            }
            else
            {
                var existEntity = clientList[key];
                if (messageEntity != null)
                {
                    //这里有点小问题，原因如下：
                    //发现当用户操控的时候，所有的用户请求最后都会入队列，也就是说，假如用户A连接上，套接字是***：2000，B连接上，套接字是***：3000
                    //C连接上，套接字是***：4000，最后发现进行操控的时候，所有的接收端都会变成ABC中的一种，比如说***：4000.
                    //这样看来，就很明白了，无论多少个用户上来，都是先进入队列中，然后分配给这些用户一个套接字，然后由这一个套接字来进行剩余的操作。
                    //这么看来，确实是能提高系统性能的。
                    //所以我这里要做的是：把初始连接上来的用户链接给保存好，然后需要向他们发送的时候，再把连接复原出来发送即可。

                    //下面这段代码的问题在于：
                    //每当有老用户上来发送命令什么的，都需要将machine重置。也就是说假如ABCD在一起操控，那么Ａ，Ｂ，Ｃ，Ｄ都会同时只用一个套接字来进行收数据的操作。
                    //也就是说，A点击完，machineid是A的，B点击的时候，machineid是B的。总之，这就出现了资源竞争的情况，当很多人同时点击的时候，就会出现并发问题，导致
                    //界面卡顿等情况

                    //解决这个问题的关键在于clientList列表中，如果找到ABCD的原始connect上来的连接。然后向这些原始链接发数据即可
                    if (string.IsNullOrEmpty(existEntity.MachineID))
                        existEntity.MachineID = messageEntity.MachineID;
                    if (existEntity.Source == CommandSource.UnKnown)
                        existEntity.Source = messageEntity.MessageSource;
                }
            }
        }
        #endregion 
    }
}
