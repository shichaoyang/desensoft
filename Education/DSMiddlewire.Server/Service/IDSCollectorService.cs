﻿using System;

namespace DSMiddlewire.Server
{
    public interface IDSCollectorService
    {
        void ProcessCommandFromCollector(DSMessageEntity message);
    }
}
