﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSMiddlewire.Server
{
    public enum MessageType
    {
        //心跳包
        HeartBeat = 006,

        //开关主动上报
        SwitchBroadcast = 007,

        //指示灯主动上报
        LedBroadcast = 008,

        //请求开关状态
        RequestSwitch = 009,

        //请求指示灯状态
        RequestLed = 010,

        //接收到软件端传来的可控制命令
        SwitchTransfer = 011,

        //采集数据上报
        CollectorBroadcast = 012,

        //透传发送(一台硬件发送给中间件)
        BulletSend = 013,

        //透传接收(其他硬件接收中间件发送给的信息)
        BulletReceive =  014

    }

    public enum CommandSource
    {
        UnKnown = -1,
        //从用户端发出的请求，比如web端或者手机端
        UserInterface = 0,
        //从硬件端发出的请求
        HardWare = 1,
        //从中间件发出的请求
        MiddleWare = 2
    }
}
