﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace DSMiddlewire.Server
{
    public class DSClientEntity
    {
        public DSClientEntity()
        {
            Source = CommandSource.UnKnown;

            HeartBeatCount = 0;

            if (heartBeatTimer == null)
            {
                heartBeatTimer = new Timer();
            }
            //心跳包，90秒硬件端或者软件端会上传一次
            heartBeatTimer.Interval = 1000;
            heartBeatTimer.Enabled = true;
            heartBeatTimer.Start();
            heartBeatTimer.Elapsed += (sender, args) =>
            {
                HeartBeatCount++;
                if (HeartBeatCount >= 330)
                {
                    //关闭计时器
                    heartBeatTimer.Stop();
                    heartBeatTimer.Dispose();
                    //抛出用户掉线的通知
                    if (heartBeatAction != null)
                        heartBeatAction();
                }
            };
        }

        private Timer heartBeatTimer;

        public event Action heartBeatAction;

        //客户端的EndPoint
        public EndPoint ClientEndPoint { get; set; }
        //客户端的套接字
        public Socket ClientSocket { get; set; }

        //内部计数器（每次递增1），如果服务端每90s能收到客户端的心跳包，那么count会在外部被重置为0；
        //如果服务端270s后仍未收到客户端心跳包，那么视为掉线
        public int HeartBeatCount { get; set; }

        //设备编号
        public string MachineID{ get; set; }

        public CommandSource Source { get; set; }
    }
}
