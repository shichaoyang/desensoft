﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FuNong.DataAccess;

namespace FuNong.DataContract
{
    public class RoleRequest : Request<edu_role_detail>
    {
        public int roleid { get; set; }
    }
}
