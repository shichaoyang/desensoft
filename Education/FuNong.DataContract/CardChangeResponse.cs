﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FuNong.DataContract
{
    public class CardChangeResponse : Response
    {
        public int id { get; set; }
        public string oldcardnum { get; set; }
        public string newcardnum { get; set; }
        public int? recordorder { get; set; }
        public DateTime? updatetime { get; set; }

        public int totalcount { get; set; }

        //学校
        public int schoolid { get; set; }
        public string schoolname { get; set; }

        //卡类型
        public int? cardtypeid { get; set; }
        public string cardtypename { get; set; }

        //操作类型
        public int? operationtypeid { get; set; }
        public string operationtypename { get; set; }

    }
}
