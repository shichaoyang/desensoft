﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FuNong.DataContract
{
    public class ControllerResponse
    {
        public  int id { get; set; }

        public  string machine_id { get; set; }

        public string machine_name { get; set; }

        public int machine_type { get; set; }
        //设备类别名称
        public string type_name { get; set; }

        public int company_id { get; set; }

        public string company_name { get; set; }

        public  int route_id { get; set; }

        public  string route_name { get; set; }

        public  string route_multiple { get; set; }

        public  string route_delay { get; set; }

        public  string route_note { get; set; }

        public  Nullable<int> recordorder { get; set; }

        public  Nullable<System.DateTime> updatetime { get; set; }

        public int totalcount { get; set; }
    }
}
