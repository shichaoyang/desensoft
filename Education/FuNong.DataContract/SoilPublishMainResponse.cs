﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FuNong.DataContract
{
    public class SoilPublishMainResponse : Response
    {
        public int id { get; set; }
        public int requestid { get; set; }
        public string publishtitle { get; set; }
        public int soiltypeid { get; set; }
        public int changetypeid { get; set; }
        public int districtid { get; set; }
        public string locationdetail { get; set; }
        public int sparechangeyear { get; set; }
        public int canchangeyear { get; set; }
        public float soilarea { get; set; }
        public string description { get; set; }
        public string publisher { get; set; }
        public string publisherphone { get; set; }
        public int isapprove { get; set; }
        public int? recordorder { get; set; }
        public DateTime? updatetime { get; set; }

        public string soiltypename { get; set; }
        public string changetypename { get; set; }
        
        public string districtname { get; set; }
        public string cityname { get; set; }
        public string provincename { get; set; }

        public string indeximg { get; set; }
        public string chargefee { get; set; }

        public string soilgroundlist { get; set; }
        public string earthtypelist { get; set; }
        public string waterlist { get; set; }
        public string aroundlist { get; set; }

        public int ischarge { get; set; }  //是否已经缴费

        public int isjsjg { get; set; }  //是否结算交割

        public int totalcount { get; set; }
    }
}
