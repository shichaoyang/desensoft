﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FuNong.DataAccess;

namespace FuNong.DataContract
{
    public class CourseTableRequest:Request<edu_course_table>
    {
        public int schoolid { get; set; }  //当前学校id

        public int classid { get; set; }   //班级id

        public int roleid { get; set; }  //教师角色
    }
}
