﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FuNong.DataContract
{
    public class ProcessResponse : Response
    {
        public int id { get; set; }

        public int clientid { get; set; }
        public string servergroup { get; set; }
        
        public string requestcontent { get; set; }

        public string requesttemplate { get; set; }

        public string soilcertificateimages { get; set; }
        
        public string idcardimages { get; set; }
        
        public int isapprove { get; set; }
        public int? recordorder { get; set; }
        public DateTime? updatetime { get; set; }

        public string clientname { get; set; }
        public string clientidcard { get; set; }
        public string clientphone { get; set; }
        public string clientaddress { get; set; }

        public string servername { get; set; }
        public string serveridcard { get; set; }
        public string serverphone { get; set; }
        public string serverlocation { get; set; }

        public int totalcount { get; set; }
    }
}
