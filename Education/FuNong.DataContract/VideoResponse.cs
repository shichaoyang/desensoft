﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FuNong.DataContract
{
    public class VideoResponse : Response
    {
        public int id { get; set; }
        public string videoname { get; set; }
        public string videoDNNS { get; set; }    //可不用填写
        public string videoIP { get; set; }
        public string videoTCPPort { get; set; }
        public string videoHTTPPort { get; set; }  //可不用填写
        public string videoRSTPPort { get; set; }  //可不用填写
        public string videoHTTPSPort { get; set; } //可不用填写
        public string videoLoginName { get; set; }
        public string videoLoginPass { get; set; }
        public string videoViewChannel { get; set; }
        public string videoNVRAddress { get; set; }
        public string videoNVRUser { get; set; }
        public string videoNVRPass { get; set; }
        public string videoNVRMemo { get; set; }  //可不用填写
        public string videoNote { get; set; }//可不用填写
        public string videoNote1 { get; set; }//可不用填写
        public int? recordorder { get; set; }
        public DateTime? updatetime { get; set; }

        public string videocompany { get; set; }
        public string videocompanyshow { get; set; }

        public int schoolid { get; set; }
        public string companyname { get; set; }

        public string jin { get; set; }  //经度
        public string wei { get; set; }  //维度

        public int totalcount { get; set; }
    }
}
