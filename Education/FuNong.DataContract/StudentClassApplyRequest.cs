﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FuNong.DataAccess;

namespace FuNong.DataContract
{
    public class StudentClassApplyRequest:Request<edu_student_class_apply>
    {
        public int schoolid { get; set; }
    }
}
