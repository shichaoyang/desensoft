﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FuNong.DataContract
{
    public class DealResponse : Response
    {
        public int id { get; set; }
        public int requestid { get; set; }
        public int publishmainid { get; set; }
        public int changetypeid { get; set; }
        public string signcontent { get; set; }
        public DateTime signtime { get; set; }
        public decimal chargefee { get; set; }
        public int isapprove { get; set; }
        public int? recordorder { get; set; }
        public DateTime? updatetime { get; set; }

        public string publishtitle { get; set; }
        public string clientname { get; set; }

        public int totalcount { get; set; }
    }
}
