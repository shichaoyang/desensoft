﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FuNong.DataContract
{
    public class NewsResponse : Response
    {
        public int id { get; set; }
        public string typeid { get; set; }
        public string newstitle { get; set; }
        public string newscontent { get; set; }
        public int todaytop { get; set; }
        public int todayrecommand { get; set; }
        public int todayscroll { get; set; }

        public int isapprove { get; set; }
        public int? recordorder { get; set; }
        public DateTime? updatetime { get; set; }
        public string newsnote { get; set; }

        public int? language { get; set; }

        public int totalcount { get; set; }
        public string typename { get; set; }

        public int uid { get; set; }     //当前是哪个用户发表的文章
        public string uname { get; set; }  //小记者姓名
        public string schoolname { get; set; }  //小记者所属学校

        public string pageimg { get; set; }  //新闻封面图片
    }
}
