﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using FuNong.DataAccess;

namespace FuNong.DataContract
{
    public class DepartmentRequest:Request<edu_department_detail>
    {
        public int departmentid { get; set; }
    }
}
