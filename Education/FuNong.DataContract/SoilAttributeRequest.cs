﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FuNong.DataAccess;

namespace FuNong.DataContract
{
    public class SoilAttributeRequest:Request<funong_soil_type>
    {
        //土壤类型
        public int soiltypeid { get; set; }

        //流转类型
        public int changetypeid { get; set; }

        //省级ID
        public int provinceid { get; set; }

        //市级ID
        public int cityid { get; set; }

        //地上物标记
        public string soilaroundflag { get; set; }
    }
}
