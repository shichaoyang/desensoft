﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FuNong.DataContract
{
    public class IconResponse : Response
    {
        public int id { get; set; }
        public string iconname { get; set; }
        public string iconcontent { get; set; }
        public string iconnote { get; set; }
        public int? recordorder { get; set; }
        public DateTime? updatetime { get; set; }

        public int totalcount { get; set; }
    }
}
