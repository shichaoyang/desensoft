﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using FuNong.DataAccess;

namespace FuNong.DataContract
{
    public class UserResponse : Response
    {
        public int uid { get; set; }               //用户编号,bma_users表和edu_user_detail表的共同主键

        public string idcard { get; set; }   //身份证号码
        public string useraddress { get; set; }  //用户住址
        public int? departmentid { get; set; }  //所属部门
        public string usernote { get; set; }   //用户备注
        public int? recordorder { get; set; }  //排序
        public DateTime? updatetime { get; set; }  //更新时间
        public int ischeck { get; set; }   //用户是否被审核
        public int totalcount { get; set; }  //记录总条数

        public string salt { get; set; }  //加验码

        public string departmentname { get; set; }   //部门名称

        public int requestid { get; set; }

        public int publishmainid { get; set; }

        public int roleid { get; set; }        //用户角色
        public string rolename { get; set; }   //用户角色名称
        public int? ismultischool { get; set; }  //是否允许多校

        //对于学生来说，关联的是家长，其他角色无效
        public int? refuid { get; set; }    
        public string refusername { get; set; }
        public string refuserrealname { get; set; }

        //对于教师来说，关联的是班主任，其他角色无效
        public int? classid { get; set; }   
        public string classname { get; set; }
        public int gradeid { get; set; }
        public string gradename { get; set; }

        public int schoolid { get; set; }    //学校id 
        public string schoolname { get; set; }  //学校名称

        public string smsnumber { get; set; }   //短信接收号码
        public string personcode { get; set; }   //教师工号
        public string checkincard { get; set; }  //考勤卡号码
        public int? genderid { get; set; }   //性别
        public int personstateid { get; set; }  //人员状态
        public string qq { get; set; }             //qq号码
        public string host { get; set; }       //个人主页
        public DateTime? createtime { get; set; }   //创建时间

        //以下信息均来自于bma_users表
      
        public string username { get; set; }   //用户名
        public string userpass { get; set; }  //密码
        public string usermail { get; set; }   //邮箱
        public string userphone { get; set; }   //联系电话
        public string nickname { get; set; }  //真实姓名

        public string method { get; set; }


    }
}
