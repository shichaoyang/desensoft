﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FuNong.DataContract
{
    public class LogResponse 
    {
        public int id { get; set; }
        public DateTime Date { get; set; }
        public string Thread { get; set; }
        public string Level { get; set; }
        public string Logger { get; set; }
        public string Message { get; set; }
        public string UserID { get; set; }
        public string UserName { get; set; }
        public string SysTypeID { get; set; }

        public int totalcount { get; set; }
    }
}
