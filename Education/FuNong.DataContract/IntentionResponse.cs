﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FuNong.DataContract
{
    public class IntentionResponse:Response
    {
      public int id {get;set;}
      public int? requestid {get;set;}
      public string requestname {get;set;}
      public string requestphone { get; set; }
      public string requestnote { get; set; }
      public int publishmainid {get;set;}
      public int isarrange {get;set;}
      public int issuccess {get;set;}
      public int? recordorder {get;set;}
      public DateTime? updatetime {get;set;}

      public int totalcount { get; set; }
    }
}
