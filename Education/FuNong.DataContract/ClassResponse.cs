﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FuNong.DataContract
{
    public class ClassResponse : Response
    {
        public  int id { get; set; }
        public  int gradeid { get; set; }
        public  int isgraduate { get; set; }
        public  int isupgrade { get; set; }
        public  DateTime? lastupgragetime { get; set; }
        public  int? upgradecount { get; set; }
        public  string classname { get; set; }
        public  string classcode { get; set; }
        public  int stateid { get; set; }
        public  string note { get; set; }
        public  int? recordorder { get; set; }
        public  DateTime? updatetime { get; set; }

        public string gradename { get; set; }
        public string statename { get; set; }
        public string isgraduatename { get; set; }
        public string isupgradename { get; set; }

        public int schoolid { get; set; }
        public string schoolname { get; set; }

        public int totalcount { get; set; }
    }
}
