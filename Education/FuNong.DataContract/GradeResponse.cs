﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FuNong.DataContract
{
    public class GradeResponse:Response
    {
        public int id { get; set; }
        public int schoolid { get; set; }
        public string name { get; set; }
        public int stateid { get; set; }
        public string note { get; set; }
        public int? recordorder { get; set; }
        public DateTime? updatetime { get; set; }

        public string schoolname { get; set; }
        public string statename { get; set; }

        public int totalcount { get; set; }
    }
}
