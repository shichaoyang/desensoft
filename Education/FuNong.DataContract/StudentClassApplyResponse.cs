﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FuNong.DataContract
{
    public class StudentClassApplyResponse : Response
    {
        public int id { get; set; }

        public int uid { get; set; }
        public string username { get; set; }
        public string nickname { get; set; }
        public string personcode { get; set; }

        public int classid { get; set; }
        public string classname { get; set; }

        public int gradeid { get; set; }
        public string gradename { get; set; }
        
        public int isvalid { get; set; }

        public int note { get; set; }
        public int? recordorder { get; set; }
        public DateTime? updatetime { get; set; }

        public string schoolname { get; set; }

        public int totalcount { get; set; }

    }
}
