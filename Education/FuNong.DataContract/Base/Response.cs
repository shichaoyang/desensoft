﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace FuNong.DataContract
{
    [DataContract]
    public abstract class Response
    {
        [DataMember]
        public bool Success { get; set; }  //返回消息是否成功

        [DataMember]
        public string Message { get; set; } //如果返回错误，这里可以附加错误数据
    }
}
