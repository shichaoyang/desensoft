﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FuNong.DataAccess;
using FuNong.DataContract;


namespace FuNong.TestDataAccess
{
    class Program
    {
        static void Main(string[] args)
        {
            var context = new FuNongContext();
            var uow = new UnitOfWork(context);
            //var cache = new MemoryCacheManager();
            //var log = new LoggerService();
            //var query = new DynamicQuery(uow);

            //一定要将 DataAccess中的appconfig拷贝到这个项目下，否则程序会报错，会从你本机寻找数据库。
            //var basesetting = uow.Repository<base_setting>().Get(x => x.id == 1);

            //var dservice = new DepartmentService(uow,cache,log,query);
            //var departmentRequest = new DepartmentRequest();
            //departmentRequest.departmentid = 0;
            //departmentRequest.pager = false;
            //var departmentList = dservice.GetDepartments(departmentRequest);

            Console.ReadKey();
        }
    }
}
