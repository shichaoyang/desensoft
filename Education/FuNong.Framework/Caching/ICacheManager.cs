﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FuNong.Framework.Caching
{
    public interface ICacheManager
    {
        //根据key获取缓存对象
        T Get<T>(string key);

        //设置缓存对象
        void Set(string key, object data, int cacheTime);

        //查询key是否被缓存
        bool IsSet(string key);

        //从缓存移除
        void Remove(string key);

        //缓存移除匹配
        void RemoveByPattern(string pattern);

        //清空所有缓存
        void Clear();
    }
}
