﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Specialized;
using System.Web;

namespace FuNong.Framework.Cookie
{
    public interface ICookie
    {
        void AddCookie(string key, NameValueCollection nvCollection);
        void AddCookie(string key, string value);
        void AddCookie(string key, string value,DateTime expireTime);
        void AddCookie(string key, NameValueCollection nvCollection,DateTime expireTime);

        string GetCookieString(string key);
        NameValueCollection GetCookieCollection(string key);

        void ClearCooke(string key);
    }
}
