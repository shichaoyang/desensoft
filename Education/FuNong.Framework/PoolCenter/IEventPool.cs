﻿using System;
namespace FuNong.Framework.PoolCenter
{
    public interface IEventPool
    {
        void FireAll();
        void FireAll(EventArgs args);
        void FireAll(object sender);
        void FireAll(object sender, EventArgs args);
        void FireEvent(object tag);
        void FireEvent(object tag, EventArgs args);
        void FireEvent(object tag, object sender);
        void FireEvent(object tag, object sender, EventArgs args);
        void Publish(params object[] tags);
        void Subscribe(object tag, EventHandler eventHandler);
        void Subscribe(object tag, EventHandler eventHandler, bool loose);
    }
}
