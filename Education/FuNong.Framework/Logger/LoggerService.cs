﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;

namespace FuNong.Framework.Logger
{
    public class LoggerService:ILoggerService
    {
        public LoggerService()
        {
            log4net.Config.XmlConfigurator.Configure();
            logger = LogManager.GetLogger(typeof(LoggerService));
        }

        private readonly ILog logger;

        public void Info(object message)
        {
            logger.Info(message);
        }
        public void Warn(object message)
        {
            logger.Warn(message);
        }
        public void Debug(object message)
        {
            logger.Debug(message);
        }
        public void Error(object message)
        {
            logger.Error(message);
        }
        public void Fatal(object message)
        {
            logger.Fatal(message);
        }
       
    }
}
