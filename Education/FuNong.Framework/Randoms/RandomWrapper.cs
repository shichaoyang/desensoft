﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FuNong.Framework.Randoms
{
    /// <summary>
    /// 随机性操作管理类
    /// </summary>
    public class RandomWrapper
    {
        public RandomWrapper()
        {
            _randomlibrary[0] = "1";
            _randomlibrary[1] = "2";
            _randomlibrary[2] = "3";
            _randomlibrary[3] = "4";
            _randomlibrary[4] = "5";
            _randomlibrary[5] = "6";
            _randomlibrary[6] = "7";
            _randomlibrary[7] = "8";
            _randomlibrary[8] = "9";
        }                       

        private Random _random = new Random();//随机发生器
        private string[] _randomlibrary = new string[9];//随机库

        /// 创建数字随机值
        /// <param name="length">随机值长度</param>
        /// <returns>随机值</returns>
        public string CreateRandomValue(int length)
        {
            return CreateRandomValue(length, true);
        }

        /// 创建随机值
        /// <param name="length">长度</param>
        /// <param name="onlyNumber">是否只包含数字</param>
        /// <returns>随机值</returns>
        public string CreateRandomValue(int length, bool onlyNumber)
        {
            int index;
            StringBuilder randomValue = new StringBuilder();

            for (int i = 0; i < length; i++)
            {
                if (onlyNumber)
                    index = _random.Next(0, 9);
                else
                    index = _random.Next(0, _randomlibrary.Length);

                randomValue.Append(_randomlibrary[index]);
            }

            return randomValue.ToString();
        }
    }
}
