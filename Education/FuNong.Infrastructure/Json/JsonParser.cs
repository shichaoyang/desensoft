﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization.Json;
using System.IO;

namespace FuNong.Infrastructure.Json
{
    public class JsonParser
    {
        public static T JsonDeserializer<T>(string json)
        {
            DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(T));
            MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(json));
            T t = (T)jsonSerializer.ReadObject(ms);
            return t;
        }
    }
}
